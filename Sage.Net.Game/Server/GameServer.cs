﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sage.Net.Base;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Sage.Net.Game.Service;
using System.ServiceModel.Description;
using System.Runtime.Serialization;

namespace Sage.Net.Game.Server
{
	public class GameServer : BaseServer
	{
		public IGameMachine Game { get; private set; }

		public GameServer(IGameMachine game)
		{
			this.Game = game;
		}

		public void StartServer()
		{
			StartServer(ServerAddressFactory.GetDefaultAddresses().ToArray());
		}

		public void StartServer(params Uri[] baseAddresses)
		{
			var host = CreateServiceHost<GameService,IGameService>(CreateService, baseAddresses);

			AttachDataContractResolver(host.Description.Endpoints[0], new SymbolContractResolver());

			//ContractDescription contractDesc = host.Description.Endpoints[0].Contract;
			//OperationDescription operationDesc = contractDesc.Operations.Find("GetCurrentPlayer");
			//var serializerBehavior = operationDesc.Behaviors.Find<DataContractSerializerOperationBehavior>();
			//if (serializerBehavior == null) {
			//    serializerBehavior = new DataContractSerializerOperationBehavior(operationDesc);
			//    operationDesc.Behaviors.Add(serializerBehavior);
			//}
			//serializerBehavior.DataContractResolver = new DeserializeAsBaseResolver();
 
			host.Open();
		}

		private static void AttachDataContractResolver(ServiceEndpoint endpoint, DataContractResolver resolver)
		{
			ContractDescription cd = endpoint.Contract;

			foreach (OperationDescription opdesc in cd.Operations)
			{
				var serializerBehavior = opdesc.Behaviors.Find<DataContractSerializerOperationBehavior>();

				if (serializerBehavior == null) {
					serializerBehavior =
						new DataContractSerializerOperationBehavior(opdesc);

					opdesc.Behaviors.Add(serializerBehavior);
				}

				serializerBehavior.DataContractResolver = resolver;
			}
		}

		private GameService CreateService()
		{
			//var add = OperationContext.Current.EndpointDispatcher.EndpointAddress;
			//System.Diagnostics.Trace.WriteLine(add);

			//var clientEndpoint = GetRemoteEndpoint(OperationContext.Current);
			//if (clientEndpoint != null) {
			//    System.Diagnostics.Trace.WriteLine(string.Format("{0}:{1}", clientEndpoint.Address, clientEndpoint.Port));
			//}

			return new GameService(Game);
		}

		private static RemoteEndpointMessageProperty GetRemoteEndpoint(OperationContext context)
		{
			/// http://www.danrigsby.com/blog/index.php/2008/05/21/get-the-clients-address-in-wcf/

			object property;
			if (context.IncomingMessageProperties.TryGetValue(RemoteEndpointMessageProperty.Name, out property)) {
				return property as RemoteEndpointMessageProperty;
			}

			return null;
		}
	}
}
