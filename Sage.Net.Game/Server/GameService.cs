﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Sage.Net.Game.Service;

namespace Sage.Net.Game.Server
{
	[ServiceBehavior(
		#if DEBUG
			IncludeExceptionDetailInFaults = true,
		#endif
		InstanceContextMode=InstanceContextMode.PerCall
	)]
	internal class GameService : IGameService
	{
		private readonly IGameMachine game;

		public GameService(IGameMachine game)
		{
			this.game = game;
		}

		IGameState IGameService.GetState()
		{
			return game.State;
		}

		IEnumerable<IGameMoveDescription> IGameService.GetMoveOptions()
		{
			return game.GetMoveOptions();
		}

		void IGameService.Play(IGameMoveDescription move)
		{
			//TODO: translate known exceptions in faults

			game.Play(move);
		}
	}
}
