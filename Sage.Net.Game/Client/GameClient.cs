﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sage.Net.Base;
using Sage.Net.Game.Service;
using System.Threading;
using System.ServiceModel;
using JpLabs.Extensions;

namespace Sage.Net.Game.Client
{
	public class GameClient : BaseClient, IGameMachine, IDisposable
	{
		class DisconnectedGameService : IGameService
		{
			public static IGameService Instance = new DisconnectedGameService();
			
			private DisconnectedGameService() {}

			private Exception DefaultException()
			{ return new InvalidOperationException("Service is disconnected"); }

			IGameState IGameService.GetState()
			{ throw DefaultException(); }

			IEnumerable<IGameMoveDescription> IGameService.GetMoveOptions()
			{ throw DefaultException(); }

			void IGameService.Play(IGameMoveDescription move)
			{ throw DefaultException(); }
		}

		private IGameService gameService;

		public event EventHandler<ThreadExceptionEventArgs> CommunicationExceptionHappened;

		public GameClient()
		{
			this.gameService = DisconnectedGameService.Instance;
		}

		public GameClient(IGameService gameService)
		{
			this.gameService = gameService;
		}

		private IClientChannel ServiceChannel
		{
			get { return this.gameService as IClientChannel; } //IClientChannel or IDuplexContextChannel
		}

		public CommunicationState State
		{
			get {
				var channel = gameService as IClientChannel;
				if (channel == null) return CommunicationState.Created;
				return channel.State;
			}
		}

		public bool IsConnected()
		{
			return State != CommunicationState.Created;
		}

		public void Connect(Uri hostAddress)
		{
			//TODO: handle username in the URI (e.g.: http://guest:pwd@localhost:8080/lobby)

			if (!hostAddress.IsAbsoluteUri) throw new ArgumentException("Invalid URI");
			if (IsConnected()) throw new InvalidOperationException("Already connected");

			var endpoint = BaseClient.CreateServiceEndpoint(typeof(IGameService), hostAddress, false);

			//remark: disposing the factory will close all created channels!
			var channelFactory = new ChannelFactory<IGameService>(endpoint);
			var service	= channelFactory.CreateChannel();

			var lobbyServiceCommObj = (ICommunicationObject)service;
			//CommObjWatcher.Create(lobbyServiceCommObj).StateChanged += LobbyServiceChannel_StateChanged;
			lobbyServiceCommObj.Open();

			this.gameService = service;
		}

		public void Dispose()
		{
			this.Disconnect();
		}

		public void Disconnect()
		{
			var service = this.gameService;
			var channel = this.ServiceChannel;

			if (channel != null) {
				//var debug = channel.State;
				//var debug = ((IDuplexContextChannel)service).AutomaticInputSessionShutdown;
				
				if (channel.State == CommunicationState.Opened) {
					try
					{
						channel.Close();
					} catch (CommunicationException ex) {
						OnCommunicationExceptionHappened(ex);
					}
				}

				if (channel.State != CommunicationState.Faulted) channel.Dispose();
			}

			this.gameService = DisconnectedGameService.Instance;
		}

		//private void DisconnectChannel()
		//{
		//    var channel = this.ServiceChannel;
		//    if (channel != null) channel.Close();
		//}

		private void OnCommunicationExceptionHappened(CommunicationException exception)
		{
			System.Diagnostics.Debug.Write(exception, "CommunicationException");

			OperationContext.Current.InvokeOnCompleted(
				() => this.CommunicationExceptionHappened.RaiseEvent(this, new ThreadExceptionEventArgs(exception))
			);
		}

		IGameState IGameMachine.State
		{
			get { return gameService.GetState(); }
		}

		IEnumerable<IGameMoveDescription> IGameMachine.GetMoveOptions()
		{
			return gameService.GetMoveOptions();
		}

		void IGameMachine.Play(IGameMoveDescription moveDesc)
		{
			gameService.Play(moveDesc);
		}
	}
}
