﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Reflection;

namespace Sage.Net.Game.Service
{
	internal static class Contract
	{
		public const string Namespace = "http://jplabs.org/mage/game";
	}

	internal static class FaultHelper
	{
		internal static FaultException<T> Exception<T>() where T : new()
		{
			return new FaultException<T>(new T());
		}
	}

	[DataContract(Namespace=Contract.Namespace)]
	public sealed class NotAllowedError {}

	[ServiceContract(SessionMode=SessionMode.Allowed, Namespace=Contract.Namespace)]
	//[ServiceKnownType(typeof(GamePlayerData))]
	[ServiceKnownType("GetKnownTypes", typeof(KnownTypesHelper))]
	public interface IGameService
	{
		//[OperationContract]
		//IEnumerable<IGamePlayer> GetPlayers();

		//TODO: How will this contract support partial-information games?

		[OperationContract]
		IGameState GetState();
		
		[OperationContract]
		IEnumerable<IGameMoveDescription> GetMoveOptions();

		[OperationContract][FaultContract(typeof(NotAllowedError))]
		void Play(IGameMoveDescription move);
	}

	static class KnownTypesHelper
	{
		public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider serviceContractType)
		{
			return new [] {
				typeof(BasicGameState),
				typeof(RectBoard),
				typeof(BasicPlayer),
				typeof(BasicPiece),
				typeof(BasicPieceClass),
				typeof(JpLabs.Symbols.Symbol),

				typeof(MoveDescription),
				typeof(JpLabs.Geometry.PointInt),
			};
		}
	}
}
