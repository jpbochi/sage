﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Ninject.Modules;

namespace Sage.Games
{
	public static class GameFactory
	{
		public static IGameMachine Get<TModule>() where TModule : INinjectModule, new()
		{
			IKernel kernel = new StandardKernel();

			kernel.Load<TModule>();

			return kernel.Get<IGameMachine>();
		}
	}
}
