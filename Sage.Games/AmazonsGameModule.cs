﻿using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using Sage.Augmentation;

namespace Sage.Games.Amazons
{
	public abstract class AmazonPieces : SymbolEnum
	{
		static AmazonPieces() { SymbolEnum.InitEnum<AmazonPieces>(); }

		public static readonly Symbol Amazon = null;
		public static readonly Symbol Arrow = null;
	}

	public class AmazonsGameModule : NinjectModule
	{
		public const int BOARD_SIZE = 7;

		public override void Load()
		{
			this.Kernel.Settings.InjectNonPublic = true;

			Bind<IGameMachine>().To<BasicGameMachine>();

			Bind<IGameState>().To<BasicGameState>().WithConstructorArgument("players", GetPlayers()).OnActivation(StartUpGame);

			Bind<IGameBoard>().ToMethod( CreateInitialBoard );
			Bind<IDirectionSystem>().ToMethod( ctx => BasicDirectionSystem.CreateCardinalPlusIntercardinal() );

			Bind<IGameCommand>().To<IsGameOverEvaluation>().Named(KnownGameOperations.UpdateScore);

			Bind<IGamePieceClass>().To<BasicPieceClass>().InSingletonScope().Named(AmazonPieces.Arrow.Name)
			.OnActivation(
				(ctx,p) => p.SetPieceType(AmazonPieces.Arrow)
			);

			Bind<IGamePieceClass>().To<BasicPieceClass>().InSingletonScope().Named(AmazonPieces.Amazon.Name)
			.OnActivation(
				(ctx,p) => {
					p.SetPieceType(AmazonPieces.Amazon);
					p.SetArrowClass(ctx.Kernel.Get<IGamePieceClass>(AmazonPieces.Arrow.Name));
				}
			);

			Bind<IGameReferee>().To<AmazonsReferee>();
		}

		public static IEnumerable<IGamePlayer> GetPlayers()
		{
			return Symbol.GetEnumValues<ClassicPlayer>().Select( p => new BasicPlayer(p.GetId()) );
		}

		private static void StartUpGame(IGameState state)
		{
			state.CurrentPlayerId = ClassicPlayer.White.GetId();
		}

		private IGameBoard CreateInitialBoard(IContext context)
		{
			var kernel = context.Kernel;
			var board = new RectBoard(BOARD_SIZE, BOARD_SIZE);

			board.AddPiece(kernel.Get<IGamePieceClass>(AmazonPieces.Arrow.Name), TilePos.OffBoard);

			board.AddPiece(GetAmazonForPlayer(kernel, ClassicPlayer.White), TilePos.FromName("a3"));
			board.AddPiece(GetAmazonForPlayer(kernel, ClassicPlayer.White), TilePos.FromName("c1"));
			board.AddPiece(GetAmazonForPlayer(kernel, ClassicPlayer.White), TilePos.FromName("e1"));
			board.AddPiece(GetAmazonForPlayer(kernel, ClassicPlayer.White), TilePos.FromName("g3"));

			board.AddPiece(GetAmazonForPlayer(kernel, ClassicPlayer.Black), TilePos.FromName("a5"));
			board.AddPiece(GetAmazonForPlayer(kernel, ClassicPlayer.Black), TilePos.FromName("c7"));
			board.AddPiece(GetAmazonForPlayer(kernel, ClassicPlayer.Black), TilePos.FromName("e7"));
			board.AddPiece(GetAmazonForPlayer(kernel, ClassicPlayer.Black), TilePos.FromName("g5"));

			return board;
		}

		private IGamePiece GetAmazonForPlayer(IKernel kernel, Symbol<ClassicPlayer> player)
		{
			return kernel.Get<IGamePieceClass>(AmazonPieces.Amazon.Name).CreatePiece().SetFlag(player.GetId());
		}
	}

	internal class IsGameOverEvaluation : BaseStructuralEquatable, IGameCommand
	{
		[Inject]
		public IGameReferee Referee { get; internal set; }

		public void Execute(IGameState state)
		{
			var winners = GetWinners(state).Select( p => p.Id ).ToArray();
			state.Winners = winners;

			if (winners.Any()) state.CurrentPlayerId = null;
		}

		public IEnumerable<IGamePlayer> GetWinners(IGameState state)
		{
			if (IsStalemated(state)) return state.Players.Where( p => p.Id != state.CurrentPlayerId );

			return Enumerable.Empty<IGamePlayer>();
		}

		private bool IsStalemated(IGameState state)
		{
			if (Referee == null) return false;

			return !Referee.GetMoveOptions(state).Any();
		}

		protected override System.Collections.IStructuralEquatable GetEquatableSurrogate()
		{
			return null;
		}
	}

	internal static class AmazonProperties
	{
		public static AugProperty LastPieceToMoveProperty =
			AugProperty.Create("LastPieceToMove", typeof(IGamePiece), typeof(AmazonProperties));

		public static AugProperty ArrowClassProperty =
			AugProperty.Create("ArrowClass", typeof(IGamePieceClass), typeof(AmazonProperties));

		public static IGamePiece GetLastPieceToMove(this IGameState state)
		{
			return state.GetValue<IGamePiece>(LastPieceToMoveProperty);
		}

		public static void SetArrowClass(this IGamePiece piece, IGamePieceClass pieceClass)
		{
			piece.SetValue(ArrowClassProperty, pieceClass);
		}

		public static IGamePieceClass GetArrowClass(this IGamePiece piece)
		{
			return piece.GetValue<IGamePieceClass>(ArrowClassProperty);
		}
	}

	internal class AmazonsReferee : IGameReferee
	{
		public IEnumerable<IGameMove> GetMoveOptions(IGameState state)
		{
			if (state.IsOver()) return Enumerable.Empty<IGameMove>();

			var lastPieceToMove = state.GetLastPieceToMove();

			if (lastPieceToMove != null) return GetArrowShotMoves(state, lastPieceToMove);
			
			return GetAmazonMoves(state);
		}

		private IEnumerable<IGameMove> GetAmazonMoves(IGameState state)
		{
			var currentPlayerId = state.CurrentPlayerId.Value;
			var amazonPieces = state.Board.GetAllPieces().Where( p => p.GetFlag() == currentPlayerId );

			return
				from amazon in amazonPieces
				from destination in SlideDestinations(amazon)
				select new AmazonSlideMove(currentPlayerId, amazon, destination) { Referee = this };
		}

		private IEnumerable<IGameMove> GetArrowShotMoves(IGameState state, IGamePiece lastPieceToMove)
		{
			var currentPlayerId = state.CurrentPlayerId.Value;
			var arrowClass = lastPieceToMove.GetArrowClass();

			return
				from destination in SlideDestinations(lastPieceToMove)
				select new ArrowShotMove(currentPlayerId, arrowClass, destination) { Referee = this };
		}

		private IEnumerable<PointInt> SlideDestinations(IGamePiece amazon)
		{
			var origin = amazon.Position;
			var board = amazon.Board;
			return SlideDestinations(origin, board);
		}

		private IEnumerable<PointInt> SlideDestinations(PointInt origin, IGameBoard board)
		{	
			var directions = board.DirectionSystem.GetDirections();

			foreach (var direction in directions) {
				var position = direction.Transform(origin);

				while (board.Contains(position) && CanSlideAcrossTile(board.GetPiecesAt(position))) {
					yield return position;

					position = direction.Transform(position);
				}
			}
		}

		private bool CanSlideAcrossTile(IEnumerable<IGamePiece> entries)
		{
			return entries.IsEmpty();
		}
	}

	internal class AmazonSlideMove : IGameMove
	{
		[Inject]
		public IGameReferee Referee { get; internal set; }

		public int PlayerId { get; private set; }
		public IGamePiece Piece { get; private set; }
		public PointInt Position { get; private set; }

		public AmazonSlideMove(int playerId, IGamePiece piece, PointInt position)
		{
			this.PlayerId = playerId;
			this.Piece = piece;
			this.Position = position;
		}

		IGameMoveDescription IGameMove.Description
		{
			get { return MoveDesc.CreateMovePieceDesc(PlayerId, Piece, Position); }
		}

		public IEnumerable<IGameCommand> Resolve(IGameState state)
		{
			yield return new MovePieceCommand(Piece, Position);
			yield return new SetGamePropertyCommand(AmazonProperties.LastPieceToMoveProperty, Piece);
			yield return new IsGameOverEvaluation() { Referee = Referee};
		}
	}

	internal class ArrowShotMove : IGameMove
	{
		[Inject]
		public IGameReferee Referee { get; internal set; }

		public int PlayerId { get; private set; }
		public IGamePieceClass PieceClass { get; private set; }
		public PointInt Position { get; private set; }

		public ArrowShotMove(int playerId, IGamePieceClass pieceClass, PointInt position)
		{
			this.PlayerId = playerId;
			this.PieceClass = pieceClass;
			this.Position = position;
		}

		IGameMoveDescription IGameMove.Description
		{
			get { return MoveDesc.CreateDropPieceDesc(PlayerId, PieceClass, Position); }
		}

		public IEnumerable<IGameCommand> Resolve(IGameState state)
		{
			yield return new AddPieceCommand(PieceClass, Position);
			yield return new SetGamePropertyCommand(AmazonProperties.LastPieceToMoveProperty, null);
			yield return new CycleTurnCommand();
			yield return new IsGameOverEvaluation() { Referee = Referee};
		}
	}
}
