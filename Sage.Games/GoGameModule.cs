﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using Sage.Augmentation;

namespace Sage.Games.Go
{
	public abstract class GoPieces : SymbolEnum
	{
		static GoPieces() { SymbolEnum.InitEnum<GoPieces>(); }

		public static readonly Symbol Stone = null;
	}

	public class GoGameModule : NinjectModule
	{
		public const int DEFAULT_BOARD_SIZE = 9; // classic Go board sizes are: 9, 13, 19
		public const string ROWS_PARAM = "BOARD_ROWS";
		public const string COLS_PARAM = "BOARD_COLS";

		public override void Load()
		{
			this.Kernel.Settings.InjectNonPublic = true;

			Bind<IGameMachine>().To<BasicGameMachine>();

			Bind<IGameState>().To<BasicGameState>().WithConstructorArgument("players", GetPlayers()).OnActivation(StartUpGame);

			Bind<ushort>().ToConstant(DEFAULT_BOARD_SIZE).Named(ROWS_PARAM);
			Bind<ushort>().ToConstant(DEFAULT_BOARD_SIZE).Named(COLS_PARAM);
			Bind<IGameBoard>().ToMethod( CreateInitialBoard );
			Bind<IDirectionSystem>().ToMethod( ctx => BasicDirectionSystem.CreateCardinal() );

			Bind<IGameCommand>().To<UpdateScoreCommand>().Named(KnownGameOperations.UpdateScore);

			Bind<IGamePieceClass>().To<BasicPieceClass>().Named(GoPieces.Stone.Name)
			.OnActivation(
				(ctx,p) => p.SetPieceType(GoPieces.Stone)
			);

			Bind<IGameReferee>().To<GoReferee>();
		}

		public static IEnumerable<IGamePlayer> GetPlayers()
		{
			return Symbol.GetEnumValues<ClassicPlayer>().Reverse().Select( p => new BasicPlayer(p.GetId()) );
		}

		private static void StartUpGame(IGameState state)
		{
			state.CurrentPlayerId = ClassicPlayer.Black.GetId();
		}

		private IGameBoard CreateInitialBoard(IContext context)
		{
			var kernel = context.Kernel;

			ushort rows = kernel.Get<ushort>(ROWS_PARAM);
			ushort cols = kernel.Get<ushort>(COLS_PARAM);
			var board = new RectBoard(cols, rows);
				
			board.AddPiece(kernel.Get<IGamePieceClass>(GoPieces.Stone.Name).SetFlag(ClassicPlayer.Black.GetId()), TilePos.OffBoard);
			board.AddPiece(kernel.Get<IGamePieceClass>(GoPieces.Stone.Name).SetFlag(ClassicPlayer.White.GetId()), TilePos.OffBoard);

			return board;
		}
	}

	internal static class GoProperties
	{
		public static AugProperty PreviousMoveTypeProperty =
			AugProperty.Create("PreviousMoveType", typeof(Symbol<MoveType>), typeof(GoProperties), null);

		public static AugProperty TerritoryProperty =
			AugProperty.Create("Territory", typeof(int), typeof(GoProperties), 0);
		
		public static AugProperty PreviousBoardProperty =
			AugProperty.Create("PreviousBoard", typeof(WorkBoard), typeof(GoProperties));

		public static int GetTerritory(this IGamePlayer player)
		{
			return player.GetValue<int>(TerritoryProperty);
		}

		public static void SetTerritory(this IGamePlayer player, int value)
		{
			player.SetValue(TerritoryProperty, value);
		}

		public static WorkBoard GetPreviousBoard(this IGameState state)
		{
			return state.GetValue<WorkBoard>(PreviousBoardProperty);
		}

		private static IEnumerable<PointInt> GetNeighbors(this IGameBoard board, PointInt tile)
		{
			return GetNeighbors(board, tile, board.DirectionSystem);
		}

		private static IEnumerable<PointInt> GetNeighbors(ITileGrid grid, PointInt tile, IDirectionSystem directionSystem)
		{
			if (directionSystem == null) yield break;

			var directions = directionSystem.GetDirections();

			foreach (var d in directions) {
				var p = d.Transform(tile);

				if (grid.Contains(p)) yield return p;
			}
		}

		public static IEnumerable<PointInt> GetGroup(this IGameBoard board, PointInt root, Func<PointInt,bool> stopGroupingFunc)
		{
			var group = new HashSet<PointInt>();

			return YieldConnectedGroup(board, root, group, stopGroupingFunc);
		}

		private static IEnumerable<PointInt> YieldConnectedGroup(this IGameBoard board, PointInt tile, ISet<PointInt> group, Func<PointInt,bool> stopGroupingFunc)
		{
			if (group.Contains(tile)) yield break;
			group.Add(tile);

			yield return tile;

			if (stopGroupingFunc(tile)) yield break;

			foreach (var neighbor in board.GetNeighbors(tile))
			foreach (var connected in board.YieldConnectedGroup(neighbor, group, stopGroupingFunc)) yield return connected;
		}

		public static IEnumerable<IEnumerable<PointInt>> GroupBy(this IGameBoard board, Func<PointInt,IEnumerable<PointInt>> groupFunc)
		{
			//*//
			//var pendingTiles = board.GetAllTiles().ToList();
			var visitedTiles = new HashSet<PointInt>();
			//for (int i=0; i<pendingTiles.Count; i++) {
			//	var tile = pendingTiles[i];
			foreach (var tile in board.GetAllTiles()) {
				if (!visitedTiles.Contains(tile)) {
					var group = groupFunc(tile).ToArray();
					visitedTiles.UnionWith(group);

					yield return group;
				}
			}
			/*/
			var pendingTiles = new HashSet<PointInt>(board.GetAllTiles());
			while (pendingTiles.Count > 0) {
				var tile = pendingTiles.First();

				var group = groupFunc(tile).ToArray();
				pendingTiles.ExceptWith(group);

				yield return group;
			}//*/
		}

		public static IEnumerable<IEnumerable<PointInt>> GroupBy(this IGameBoard board, Func<PointInt,bool> stopGroupingFunc)
		{
			return board.GroupBy(tile => board.GetGroup(tile, stopGroupingFunc));
		}
	}

	internal class UpdateScoreCommand : BaseStructuralEquatable, IGameCommand
	{
		public void Execute(IGameState state)
		{
			CountTerritories(state);

			state.Winners = Enumerable.Empty<int>();
		}

		private void CountTerritories(IGameState state)
		{
			var territories = state.Players.ToDictionary(p => p.Id, p => 0);

			var board = state.Board;

			//var groups = board.GroupBy(tile => board.GetConnectedTerritoryWithBorder(tile));
			var groups = board.GroupBy(tile => board.GetPiecesAt(tile).Any());

			foreach (var group in groups) {
				var piecesAtGroup = group.SelectMany(t => board.GetPiecesAt(t));
				var owners = piecesAtGroup
					.Select(p => p.GetFlag())
					.Where(f => f != null)
					.Distinct().ToArray();
				
				if (owners.Length == 1) territories[owners[0].Value] += group.Where(t => board.GetPiecesAt(t).IsEmpty()).Count();
			}

			foreach (var playerTerritory in territories) {
				state.GetPlayerById(playerTerritory.Key).SetTerritory(playerTerritory.Value);
			}
		}

		protected override IStructuralEquatable GetEquatableSurrogate()
		{
			return null;
		}
	}

	internal class GoReferee : IGameReferee
	{
		public IEnumerable<IGameMove> GetMoveOptions(IGameState state)
		{
			if (state.IsOver()) yield break;

			var currentPlayerId = state.CurrentPlayerId.Value;
			yield return new PassTurnMove(currentPlayerId);

			var otherPlayer = CycleTurnCommand.GetNextPlayer(state).Id;
			var board = state.Board;

			var pieceTemplate
				= board
				.GetPiecesAt(TilePos.OffBoard)
				.Where( p => p.GetFlag() == currentPlayerId )
				.OfType<IGamePieceClass>()
				.First();
			
			//Searching for valid moves in parallel showed a 60% gain in measured performance
			var otherMoves = 
				state.Board.GetAllTiles()
					.AsParallel()
					.Where( p => board.GetPiecesAt(p).IsEmpty() )
					.Select( p => new GoMove(currentPlayerId, pieceTemplate, p) )
					.Where( m => !m.IsSuicide(board, otherPlayer) )
					.Where( m => !m.IsKo(state) );

			foreach (var move in otherMoves) yield return move;
		}

		public IGameMove GetMoveByDescription(IGameState state, IGameMoveDescription moveDesc)
		{
			///TODO: Optimization opportunity: Go has a exceptionally high number of available move (361 at start with a 19x19 board)
			/// When trying to play a move, I should verify only if that move is valid, instead of creating a full list.
			/// In any case, I MUST NOT duplicate rule implementations.

			throw new NotImplementedException();
		}
	}

	//*
	internal class WorkBoard : IStructuralEquatable
	{
		int[,] board;

		private WorkBoard(IGameBoard board)
		{
			this.board = GetWorkBoard(board); 
		}

		public static WorkBoard Create(IGameBoard board)
		{
			return new WorkBoard(board); 
		}

		public static int[,] GetWorkBoard(IGameBoard board)
		{
			var rectBoard = ((RectBoard)board);

			var newBoard = new int[rectBoard.Cols, rectBoard.Rows];
			var pieces = board
				.GetAllPieces()
				.Where(p => p.Position != TilePos.OffBoard);

			foreach (var p in pieces) newBoard[p.Position.X, p.Position.Y] = p.GetFlag().Value;

			return newBoard;
		}

		public void Add(PointInt position, int flag)
		{
			board[position.X, position.Y] = flag;
		}

		public bool Remove(PointInt position)
		{
			board[position.X, position.Y] = 0; return true;
		}

		public int? Get(PointInt position)
		{
			int flag = board[position.X, position.Y]; return (flag == 0) ? null : (int?)flag;
		}

		public bool Equals(object objOther, IEqualityComparer comparer)
		{
			var other = objOther as WorkBoard;
			if (other == null) return false;

			if (board.GetLength(0) != other.board.GetLength(0)) return false;
			if (board.GetLength(1) != other.board.GetLength(1)) return false;

			for (int c=0; c<board.GetLength(0); c++)
				for (int r=0; r<board.GetLength(1); r++)
					if (board[c,r] != other.board[c,r]) return false;
			
			return true;
		}

		public int GetHashCode(IEqualityComparer comparer)
		{
			return comparer.GetHashCode(board);
		}
	}
	/*/
	internal class WorkBoard : IStructuralEquatable
	{
		IDictionary<PointInt,int> board;

		private WorkBoard(IGameBoard board)
		{
			this.board = GetWorkBoard(board); 
		}

		public static WorkBoard Create(IGameBoard board)
		{
			return new WorkBoard(board); 
		}

		public static IDictionary<PointInt,int> GetWorkBoard(IGameBoard board)
		{
			return board
				.GetAllPieces()
				.Where(p => p.Position != TilePos.OffBoard)
				.ToDictionary(p => p.Position, p => p.GetFlag().Value);
		}

		public void Add(PointInt position, int flag)
		{
			board.Add(position, flag);
		}

		public bool Remove(PointInt position)
		{
			return board.Remove(position);
		}

		public int? Get(PointInt position)
		{
			int flag; return board.TryGetValue(position, out flag) ? (int?)flag : null;
		}

		public bool Equals(object objOther, IEqualityComparer comparer)
		{
			var other = objOther as WorkBoard;
			if (other == null) return false;

			return board.OrderBy(p => p.Key.GetHashCode()).SequenceEqual(other.board.OrderBy(p => p.Key.GetHashCode()));
		}

		public int GetHashCode(IEqualityComparer comparer)
		{
			return comparer.GetHashCode(board);
		}
	}//*/

	internal class GoMove : IGameMove
	{
		public int PlayerId { get; private set; }
		public IGamePieceClass PieceClass { get; private set; }
		public PointInt Position { get; private set; }

		private WorkBoard boardAfterMove;
		private ICollection<PointInt> killPositions;

		public GoMove(int playerId, IGamePieceClass pieceClass, PointInt position)
		{
			this.PlayerId = playerId;
			this.PieceClass = pieceClass;
			this.Position = position;
		}

		IGameMoveDescription IGameMove.Description
		{
			get { return MoveDesc.CreateDropPieceDesc(PlayerId, PieceClass, Position); }
		}

		private void PrepareCache(IGameBoard board)
		{
			var workBoard = GetWorkBoardAfterDrop(board, Position, PieceClass.GetFlag().Value);

			this.killPositions = GetSurroundedTiles(board, workBoard, PlayerId).ToArray();

			foreach (var k in killPositions) workBoard.Remove(k);

			this.boardAfterMove = workBoard;
		}

		private static WorkBoard GetWorkBoardAfterDrop(IGameBoard board, PointInt dropPosition, int playerId)
		{
			var workBoard = WorkBoard.Create(board);
			workBoard.Add(dropPosition, playerId);
			return workBoard;
		}

		private static IEnumerable<PointInt> GetSurroundedTiles(IGameBoard board, WorkBoard workBoard, int attackerId)
		{
			var groups = board.GroupBy(
				t => { var flag = workBoard.Get(t); return flag == null || flag == attackerId; }
				//t => workBoard.Get(t) == attackerId
			);

			return groups
				.Where(g => g.All(t => workBoard.Get(t) != null))
				.SelectMany(g => g.Where(t => workBoard.Get(t) != attackerId));
		}

		public WorkBoard GetWorkBoardAfterMove(IGameBoard board)
		{
			if (boardAfterMove == null) PrepareCache(board);
			return boardAfterMove;
		}

		public IEnumerable<PointInt> GetKills(IGameBoard board)
		{
			if (killPositions == null) PrepareCache(board);
			return killPositions;
		}

		public bool IsKo(IGameState state)
		{
			var previousBoard = state.GetPreviousBoard();

			var workBoard = GetWorkBoardAfterMove(state.Board);

			return StructuralComparisons.StructuralEqualityComparer.Equals(workBoard, previousBoard);
		}

		public bool IsSuicide(IGameBoard board, int otherPlayerId)
		{
			return GetSuicides(board, otherPlayerId).Any();
		}

		private IEnumerable<PointInt> GetSuicides(IGameBoard board, int otherPlayerId)
		{
			var workBoard = GetWorkBoardAfterMove(board);

			return GetSurroundedTiles(board, workBoard, otherPlayerId);
		}

		public IEnumerable<IGameCommand> Resolve(IGameState state)
		{
			/// TODO: yield CapturePieceCommands
			/// Problem description:
			/// . In order to know which pieces to capture, I need to add the piece being played in this move;
			/// . I can't add the piece to the current board, because move resolution must not have side-effects;
			/// . Cloning the board makes sense (yet, it's not implemented);
			/// . The captured pieces must be on the original board, not the clone;
			/// . 1) Should the cloned pieces keep a reference to the original pieces?
			/// . 2) Or should I rely on the rule that no two pieces can stand at the same position?
			/// answer= both! I'll create a light-weight clone!

			var board = PieceClass.Board;
			var previousBoard = WorkBoard.Create(board);

			var kills = GetKills(board);
			var killedStoned = kills.Select(k => board.GetFirstPieceAt(k));
			foreach (var s in killedStoned) yield return new CapturePieceCommand(s);

			yield return new AddPieceCommand(PieceClass, Position);
			yield return new SetGamePropertyCommand(GoProperties.PreviousMoveTypeProperty, MoveType.DropPiece);
			yield return new SetGamePropertyCommand(GoProperties.PreviousBoardProperty, previousBoard);
			yield return new CycleTurnCommand();
			yield return new UpdateScoreCommand();
		}
	}

	internal class PassTurnMove : IGameMove
	{
		public int PlayerId { get; private set; }

		public PassTurnMove(int playerId)
		{
			this.PlayerId = playerId;
		}

		IGameMoveDescription IGameMove.Description
		{
			get { return MoveDesc.CreatePassTurnDesc(PlayerId); }
		}

		public IEnumerable<IGameCommand> Resolve(IGameState state)
		{
			bool wasPreviousMovePass = state.GetValue<Symbol>(GoProperties.PreviousMoveTypeProperty) == MoveType.PassTurn;

			yield return new SetGamePropertyCommand(GoProperties.PreviousBoardProperty, null);

			if (wasPreviousMovePass) {
				yield return new SetGamePropertyCommand(BasicGameState.CurrentPlayerIdProperty, null);
			} else {
				yield return new SetGamePropertyCommand(GoProperties.PreviousMoveTypeProperty, MoveType.PassTurn);
				yield return new CycleTurnCommand();
			}

			yield return new UpdateScoreCommand();
		}
	}
}
