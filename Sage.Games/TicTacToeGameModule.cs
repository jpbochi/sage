﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;

namespace Sage.Games.TicTacToe
{
	public abstract class TicTacToePieces : SymbolEnum
	{
		static TicTacToePieces() { SymbolEnum.InitEnum<TicTacToePieces>(); }

		public static readonly Symbol Mark = null;
	}

	public abstract class TicTacToePlayer : SymbolEnum
	{
		static TicTacToePlayer() { SymbolEnum.InitEnum<TicTacToePlayer>(); }

		public static readonly Symbol X = null;
		public static readonly Symbol O = null;
	}

	public class TicTacToeGameModule : NinjectModule
	{
		public const int BOARD_SIZE = 3;

		public override void Load()
		{
			this.Kernel.Settings.InjectNonPublic = true;

			Bind<IGameMachine>().To<BasicGameMachine>();

			Bind<IGameState>().To<BasicGameState>().WithConstructorArgument("players", GetPlayers()).OnActivation(StartUpGame);

			Bind<IGameBoard>().ToMethod( CreateInitialBoard );

			Bind<IGameCommand>().To<IsGameOverEvaluation>().Named(KnownGameOperations.UpdateScore);

			Bind<IGamePieceClass>().To<BasicPieceClass>().Named(TicTacToePieces.Mark.Name)
			.OnActivation(
				(ctx,p) => p.SetPieceType(TicTacToePieces.Mark)
			);

			Bind<IGameReferee>().To<TicTacToeReferee>();
		}

		public static IEnumerable<IGamePlayer> GetPlayers()
		{
			return Symbol.GetEnumValues<TicTacToePlayer>().Select( p => new BasicPlayer(p.GetId()) );
		}

		private static void StartUpGame(IGameState state)
		{
			state.CurrentPlayerId = TicTacToePlayer.X.GetId();
		}

		private IGameBoard CreateInitialBoard(IContext context)
		{
			var kernel = context.Kernel;
			var board = new RectBoard(BOARD_SIZE, BOARD_SIZE);

			board.AddPiece(kernel.Get<IGamePieceClass>(TicTacToePieces.Mark.Name).SetFlag(TicTacToePlayer.X.GetId()), TilePos.OffBoard);
			board.AddPiece(kernel.Get<IGamePieceClass>(TicTacToePieces.Mark.Name).SetFlag(TicTacToePlayer.O.GetId()), TilePos.OffBoard);

			return board;
		}
	}

	internal class IsGameOverEvaluation : BaseStructuralEquatable, IGameCommand
	{
		public void Execute(IGameState state)
		{
			var winners = GetWinners(state).Select( p => p.Id ).ToArray();
			state.Winners = winners;

			if (winners.Any()) state.CurrentPlayerId = null;
		}

		public IEnumerable<IGamePlayer> GetWinners(IGameState state)
		{
			var winners = state.Players.Where( p => DidPlayerWon(state, p.Id) ).ToArray();

			if (winners.Length == 0 && IsBoardFull(state.Board)) return state.Players; //Both players won = Draw

			return winners;
		}

		private bool IsBoardFull(IGameBoard board)
		{
			return board.GetAllTiles().All( pos => board.GetPiecesAt(pos).Any() );
		}

		private bool DidPlayerWon(IGameState game, int playerId)
		{
			var board = (RectBoard)game.Board;

			var winningLines =
				Enumerable.Range(0, 3).Select( r => Enumerable.Range(0, 3).Select( c => TilePos.Create(r, c) ) )
				.Concat(
					Enumerable.Range(0, 3).Select( c => Enumerable.Range(0, 3).Select( r => TilePos.Create(r, c) ) )
				)
				.Concat(
					Enumerable.Range(0, 3).Select( d => TilePos.Create(d, d) )
				)
				.Concat(
					Enumerable.Range(0, 3).Select( d => TilePos.Create(d, 2-d) )
				);

			return winningLines.Any(
				line => line.All(
					pos => {
						var piece = board[pos];
						return (piece != null) && (piece.GetFlag() == playerId);
					}
				)
			);
		}

		protected override IStructuralEquatable GetEquatableSurrogate()
		{
			return null;
		}
	}

	internal class TicTacToeReferee : IGameReferee
	{
		public IEnumerable<IGameMove> GetMoveOptions(IGameState state)
		{
			//if (!state.IsStarted) return new StartupMove().ToEnumerable();
			if (state.IsOver()) return Enumerable.Empty<IGameMove>();

			var currentPlayerId = state.CurrentPlayerId.Value;

			var pieceTemplate
				= state.Board
				.GetPiecesAt(TilePos.OffBoard)
				.Where( p => p.GetFlag() == currentPlayerId )
				.OfType<IGamePieceClass>()
				.Single();

			return state.Board
				.GetAllTiles()
				.Where( p => state.Board.GetPiecesAt(p).IsEmpty() )
				.Select( p => new TicTacToeMove(currentPlayerId, pieceTemplate, p) );
		}
	}

	internal class TicTacToeMove : IGameMove
	{
		public int PlayerId { get; private set; }
		public IGamePieceClass PieceClass { get; private set; }
		public PointInt Position { get; private set; }

		public TicTacToeMove(int playerId, IGamePieceClass pieceClass, PointInt position)
		{
			this.PlayerId = playerId;
			this.PieceClass = pieceClass;
			this.Position = position;
		}

		IGameMoveDescription IGameMove.Description
		{
			get { return MoveDesc.CreateDropPieceDesc(PlayerId, PieceClass, Position); }
		}

		public IEnumerable<IGameCommand> Resolve(IGameState state)
		{
			yield return new AddPieceCommand(PieceClass, Position);
			yield return new CycleTurnCommand();
			yield return new IsGameOverEvaluation();
		}
	}
}
