﻿using System.Linq;
using JpLabs.Geometry;
using JpLabs.Symbols;
using System.Collections.Generic;

namespace Sage
{
	public abstract class ClassicPlayer : SymbolEnum
	{
		static ClassicPlayer() { SymbolEnum.InitEnum<ClassicPlayer>(); }

		public static readonly Symbol<ClassicPlayer> White = null;
		public static readonly Symbol<ClassicPlayer> Black = null;
	}

	public static class GameBoardExt
	{
		public static IGamePiece GetFirstPieceAt(this IGameBoard board, PointInt position)
		{
			return board.GetPiecesAt(position).First();
		}
	}

	public static class AugObjectExt
	{
		public static T GetValue<T>(this IAugObject augObj, Augmentation.AugProperty ap)
		{
			//return (T)augObj.GetValue(ap);
			var value = augObj.GetValue(ap);

			if (value is T) return (T)value;

			return (T)ap.ConvertValue(value);
		}
	}

	public static class GameRefereeExt
	{
		public static IGameMove GetMoveByDescription(this IGameReferee referee, IGameState state, IGameMoveDescription moveDesc)
		{
			var moves = referee.GetMoveOptions(state);

			//#warning //DEBUG ONLY
			//moves = moves.ToArray();

			return moves.FirstOrDefault( m => MoveDesc.Comparer.Equals(m.Description, moveDesc) );
		}
	}

	public static class GameStateExt
	{
		public static bool IsOver(this IGameState state)
		{
			return state.CurrentPlayerId == null;
		}

		public static IGamePlayer GetPlayerById(this IGameState state, int id)
		{
			return state.Players.Where( p => p.Id == id ).FirstOrDefault();
		}

		public static IGamePlayer GetCurrentPlayer(this IGameState state)
		{
			return state.Players.Where( p => p.Id == state.CurrentPlayerId ).FirstOrDefault();
		}

		public static IEnumerable<IGamePlayer> GetWinners(this IGameState state)
		{
			return state.Players.Where( p => state.Winners.Contains(p.Id) );
		}
	}
}
