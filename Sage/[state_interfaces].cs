﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Geometry;

namespace Sage
{
	/// <summary>
	/// Represents an frozen game state.
	/// The rules necessary to say what moves are availlable are kept in a Referee.
	/// It must be clonable and serializable
	/// </summary>
	public interface IGameState : IAugObject //: ICloneable//, System.Runtime.Serialization.ISerializable
	{
		IEnumerable<IGamePlayer> Players { get; }
		IGameBoard Board { get; }

		int? CurrentPlayerId { get; set; }
		IEnumerable<int> Winners { get; set; }

		//Extended properties candidates: IsStarted
	}

	public interface IGamePlayer : IAugObject
	{
		int Id { get; }

		//Extended properties candidates: Name; HasResigned; DisplayImage; Team; RemoteUri; Power;
	}

	/// <summary>
	/// Represents a command to be executed in a IGameState
	/// Operation examples: MovePiece, AddPiece, UpdatePiece, PassTurn, Resign
	/// An operation can also be a sequence of smaller ops
	/// </summary>
	public interface IGameCommand
	{
		void Execute(IGameState state);
	}
	
	public interface IGameBoard : IPieceContainer, ITileGrid
	{
		/// TODO:
		/// Represent boards as directed graphs
		///   with a map (i.e., a functor) to a N-dimension discrete world
		///   and/or another map to a N-dimension continuous world.
		/// These functors may be useful to calculate distances and directions.
		/// All functors should have a reverse functor.
		/// 
		/// Grids (square-based or hex-based) are subtypes of this generic view.
		/// 

		IDirectionSystem DirectionSystem { get; set; }
	}

	public interface IPieceContainer
	{
		IEnumerable<IGamePiece> GetAllPieces();
		IEnumerable<IGamePiece> GetPiecesAt(PointInt pos);

		IGamePiece AddPiece(IGamePiece piece, PointInt posistion);
		IGamePiece MovePiece(IGamePiece piece, PointInt destination);
		//bool RemovePiece(IGamePiece piece);
	}

	public interface ITileGrid
	{
		bool Contains(PointInt position);
		IEnumerable<PointInt> GetAllTiles();
	}

	public interface IDirectionSystem
	{
		//PointInt GetPositionFromPoint(PointReal point);
		//PointReal GetCentralPointFromPosition(PointInt position);

		IEnumerable<IDirection> GetDirections();
		//PointInt Translate(PointInt position, string directionName);
	}

	public interface IDirection
	{
		string Name { get; }
		PointInt Transform(PointInt origin);

		//IDirection Invert();
		//IDirection TurnRight();
		//IDirection TurnLeft();
	}

	public interface IGamePiece : IAugObject
	{
		IGameBoard Board { get; }
		PointInt Position { get; }
		int? Id { get; }

		void SetPosition(IGameBoard board, PointInt position, int id);
	}

	public interface IGamePieceClass : IGamePiece
	{
		IGamePiece CreatePiece();
	}

	public interface IAugObject
	{
		object GetValue(Augmentation.AugProperty property);
		void SetValue(Augmentation.AugProperty property, object value);

		bool IsValueUnset(Augmentation.AugProperty property);
		void ClearValue(Augmentation.AugProperty property);
	}
}
