﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using JpLabs.Extensions;
using JpLabs.Geometry;

namespace Sage
{
	[DataContract]
	public class RectBoard : IGameBoard
	{
		[DataMember(EmitDefaultValue=false)]
		private IDictionary<PointInt,IGamePiece> boardDict;

		[DataMember(EmitDefaultValue=false)]
		private ICollection<IGamePiece> offBoardList;

		[IgnoreDataMember]
		private BoundingBoxInt boundingBox;

		[DataMember(EmitDefaultValue=false)]
		public ushort Cols { get; private set; } // aka Files
		[DataMember(EmitDefaultValue=false)]
		public ushort Rows { get; private set; } // aka Ranks

		[IgnoreDataMember]
		UniqueIdGenerator pieceIdGenerator;

		public RectBoard(ushort columns, ushort rows)
		{
			this.Cols = columns;
			this.Rows = rows;

			SetDefaultFields();
		}

		private void SetDefaultFields()
		{
			pieceIdGenerator = new UniqueIdGenerator();

			if (boardDict == null) boardDict = new Dictionary<PointInt,IGamePiece>();
			if (offBoardList == null) offBoardList = new List<IGamePiece>();
			boundingBox = new BoundingBoxInt(new PointInt(0, 0), new PointInt(Cols - 1, Rows - 1));
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			SetDefaultFields();

			foreach (var keyPair in boardDict) keyPair.Value.SetPosition(this, keyPair.Key, keyPair.Value.Id ?? GetNextId());
			foreach (var piece in offBoardList) piece.SetPosition(this, TilePos.OffBoard, piece.Id ?? GetNextId());

			//TODO: all piece ids should be "known" by the id generator
		}

		[Ninject.Inject]
		[Ninject.Optional]
		IDirectionSystem IGameBoard.DirectionSystem { get; set; }

		public IGamePiece this[PointInt position]
		{
			get {
				//if (position == TilePos.OffBoardTile) return null;

				IGamePiece value;
				return boardDict.TryGetValue(position, out value) ? value : null;
			}
		}

		public bool Contains(PointInt position)
		{
			if (position == TilePos.OffBoard) return true;

			return boundingBox.Contains(position);
		}

		[IgnoreDataMember]
		private IEnumerable<PointInt> allTiles;
		public IEnumerable<PointInt> GetAllTiles()
		{
			if (allTiles == null) allTiles = CreateAllTiles().ToReadOnlyColl();
			return allTiles;
		}
		private IEnumerable<PointInt> CreateAllTiles()
		{
			return
				from c in Enumerable.Range(0, Cols)
				from r in Enumerable.Range(0, Rows)
				select TilePos.Create(c, r);
		}

		public IEnumerable<IGamePiece> GetAllPieces()
		{
			return offBoardList.Concat(boardDict.Values);
		}

		public IGamePiece AddPiece(IGamePiece piece, PointInt position)
		{
			if (boardDict.ContainsKey(position)) throw new ArgumentException("position already taken");
			if (!Contains(position)) throw new ArgumentException("position outside board");

			piece.SetPosition(this, position, GetNextId());
			Add(piece);

			return piece;
		}

		public IGamePiece MovePiece(IGamePiece piece, PointInt destination)
		{
			if (!Contains(piece)) throw new ArgumentException("piece not in board");
			if (!Contains(destination)) throw new ArgumentException("destination outside board");

			Remove(piece);
			piece.SetPosition(this, destination, piece.Id ?? GetNextId());
			Add(piece);

			return piece;
		}

		private bool Remove(IGamePiece piece)
		{
			var position = piece.Position;

			if (position == TilePos.OffBoard) return offBoardList.Remove(piece);
			return boardDict.Remove(position);
		}

		private void Add(IGamePiece piece)
		{
			var position = piece.Position;

			if (position == TilePos.OffBoard) {
				offBoardList.Add(piece);
			} else {
				boardDict.Add(position, piece);
			}
		}

		private bool Contains(IGamePiece piece)
		{
			var position = piece.Position;

			if (position == TilePos.OffBoard) return offBoardList.Contains(piece);
			return this[position] == piece;
		}

		private int GetNextId()
		{
			return pieceIdGenerator.Next();
		}

		public IEnumerable<IGamePiece> GetPiecesAt(PointInt position)
		{
			if (position == TilePos.OffBoard) return offBoardList;//.ToReadOnlyColl();

			var piece = this[position];
			return (piece == null) ? Enumerable.Empty<IGamePiece>() : piece.ToEnumerable();
		}
	}
}
