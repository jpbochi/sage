﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Sage.Augmentation;

namespace Sage
{
	[DataContract]
	public class BasicPlayer : AugObject, IGamePlayer
	{
		protected override AugObject AugParent { get { return null; } }

		[DataMember]
		public int Id { get; private set; }
		
		//[DataMember]public string Name { get; private set; }

		public BasicPlayer(int id)
		{
			this.Id = id;
		}
	}
}
