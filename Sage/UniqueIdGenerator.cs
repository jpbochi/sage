﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sage
{
	public class UniqueIdGenerator
	{
		private readonly Random Rnd = new Random();
		private readonly HashSet<int> Set = new HashSet<int>();

		public int Next()
		{
			while (true) {
				var key = Rnd.Next();

				if (Set.Add(key)) return key;
			}
		}
	}
}
