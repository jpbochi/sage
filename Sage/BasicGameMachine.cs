﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Sage
{
	public class BasicGameMachine : IGameMachine
	{
		private IGameState state;
		private IGameReferee referee;

		private IEnumerable<IGameMoveDescription> moveOptions;

		public BasicGameMachine(IGameState state, IGameReferee referee)
		{
			this.state = state;
			this.referee = referee;
		}

		IGameState IGameMachine.State { get { return state; } }

		public IEnumerable<IGameMoveDescription> GetMoveOptions()
		{
			if (moveOptions == null) moveOptions = referee.GetMoveOptions(state).Select( m => m.Description ).ToArray();

			return moveOptions;
		}

		public void Play(IGameMoveDescription moveDesc)
		{
			var move = referee.GetMoveByDescription(state, moveDesc);
			if (move == null) throw new InvalidOperationException();

			moveOptions = null;

			foreach (var op in move.Resolve(state)) op.Execute(state);
		}
	}
}
