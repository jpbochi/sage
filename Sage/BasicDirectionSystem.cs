﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Extensions;
using JpLabs.Geometry;

namespace Sage
{
	public class BasicDirectionSystem : IDirectionSystem
	{
		public readonly ICollection<IDirection> directions;

		public BasicDirectionSystem(IEnumerable<IDirection> directions)
		{
			this.directions = directions.ToReadOnlyColl();
		}

		public IEnumerable<IDirection> GetDirections()
		{
			return directions;
		}

		public static IDirectionSystem CreateCardinal()
		{
			return new BasicDirectionSystem(
				new [] {
					new VectorDirection("S", new VectorInt(0, -1)),		new VectorDirection("N", new VectorInt(0, 1)),
					new VectorDirection("W", new VectorInt(-1, 0)),		new VectorDirection("E", new VectorInt(1, 0)),
				}
			);
		}

		public static IDirectionSystem CreateCardinalPlusIntercardinal()
		{
			return new BasicDirectionSystem(
				new [] {
					new VectorDirection("S", new VectorInt(0, -1)),		new VectorDirection("N", new VectorInt(0, 1)),
					new VectorDirection("W", new VectorInt(-1, 0)),		new VectorDirection("E", new VectorInt(1, 0)),
					new VectorDirection("SW", new VectorInt(-1, -1)),	new VectorDirection("NE", new VectorInt(1, 1)),
					new VectorDirection("NW", new VectorInt(-1, 1)),	new VectorDirection("SE", new VectorInt(1, -1)),
				}
			);
		}
	}

	public class VectorDirection : IDirection
	{
		public string Name { get; private set; }
		public VectorInt Vector { get; private set; }

		public VectorDirection(string name, VectorInt vector)
		{
			this.Name = name;
			this.Vector = vector;
		}
		
		public PointInt Transform(PointInt origin)
		{
			return origin + Vector;
		}
	}
}
