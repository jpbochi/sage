﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Geometry;
using System.Collections;

namespace Sage
{
	/// ### Complex Move Resolution Example:
	/// Expected operation sequence for a Duellum Shot:
	/// Root: ShootMove {Player, PlayerPuppet, Direction} + implicit params { Weapon, Ammo, FiringAccuracy, Stance, etc. }
	///		- UpdatePuppet
	/// 		- ConsumeCosts
	///				- DecrementAmmo
	///				- SpendTime
	///			- EmitSignals for shot (sight, hearing, smell, touch, and taste)
	///				- Perceive RecordShot
	/// 	- Fire (resolves: AdjustedDirection, TimeCost)
	///			- FireProjectiles (complex simulation of projectile trajectory involved)
	///				- HitPiece, Explode, Incinerate, Teleport, DestroyPiece, CreateDebris, InjureCreature, SpillBlood, etc.
	///				- EmitSignals foreach outcome
	///			- EmitTimeGoingBySignal (Time is a Signal that is universally Perceived by TimeSensors!!)
	///		- ConcludeMove
	///			- PropagateQueuedSignals
	///				- PerceiveSignals foreach Sensor in board
	///			- CheckIsGameOver
	///	.
	///	.
	///	.
	///	http://www.codeproject.com/KB/cs/symbol.aspx
	///	http://www.codeproject.com/KB/dotnet/gointerfaces.aspx

	/// <summary>
	/// Represents a state-independent set of game rules.
	/// It must be capable of telling which are the valid moves for a given game state.
	/// And it will create a sequence of unit operations to be executed when a move option is played.
	/// </summary>
	public interface IGameReferee
	{
		IEnumerable<IGameMove> GetMoveOptions(IGameState state);
	}

	/// <summary>
	/// Represents a description of a move.
	/// It won't tell how the move changes a game. It just express what a player wants to do.
	/// </summary>
	public interface IGameMoveDescription : IAugObject, IStructuralEquatable //, IDictionary<string, object>
	{
		//object this[string key] { get; set; }
		//int Count { get; }
		//bool TryGetValue(string key, out object value);

		//IEnumerable<Tuple<Symbol,object>> GetValues();
		//System.Dynamic.ExpandoObject
		//Tuple<int,double,...>
		//System.Collections.IStructuralEquatable
		//System.Collections.StructuralComparisons
	}

	public interface IGameMove
	{
		IGameMoveDescription Description { get; }

		IEnumerable<IGameCommand> Resolve(IGameState state);
	}

	/// <summary>
	/// Has all the information necessary to play a game (i.e. a State + a Referee).
	/// </summary>
	public interface IGameMachine
	{
		IGameState State { get; }
		//IGameReferee GetReferee();

		IEnumerable<IGameMoveDescription> GetMoveOptions();

		void Play(IGameMoveDescription moveDesc);
	}

	public static class KnownGameOperations
	{
		//public const string StartUp = "StartUp";
		public const string UpdateScore = "UpdateScore";
	}
}
