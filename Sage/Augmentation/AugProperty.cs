﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Sage.Augmentation
{
	/// http://msdn2.microsoft.com/en-us/library/ms752914.aspx = Dependency Properties Overview
	/// http://msdn.microsoft.com/en-us/library/ms745795.aspx = Dependency Property Callbacks and Validation (and Coercion)
	/// http://msdn2.microsoft.com/en-us/library/ms753197.aspx = Property Value Inheritance
	/// http://msdn2.microsoft.com/en-us/library/ms749011.aspx = Attached Properties Overview
	/// http://msdn.microsoft.com/en-us/library/ms742806.aspx = Routed Events Overview
	
	//public struct DependentPropertyChangedEventArgs
	//{
	//    public object NewValue { get; }
	//    public object OldValue { get; }
	//    public DependentProperty Property { get; }
	//}
	//public delegate void PropertyChangedCallback(DependentObject d, DependentPropertyChangedEventArgs e);
	//public class PropMetadata
	//{
	//    public object DefaultValue { get; set; }
	//    //public PropertyChangedCallback PropertyChangedCallback { get; set; }
	//    public PropMetadata(object defaultValue)
	//    {
	//        DefaultValue = defaultValue;
	//    }
	//}
	
	[Flags]
	public enum AugPropertyFlags
	{
		None = 0,
		DoesNotInherit = 1
	}

	public class AugValueChangedEventArgs : EventArgs
	{
		public delegate void EventHandler(AugObject sender, AugValueChangedEventArgs evArgs);
		
		public object NewValue { get; private set; }
		public object OldValue { get; private set; }
		public AugProperty Property { get; private set; }
		
		public AugValueChangedEventArgs(AugProperty property, object oldValue, object newValue)
		{
			this.Property = property;
			this.OldValue = oldValue;
			this.NewValue = newValue;
		}
	}

	public class CancellableAugValueChangedEventArgs : AugValueChangedEventArgs //seealso: System.ComponentModel.CancelEventArgs
	{
		public bool Cancel { get; set; }
		
		public CancellableAugValueChangedEventArgs(AugProperty property, object oldValue, object newValue)
			: base(property, oldValue, newValue)
		{}
	}
	
	#warning //TODO: AugProperties shouldn't be serialized. An identifier of the property should be used as surrogate.
	[DataContract]
	public sealed class AugProperty : IEquatable<AugProperty>
	{
		static public readonly object UnsetValue = new object();
		
		[DataMember(EmitDefaultValue=false)]
		private object DefaultValue;// { get; private set; }
		//private IAugPropertyBehavior defaultBehavior;
		//private IList<KeyValuePair<Type,IAugPropertyBehavior>> behaviorList; //TODO: use this
		
		[IgnoreDataMember]
		private TypeConverter propertyTypeConverter;
		
		[DataMember]public string Name { get; private set; }
		[DataMember]public AugPropertyFlags Flags { get; private set; }

		[DataMember]public string DeclaringTypeName { get; private set; }
		[IgnoreDataMember]public Type PropertyType { get; private set; }
		
		//public bool ReadOnly { get; private set; }
		//public Func<object,bool> ValidateValueCallback { get; private set; }
		//public Func<object,bool> OnSetCoerceValueCallback { get; private set; }
		//public Func<object,bool> OnGetCoerceValueCallback { get; private set; }
		
		public bool InheritsValue { get { return (Flags & AugPropertyFlags.DoesNotInherit) == 0; } }
		
		[IgnoreDataMember]
		private string toString;
		[IgnoreDataMember]
		private int hashCode;

		private AugProperty(string name, Type propType, Type declaringType, AugPropertyFlags flags, object defaultValue)//, IAugPropertyBehavior defaultBehavior)
		{
			Name = name;
			PropertyType = propType;
			DeclaringTypeName = (declaringType == null) ? null : declaringType.FullName;
			DefaultValue = defaultValue;//this.defaultBehavior = defaultBehavior;
			Flags = flags;
			
			InitReadOnlyFields();
		}

		[DataMember(Name="PropertyType")]
		private string SerializablePropertyType
		{
			get { return PropertyType.AssemblyQualifiedName; }
			set { PropertyType = Type.GetType(value, true, false); }
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			InitReadOnlyFields();
		}

		private void InitReadOnlyFields()
		{
			propertyTypeConverter = new TypeConverter(PropertyType);
			toString = string.Concat(DeclaringTypeName, "*", Name);
			hashCode = toString.GetHashCode();
		}

		static public AugProperty Create(string name, Type propType, Type declaringType)
		{
		    return Create(name, propType, declaringType, AugPropertyFlags.None, null);
		}

		static public AugProperty Create(string name, Type propType, Type declaringType, object defaultValue)
		{
			return Create(name, propType, declaringType, AugPropertyFlags.None, defaultValue);
		}

		static public AugProperty Create(string name, Type propType, Type declaringType, AugPropertyFlags flags, object defaultValue)
		{
			//TODO: validate defaultValue

			return new AugProperty(name, propType, declaringType, AugPropertyFlags.None, defaultValue);
		}

		/*
		static public AugProperty Create(string name, Type propType, Type declaringType, object defaultValue)
		{
			IAugPropertyBehavior behavior = BehaviorFromDefaultValue(defaultValue, propType);
			return Create(name, propType, declaringType, AugPropertyFlags.None, behavior);
		}
		static public AugProperty Create(string name, Type propType, Type declaringType, AugPropertyFlags flags, IAugPropertyBehavior defaultBehavior)
		{
			if (defaultBehavior == null) defaultBehavior = new SimpleAugPropBehavior(propType.GetDefaultValue());
			return new AugProperty(name, propType, declaringType, AugPropertyFlags.None, defaultBehavior);
		}
		static private IAugPropertyBehavior BehaviorFromDefaultValue(object defaultValue, Type propType)
		{
			if (defaultValue is IAugPropertyBehavior) return (IAugPropertyBehavior)defaultValue;
			if (!(defaultValue is IAugValueFactory)
			&&  !(defaultValue is IAugValue)
			&&  !propType.IsAssignableFromValue(defaultValue)) throw Error.ArgumentOfIncorrectType(propType, defaultValue, "defaultValue");
			return new SimpleAugPropBehavior(defaultValue);
		}
		public IAugPropertyBehavior GetDefaultBehavior()
		{
			return this.defaultBehavior;
		}
		public IAugPropertyBehavior GetBehavior(Type forType)
		{
			if (behaviorList != null) {
				//return first that is bigger or equal to requested type
				foreach (var kvPair in behaviorList) {
					if (kvPair.Key.IsBiggerOrEqual(forType)) return kvPair.Value;
				}
			}
			//or use default
			return this.defaultBehavior;
		}
		public void OverrideBehavior(Type forType, IAugPropertyBehavior typeBehavior)
		{
		    //TODO: implement this: some of the augProperty behavior may be overridden for specific types
		    throw new NotImplementedException();
		}
		//*/

		public object GetDefaultValue(Type forType)
		{
			//return ConvertValue(this.GetBehavior(forType).GetDefaultValue());
			return ConvertValue(DefaultValue);
		}

		//public bool IsValidValue(object value)
		//{
		//    //TODO: Test value type
		//    //TODO: Call ValidateValue callback, if any
		//    throw new NotImplementedException();
		//}
		//public object CoerceValue(object obj, object value)
		//{
		//    throw new NotImplementedException();
		//}
		
		public override bool Equals(object obj)
		{
			if (obj is AugProperty) return Equals((AugProperty)obj);
			return false;
		}

		public bool Equals(AugProperty other)
		{
			return Name == other.Name && DeclaringTypeName == other.DeclaringTypeName;
		}

		public override int GetHashCode()
		{
			return hashCode;
		}

		public override string ToString()
		{
			return toString;
		}

		public object ConvertValue(object value)
		{
			//if (value is IAugValue) return value;

			return propertyTypeConverter.Convert(value);
		}
	}
}
