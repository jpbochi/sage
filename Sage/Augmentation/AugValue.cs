﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace Sage.Augmentation
{
	[ImmutableObject(true)] //Aspect: all IAugValues should be immutable
	public interface IAugValue
	{
		object GetValue(AugObject owner);
		
		//TODO: consider the following:
		//object GetLocalValue(AugObject owner); //without functionAugValues
		//IAugValue CombineValue(AugObject owner, IAugValue augValue);
	}
	
	internal interface IXSerializable
	{
		XObject ToXml(XName name);
	}
	
	public static class AugValueExt
	{
		static public T GetValue<T>(this IAugValue value, AugObject owner)
		{
			return (T)value.GetValue(owner);
		}
		
		static public IAugValue Create(object value, AugProperty prop)
		{
			//if (value is LambdaExpression)	return FunctionAugValue.Create((LambdaExpression)value, prop);
			//if (value is Delegate)			return FunctionAugValue.Create((Delegate)value, prop);
			return new ConstantAugValue(value);
		}
		
		static public XObject ToXml(this KeyValuePair<AugProperty,IAugValue> pair)
		{
			if (pair.Value is IXSerializable) return ((IXSerializable)pair.Value).ToXml(pair.Key.Name);
			return new XAttribute(pair.Key.Name, pair.Value.ToString());
		}
	}
	
	[ImmutableObject(true)]
	[DataContract]
	internal sealed class ConstantAugValue : IAugValue, IXSerializable
	{
		[DataMember]
		public object Value { get; private set; }

		public object GetValue(AugObject owner) { return Value; }
		
		public ConstantAugValue(object value) { Value = value; }
		
		static public IAugValue Create(object value)
		{
			return new ConstantAugValue(value);
		}

		public override bool Equals(object obj)
		{
			return (obj is ConstantAugValue) && Value == ((ConstantAugValue)obj).Value;
		}

		public override int GetHashCode()
		{
			return (Value == null) ? -1 : Value.GetHashCode();
		}

		public override string ToString()
		{
			return ToString(false);
		}

		public string ToString(bool withQuote)
		{
			var value = (Value == null) ? "null" : Value.ToString();
			return (withQuote) ? string.Concat("'", value, "'") : value;
		}
		
		public XObject ToXml(XName name)
		{
			if (Value is AugObject) return new XElement(name, ((AugObject)Value).ToXml());

			return new XAttribute(name, this.ToString(false));
		}
	}
}
