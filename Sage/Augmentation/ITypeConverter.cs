﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sage.Augmentation
{
	internal interface ITypeConverter
	{
		object Convert(object value);
		object Convert(object value, System.Globalization.CultureInfo cultureInfo);
	}
	
	internal static class TypeConverter<TTargetType>
	{
		public static TypeConverter Converter = new TypeConverter(typeof(TTargetType));
	}

	internal class TypeConverter : ITypeConverter
	{
		//TODO: research TypeConverterAttribute and ValueSerializerAttribute

		private readonly Type targetType;
		private readonly bool isTargetTypeConvertible;
		
		public TypeConverter(Type type)
		{
			if (type == null) throw new ArgumentNullException("type");
			
			targetType = type;
			
			//Convert.ChangeType() does not work with Nullable (e.g. DateTime?, int?)
			if (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				targetType = targetType.GetGenericArguments()[0];
			}
			
			isTargetTypeConvertible = typeof(IConvertible).IsAssignableFrom(type);
		}

		public object Convert(object value)
		{
			return Convert(value, System.Threading.Thread.CurrentThread.CurrentCulture);
		}
		
		public object Convert(object value, System.Globalization.CultureInfo cultureInfo)
		{
			//Try light conversion
			if (isTargetTypeConvertible) return System.Convert.ChangeType(value, targetType, cultureInfo); //might throw a FormatException
			
			//Try equivalent to: return value as Type;
			if (value == null || !targetType.IsAssignableFrom(value.GetType())) return null;
			return value;
		}
	}
}
