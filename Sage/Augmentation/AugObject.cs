﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace Sage.Augmentation
{
	/// <summary>
	/// base class for objects that can have Augmented Properties
	/// </summary>
	[DataContract]
	[KnownType(typeof(ConstantAugValue))]
	[System.Diagnostics.DebuggerDisplay("{ToXml()}")]
	public abstract class AugObject : IAugObject //, INotifyPropertyChanged
	{
		protected abstract AugObject AugParent { get; }
		
		//TODO: implement synchronization //private object propValuesSyncRoot = new object();
		[DataMember(EmitDefaultValue=false)]
		private IDictionary<AugProperty,IAugValue> AugValues; //dictionary of values is lazily created
		
		public IEnumerable<KeyValuePair<AugProperty,IAugValue>> GetLocalValues()
		{
			var propValues = this.AugValues;
			return (
					(propValues != null)
					? propValues.ToArray()
					: Enumerable.Empty<KeyValuePair<AugProperty,IAugValue>>()
				);
		}
		
		public object GetValue(AugProperty ap)
		{
			IAugValue augValue;
			if (TryGetLocalAugValue(ap, out augValue)) return augValue.GetValue(this);

			var value = GetBaseValue(ap);
			if (value != AugProperty.UnsetValue) return value;

			//TODO: implement Behaviors (they can override the default value according to the AugObject's type)
			//var behav = ap.GetBehavior(this.GetType());
			//var defaultValue = behav.GetDefaultValue();
			//value = defaultValue;

			var defaultValue = ap.GetDefaultValue(this.GetType());

			var valueAsAug = defaultValue as IAugValue;
			if (valueAsAug != null) defaultValue = valueAsAug.GetValue(this);

			return ap.ConvertValue(defaultValue);
		}

		public object GetBaseValue(AugProperty ap)
		{
			if (!ap.InheritsValue || AugParent == null) return AugProperty.UnsetValue;

			return AugParent.GetInheritedValue(ap, this);
		}

		public void SetValue(AugProperty ap, IAugValue value)
		{
			SetLocalAugValue(ap, value);
		}
		
		public void SetValue(AugProperty ap, object value)
		{
			//TODO: [or not] validate value (using validate callback, if any) (might be useful for enum properties)
			
			//TODO: [or not] Coerce value (using coerce callback, if any) (what about a coerce event?)
			// Coercing should keep original value. It works like a FunctionAugValue
			
			value = ap.ConvertValue(value);
			
			SetLocalAugValue(ap, ConstantAugValue.Create(value));
		}
		
		//public void CombineValue(AugProperty ap, Delegate func)
		//{
		//    SetLocalAugValue(ap, FunctionAugValue.Create(func, ap));
		//}
		//public void CombineValue(AugProperty ap, LambdaExpression expr)
		//{
		//    SetLocalAugValue(ap, FunctionAugValue.Create(expr, ap));
		//}

		public bool IsValueUnset(AugProperty ap)
		{
			return GetInheritedValue(ap, this) == AugProperty.UnsetValue;
		}

		public void ClearValue(AugProperty ap)
		{
			InternalClearAugValue(ap);
		}
		
		protected virtual IDictionary<AugProperty,IAugValue> CreateAugValueDictionary()
		{
			return new Dictionary<AugProperty,IAugValue>();
		}
		
		private bool TryGetLocalAugValue(AugProperty ap, out IAugValue outValue)
		{
			outValue = null;
			var values = AugValues;
			if (values == null) return false;
			
			return values.TryGetValue(ap, out outValue);
		}

		private void SetLocalAugValue(AugProperty ap, IAugValue value)
		{
			var values = AugValues;
			if (values == null) values = CreateAugValueDictionary();
			AugValues = values;
			
			//object oldValue = this.GetValue(ap, false);
			
			if (IsDefaultValue(ap, value)) {
				//Remove augvalue because it's the same as default
				values.Remove(ap);
			} else {
				//Set (overwriting) the augvalue for the property
				values[ap] = value;
			}
			
			//AugValueChangedEventArgs valueChangedEvArgs = new AugValueChangedEventArgs(ap, oldValue, value.GetValue(this));
			//var behav = ap.GetBehavior(this.GetType());
			//behav.OnValueChanged(this, valueChangedEvArgs);
		}
		
		private bool IsDefaultValue(AugProperty ap, IAugValue value)
		{
			if (!(value is ConstantAugValue)) return false;
			
			//var behav = ap.GetBehavior(this.GetType());
			var defaultValue = ap.GetDefaultValue(this.GetType());//behav.GetDefaultValue();
			
			return ((ConstantAugValue)value).Value == defaultValue;
		}

		private void InternalClearAugValue(AugProperty ap)
		{
			var values = AugValues;
			if (values != null) values.Remove(ap);
		}

		private object GetInheritedValue(AugProperty ap, AugObject originalOwner)
		{
			IAugValue value;
			if (TryGetLocalAugValue(ap, out value)) return value.GetValue(originalOwner);

			return GetBaseValue(ap);
		}

		//event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
		//{
		//    add { throw new NotImplementedException(); }
		//    remove { throw new NotImplementedException(); }
		//}

		#region XML Representation
		
		public virtual XElement ToXml()
		{
		    return this.ToXml(-1);
		}

		public virtual XElement ToXml(int maxSubLevels)
		{
			return ToXml(maxSubLevels, null);
		}

		public virtual string GetTypeName()
		{
			return this.GetType().Name;
		}

		protected virtual XElement ToXml(int maxSubLevels, params XObject[] content)
		{
			var xElem = new XElement(GetTypeName());
			
			if (content != null) xElem.Add(content);
			
			xElem.Add(this.GetLocalValues().Select(pair => pair.ToXml()));
			
			if (maxSubLevels != 0 && this.AugParent != null) xElem.Add(this.AugParent.ToXml(maxSubLevels - 1));
			
			return xElem;
		}
		
		#endregion //*/
	}
}
