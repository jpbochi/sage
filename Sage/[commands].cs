﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Geometry;
using System.Runtime.Serialization;
using Sage.Augmentation;
using System.Collections;

namespace Sage
{
	public abstract class BaseStructuralEquatable : IStructuralEquatable
	{
		public override bool Equals(object obj)
		{
			return StructuralComparisons.StructuralEqualityComparer.Equals(this, obj);
		}

		public override int GetHashCode()
		{
			return GetHashCode(StructuralComparisons.StructuralEqualityComparer);
		}

		public bool Equals(object objOther, IEqualityComparer comparer)
		{
			var other = objOther as BaseStructuralEquatable;
			if (other == null) return false;
			if (!GetType().IsAssignableFrom(other.GetType())) return false;

			return comparer.Equals(GetEquatableSurrogate(), other.GetEquatableSurrogate());
		}

		public int GetHashCode(IEqualityComparer comparer)
		{
			return comparer.GetHashCode(GetEquatableSurrogate());
		}

		protected abstract IStructuralEquatable GetEquatableSurrogate();
	}

	public class AddPieceCommand : BaseStructuralEquatable, IGameCommand
	{
		public IGamePieceClass PieceClass { get; set; }
		public PointInt Position { get; set; }

		public AddPieceCommand(IGamePieceClass pieceClass, PointInt position)
		{
			this.PieceClass = pieceClass;
			this.Position = position;
		}

		public void Execute(IGameState state)
		{
			state.Board.AddPiece(PieceClass.CreatePiece(), Position);
		}

		protected override IStructuralEquatable GetEquatableSurrogate()
		{
			return Tuple.Create(PieceClass, Position);
		}
	}

	public class MovePieceCommand : BaseStructuralEquatable, IGameCommand
	{
		public IGamePiece Piece { get; set; }
		public PointInt Destination { get; set; }

		public MovePieceCommand() {}

		public MovePieceCommand(IGamePiece piece, PointInt destination)
		{
			this.Piece = piece;
			this.Destination = destination;
		}

		public void Execute(IGameState state)
		{
			state.Board.MovePiece(Piece, Destination);
		}

		protected override IStructuralEquatable GetEquatableSurrogate()
		{
			return Tuple.Create(Piece, Destination);
		}
	}

	public class CycleTurnCommand : BaseStructuralEquatable, IGameCommand
	{
		public void Execute(IGameState state)
		{
			state.CurrentPlayerId = GetNextPlayer(state).Id;
		}

		public static IGamePlayer GetNextPlayer(IGameState state)
		{
			var players = state.Players;//.ToArray();

			//*
			using (var it = players.GetEnumerator()) {
				
				bool foundCurrentPlayer = false;

				while (it.MoveNext()) {
					if (foundCurrentPlayer) return it.Current;
					foundCurrentPlayer = it.Current.Id == state.CurrentPlayerId;
				}
			}
			return players.First();
			/*/
			var nextPlayer = players.SkipWhile(p => p.Id != state.CurrentPlayer).Skip(1).FirstOrDefault();
			if (nextPlayer == null) nextPlayer = players.First();
			return nextPlayer;
			//*/
		}

		protected override IStructuralEquatable GetEquatableSurrogate()
		{
			return null;
		}
	}

	public class SetGamePropertyCommand : BaseStructuralEquatable, IGameCommand
	{
		public AugProperty Property { get; private set; }
		public object Value { get; private set; }

		public SetGamePropertyCommand(AugProperty property, object value)
		{
			this.Property = property;
			this.Value = value;
		}

		public void Execute(IGameState state)
		{
			state.SetValue(Property, Value);
		}

		protected override IStructuralEquatable GetEquatableSurrogate()
		{
			return Tuple.Create(Property, Value);
		}
	}

	public class CapturePieceCommand : BaseStructuralEquatable, IGameCommand
	{
		public IGamePiece Piece { get; set; }

		public CapturePieceCommand(IGamePiece piece)
		{
			this.Piece = piece;
		}

		public void Execute(IGameState state)
		{
			state.Board.MovePiece(Piece, TilePos.OffBoard);
		}

		protected override IStructuralEquatable GetEquatableSurrogate()
		{
			return Tuple.Create(Piece);
		}
	}
}
