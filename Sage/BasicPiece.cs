﻿using System;
using System.Runtime.Serialization;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Sage.Augmentation;

namespace Sage
{
	[DataContract]
	public class BasicPiece : AugObject, IGamePiece //System.Dynamic.ExpandoObject
	{
		[IgnoreDataMember]
		public IGameBoard Board { get; private set; }
		[IgnoreDataMember]
		public PointInt Position { get; private set; }
		[DataMember(EmitDefaultValue=false)]
		public int? Id { get; private set; }

		protected override AugObject AugParent { get { return Class; } }

		[DataMember(EmitDefaultValue=false)]
		public BasicPieceClass Class { get; private set; }

		public BasicPiece() {}

		public BasicPiece(BasicPieceClass pieceClass)
		{
			Class = pieceClass;
		}

		void IGamePiece.SetPosition(IGameBoard board, PointInt position, int id)
		{
			if (Board != null && Board != board && board != null) throw new InvalidOperationException("piece already set on another board");

			Board = board;
			Position = position;
			Id = id;
		}
	}

	public class BasicPieceClass : BasicPiece, IGamePieceClass
	{
		public IGamePiece CreatePiece()
		{
			return new BasicPiece(this);
		}
	}

	public static class PieceProperties
	{
		public static AugProperty FlagProperty = AugProperty.Create("Flag", typeof(int?), typeof(PieceProperties));

		public static AugProperty TypeProperty = AugProperty.Create("Type", typeof(Symbol), typeof(PieceProperties));

		public static int? GetFlag(this IGamePiece piece)
		{
			return piece.GetValue<int?>(FlagProperty);
		}

		public static IGamePiece SetFlag(this IGamePiece piece, int flag)
		{
			piece.SetValue(FlagProperty, flag);
			return piece;
		}

		public static void ClearFlag(this IGamePiece piece)
		{
			piece.ClearValue(FlagProperty);
		}

		public static void SetPieceType(this IGamePiece piece, Symbol type)
		{
			piece.SetValue(TypeProperty, type);
		}

		public static Symbol GetPieceType(this IGamePiece piece)
		{
			return piece.GetValue<Symbol>(TypeProperty);
		}
	}
}
