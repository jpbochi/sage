﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sage.Augmentation;
using JpLabs.Extensions;
using System.Runtime.Serialization;

namespace Sage
{
	[DataContract]
	public class BasicGameState : AugObject, IGameState
	{
		public static AugProperty CurrentPlayerIdProperty =
			AugProperty.Create("CurrentPlayerId", typeof(int?), typeof(BasicGameState), null);

		protected override AugObject AugParent { get { return null; } }
		
		[DataMember]
		public IEnumerable<IGamePlayer> Players { get; private set; }

		[DataMember]
		public IGameBoard Board { get; private set; }

		[DataMember]
		public int? CurrentPlayerId
		{
			get { return (int?)GetValue(CurrentPlayerIdProperty); }
			set { SetValue(CurrentPlayerIdProperty, value); }
		}

		[DataMember(EmitDefaultValue=false)]
		public IEnumerable<int> Winners { get; set; }

		public BasicGameState(IEnumerable<IGamePlayer> players, IGameBoard board)
		{
			this.Players = players.EmptyIfNull().ToReadOnlyColl();
			this.Board = board;
		}
	}
}
