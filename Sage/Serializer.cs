﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using JpLabs.Symbols;
using System.Xml;

namespace Sage
{
	//*/
	/// <summary>
	/// Inspired by http://blogs.msdn.com/b/youssefm/archive/2009/06/05/introducing-a-new-datacontractserializer-feature-the-datacontractresolver.aspx
	/// </summary>
	public class SymbolContractResolver : DataContractResolver
	{
		public override bool TryResolveType(Type dataContractType, Type declaredType, DataContractResolver knownTypeResolver, out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace)
		{
			if (typeof(Symbol).IsAssignableFrom(dataContractType)) {
				dataContractType = typeof(Symbol);
			}

			// Defer to the known type resolver
			return knownTypeResolver.TryResolveType(dataContractType, declaredType, null, out typeName, out typeNamespace);
		}

		public override Type ResolveName(string typeName, string typeNamespace, Type declaredType, DataContractResolver knownTypeResolver)
		{
			//if (typeName == "WOOF" && typeNamespace == "http://www.myAnimals.com") return typeof(Symbol);

			// Defer to the known type resolver
			return knownTypeResolver.ResolveName(typeName, typeNamespace, declaredType, null);
		}
	}
	/*/
	/// <summary>
	/// Inspired by http://www.janus-net.de/2008/01/13/wcf-slicing-objects-on-serialization/
	/// </summary>
	class SerializeBaseTypeSurrogate : IDataContractSurrogate
	{
		object IDataContractSurrogate.GetCustomDataToExport(Type clrType, Type dataContractType)
		{
			return null;
		}

		object IDataContractSurrogate.GetCustomDataToExport(System.Reflection.MemberInfo memberInfo, Type dataContractType)
		{
			return null;
		}

		Type IDataContractSurrogate.GetDataContractType(Type type)
		{
			if (typeof(Symbol).IsAssignableFrom(type)) return typeof(Symbol);

			return type;
		}

		object IDataContractSurrogate.GetDeserializedObject(object obj, Type targetType)
		{
			return obj;
		}

		void IDataContractSurrogate.GetKnownCustomDataTypes(Collection<Type> customDataTypes)
		{
			return;
		}

		object IDataContractSurrogate.GetObjectToSerialize(object obj, Type targetType)
		{
			if (typeof(Symbol).IsAssignableFrom(obj.GetType())) return ((Symbol)obj).Clone();

			return obj;
		}

		Type IDataContractSurrogate.GetReferencedTypeOnImport(string typeName, string typeNamespace, object customData)
		{
			return null;
		}

		System.CodeDom.CodeTypeDeclaration IDataContractSurrogate.ProcessImportedType(
			System.CodeDom.CodeTypeDeclaration typeDeclaration,
			System.CodeDom.CodeCompileUnit compileUnit)
		{
			return typeDeclaration;
		}
	}//*/
}
