﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Sage.Augmentation;

namespace Sage
{
	public abstract class MoveType : SymbolEnum
	{
		static MoveType() { SymbolEnum.InitEnum<MoveType>(); }

		[Description("Drop")]
		public static readonly Symbol<MoveType> DropPiece;
		
		[Description("Move")]
		public static readonly Symbol<MoveType> MovePiece;
		
		[Description("Pass Turn")]
		public static readonly Symbol<MoveType> PassTurn;

		///Other moves: Resign, ProposeDraw
	}

	public static class MoveDesc
	{
		public static IEqualityComparer Comparer = StructuralComparisons.StructuralEqualityComparer;

		public static AugProperty MoveTypeProperty
			= AugProperty.Create("MoveType", typeof(Symbol<MoveType>), typeof(MoveDesc)); //should the type be Symbol<MoveType>?

		public static AugProperty PlayerIdProperty
			= AugProperty.Create("PlayerId", typeof(int), typeof(MoveDesc), int.MinValue);

		public static AugProperty PieceIdProperty
			= AugProperty.Create("PieceId", typeof(int), typeof(MoveDesc), -1);

		public static AugProperty OriginProperty
			= AugProperty.Create("Origin", typeof(PointInt), typeof(MoveDesc));

		public static AugProperty DestinationProperty
			= AugProperty.Create("Destination", typeof(PointInt), typeof(MoveDesc));

		public static IGameMoveDescription CreatePassTurnDesc(int playerId)
		{
			var desc = new MoveDescription();
			desc.SetValue(MoveTypeProperty, MoveType.PassTurn);
			desc.SetValue(PlayerIdProperty, playerId);
			return desc;
		}

		public static IGameMoveDescription CreateDropPieceDesc(int playerId, IGamePieceClass pieceClass, PointInt destination)
		{
			var desc = new MoveDescription();
			desc.SetValue(MoveTypeProperty, MoveType.DropPiece);
			desc.SetValue(PlayerIdProperty, playerId);
			desc.SetValue(PieceIdProperty, pieceClass.Id ?? -1);
			desc.SetValue(DestinationProperty, destination);
			return desc;
		}

		public static IGameMoveDescription CreateMovePieceDesc(int playerId, IGamePiece piece, PointInt destination)
		{
			var desc = new MoveDescription();
			desc.SetValue(MoveTypeProperty, MoveType.MovePiece);
			desc.SetValue(PlayerIdProperty, playerId);
			desc.SetValue(PieceIdProperty, piece.Id ?? -1);
			desc.SetValue(OriginProperty, piece.Position);
			desc.SetValue(DestinationProperty, destination);
			return desc;
		}

		public static int GetPlayerId(this IGameMoveDescription desc)
		{
			return desc.GetValue<int>(PlayerIdProperty);
		}

		public static PointInt GetDestination(this IGameMoveDescription desc)
		{
			return desc.GetValue<PointInt>(DestinationProperty);
		}

		public static PointInt GetOrigin(this IGameMoveDescription desc)
		{
			return desc.GetValue<PointInt>(OriginProperty);
		}

		public static int GetPieceId(this IGameMoveDescription desc)
		{
			return desc.GetValue<int>(PieceIdProperty);
		}

		public static IGamePiece GetPiece(this IGameMoveDescription desc, IGameBoard board)
		{
			var id = desc.GetPieceId();
			return board.GetAllPieces().Where(p => p.Id == id).FirstOrDefault();
		}

		public static IGamePieceClass GetPieceClass(this IGameMoveDescription desc, IGameBoard board)
		{
			var id = desc.GetPieceId();
			return board.GetAllPieces().OfType<IGamePieceClass>().Where(p => p.Id == id).FirstOrDefault();
		}

		public static Symbol<MoveType> GetMoveType(this IGameMoveDescription desc)
		{
			return desc.GetValue<Symbol<MoveType>>(MoveTypeProperty);
		}

		public static bool IsType(this IGameMoveDescription desc, Symbol<MoveType> moveType)
		{
			return Symbol.Equals(desc.GetMoveType(), moveType);
		}
	}

	public class MoveDescription : AugObject, IGameMoveDescription, IStructuralEquatable
	{
		protected override AugObject AugParent
		{
			get { return null; }
		}

		public override bool Equals(object obj)
		{
			return StructuralComparisons.StructuralEqualityComparer.Equals(this, obj);
		}

		public override int GetHashCode()
		{
			return GetHashCode(StructuralComparisons.StructuralEqualityComparer);
		}

		public bool Equals(object objOther, IEqualityComparer comparer)
		{
			var other = objOther as MoveDescription;
			if (other == null) return false;

			var otherValues = other.GetLocalValues().ToArray();
			var thisValues = this.GetLocalValues().ToArray();

			if (thisValues.Length != otherValues.Length) return false;

			return thisValues.All( kp => comparer.Equals(kp.Value.GetValue(this), other.GetValue(kp.Key)));
		}

		public int GetHashCode(IEqualityComparer comparer)
		{
			return this.GetLocalValues().Aggregate(0, (hash, keyPair) => hash ^ GetKeyPairHashCode(keyPair, comparer));
		}

		private static int GetKeyPairHashCode<TKey,TValue>(KeyValuePair<TKey,TValue> keyPair, IEqualityComparer comparer)
		{
			return ~comparer.GetHashCode(keyPair.Key) ^ comparer.GetHashCode(keyPair.Value);
		}

		public override string ToString()
		{
			return base.ToXml().ToString();
		}
	}

}
