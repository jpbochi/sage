﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Geometry;
using System.Text.RegularExpressions;

namespace Sage
{
	public static class TilePos
	{
		public const string OffBoardName = "off";
		public static PointInt OffBoard { get { return PointInt.Null; } }

		public static PointInt Create(int col, int row)
		{
			return new PointInt(col, row);
		}

		public static string ToTileName(this PointInt point)
		{
			if (point == OffBoard) return OffBoardName;

			int col = point[0];
			int row = point[1];

			var strCol = TilePos.ToBase26(col);
			var strRow = (row + 1).ToString();

			var digits = Math.Max(strCol.Length, strRow.Length);

			strCol.PadLeft(digits, 'a');
			strRow.PadLeft(digits, '0');

			return string.Concat(strCol, strRow);
		}

		public static PointInt FromName(string tileName)
		{
			if (tileName == OffBoardName) return OffBoard;

			var regex = new Regex("(?<col>[a-z]+)(?<row>[0-9]+)");

			var match = regex.Match(tileName);

			var strCol = match.Groups["col"].Value;
			var strRow = match.Groups["row"].Value;

			int col = ParseBase26(strCol);
			int row = int.Parse(strRow) - 1;

			return new PointInt(col, row);
		}

		private static string ToBase26(int value)
		{
			const int BASE = 26;

			var chars = new Stack<char>();

			while (true)
			{
				int digit = value % BASE;

				chars.Push((char)('a' + digit));

				value /= BASE;
				if (value == 0) break;
				value--;
			}

			return new string(chars.ToArray());
		}

		private static int ParseBase26(string input)
		{
			const int BASE = 26;
			int value = -1;

			foreach (var c in input) {
				if (c < 'a' || c > 'z') throw new ArgumentException();

				value++;
				value *= BASE;
				value += (int)(c - 'a');
			}

			return value;
		}
	}
}
