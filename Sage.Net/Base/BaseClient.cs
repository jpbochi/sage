﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;

namespace Sage.Net.Base
{
	public class BaseClient
	{
		public static ServiceEndpoint CreateServiceEndpoint(Type contractType, Uri hostAddress, bool isDuplex)
		{
			var binding = CreateBinding(hostAddress, isDuplex);
			var endpointAddress = new EndpointAddress(hostAddress);
			return new ServiceEndpoint(ContractDescription.GetContract(contractType), binding, endpointAddress);
		}

		public static Binding CreateBinding(Uri hostAddress, bool isDuplex)
		{
			if (hostAddress.Scheme == Uri.UriSchemeNetPipe) {
				var pipeBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None); //net.pipe://localhost/lobby/

				return pipeBinding;
			} else if (hostAddress.Scheme == Uri.UriSchemeNetTcp) {
				var tcpBinding = new NetTcpBinding(SecurityMode.None) {
					OpenTimeout = TimeSpan.FromSeconds(9),
					CloseTimeout = TimeSpan.FromSeconds(3),
					SendTimeout = TimeSpan.FromSeconds(6000),
					ReceiveTimeout = TimeSpan.MaxValue,
				};

				//tcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromSeconds(8);
				//tcpBinding.ReliableSession.Ordered = true;

				return tcpBinding;
			} else if (hostAddress.Scheme == Uri.UriSchemeHttp) {
				if (isDuplex) {
					var httpBinding = new WSDualHttpBinding(WSDualHttpSecurityMode.None);

					httpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromSeconds(32);
					httpBinding.ReliableSession.Ordered = true;

					return httpBinding;
				} else {
					var httpBinding = new WSHttpBinding(SecurityMode.None);
					return httpBinding;
				}
			} else {
				throw new ArgumentException("Invalid URI scheme");
			}
		}
	}
}
