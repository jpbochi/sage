﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Threading;
using System.ServiceModel.Description;
using JpLabs.Extensions;

namespace Sage.Net.Base
{
	public class BaseServer : IDisposable
	{
		private ServiceHost serviceHost;
		private ICollection<IDisposable> serviceInstances;

		public event EventHandler<ThreadExceptionEventArgs> CommunicationExceptionHappened;

		public CommunicationState State
		{
			get {
				var commObj = serviceHost;
				if (commObj == null) return CommunicationState.Created;
				return commObj.State;
			}
		}

		public Uri NetPipeAddress
		{
			get { return GetAddressWithScheme(Uri.UriSchemeNetPipe); }
		}

		public Uri NetTcpAddress
		{
			get { return GetAddressWithScheme(Uri.UriSchemeNetTcp); }
		}

		public Uri HttpAddress
		{
			get { return GetAddressWithScheme(Uri.UriSchemeHttp); }
		}

		protected ServiceHost CreateServiceHost<TService,TContract>(Func<TService> serviceFactoryFunc, params Uri[] baseAddresses)
			where TService : class, TContract
		{
			return StartHost(typeof(TService), typeof(TContract), serviceFactoryFunc, baseAddresses);
		}

		protected ServiceHost StartHost(Type serviceType, Type serviceContractType, Func<object> serviceFactoryFunc, params Uri[] baseAddresses)
		{
			var state = this.State;
			if (state != CommunicationState.Created)
			if (state != CommunicationState.Closed) throw new InvalidOperationException("Server can't be started at this state");

			this.serviceHost = new ServiceHost(serviceType, baseAddresses);

			if (this.NetPipeAddress != null) {
				var pipeBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
				{
					OpenTimeout = TimeSpan.FromSeconds(4),
					CloseTimeout = TimeSpan.FromSeconds(2),
					SendTimeout = TimeSpan.FromSeconds(4),
					ReceiveTimeout = TimeSpan.MaxValue,
				};

				serviceHost.AddServiceEndpoint(serviceContractType, pipeBinding, string.Empty);
			}

			if (this.NetTcpAddress != null) {
				var tcpBinding = new NetTcpBinding(SecurityMode.None)
				{
					OpenTimeout = TimeSpan.FromSeconds(4),
					CloseTimeout = TimeSpan.FromSeconds(2),
					SendTimeout = TimeSpan.FromSeconds(4),
					ReceiveTimeout = TimeSpan.MaxValue,
				};

				serviceHost.AddServiceEndpoint(serviceContractType, tcpBinding, string.Empty);
			}

			if (this.HttpAddress != null) {
				var httpBinding = new WSHttpBinding(SecurityMode.None); //new WSDualHttpBinding(WSDualHttpSecurityMode.None);
				serviceHost.AddServiceEndpoint(serviceContractType, httpBinding, string.Empty);
			}

			serviceHost.Description.Behaviors.Find<ServiceDebugBehavior>().IncludeExceptionDetailInFaults = true;

			serviceHost.Description.Behaviors.Add(new CustomInstanceProvider(
			    new FactoryInstanceProvider(
					() => Register(serviceFactoryFunc())
				)
			));
			
			//The following two lines are explained here: https://connect.microsoft.com/VisualStudio/feedback/details/565224
			GC.Collect();
			GC.WaitForPendingFinalizers();

			return serviceHost; //.Open();
		}

		public void Close()
		{
			var serviceHost = this.serviceHost;
			this.serviceHost = null;

			if (serviceHost != null) {
				if (serviceHost.State == CommunicationState.Opened) {
					try
					{
						//this.Room.Close(); //BroadcastNow( callback => callback.Dropped() );

						var instances = serviceInstances.EmptyIfNull();
						this.serviceInstances = null;

						foreach (var service in instances) service.Dispose();

						serviceHost.Close();
					} catch (CommunicationException ex) {
						OnCommunicationExceptionHappened(ex);
					}
				}

				if (serviceHost.State != CommunicationState.Faulted) ((IDisposable)serviceHost).Dispose();
			}
		}

		private object Register(object serviceInstance)
		{
			var disposable  = serviceInstance as IDisposable;
			if (disposable != null) serviceInstances.EmptyIfNull().Concat(disposable).ToReadOnlyColl();

			return serviceInstance;
		}

		private void OnCommunicationExceptionHappened(CommunicationException exception)
		{
			System.Diagnostics.Trace.Write(exception, "CommunicationException");

			this.CommunicationExceptionHappened.RaiseEvent(this, new ThreadExceptionEventArgs(exception));
		}

		private IEnumerable<Uri> GetAddresses()
		{
			var host = serviceHost;
			if (host == null) return Enumerable.Empty<Uri>();
			return host.BaseAddresses;
		}

		private Uri GetAddressWithScheme(string scheme)
		{
			return GetAddresses().FirstOrDefault( uri => uri.Scheme == scheme );
		}

		void IDisposable.Dispose()
		{
			Close();
		}
	}
}
