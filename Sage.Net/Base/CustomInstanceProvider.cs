﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Description;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Collections.ObjectModel;

namespace Sage.Net.Base
{
	public class CustomInstanceProvider : IServiceBehavior
	{
		public IInstanceProvider InstanceProvider { get; private set; }

		public CustomInstanceProvider(IInstanceProvider instanceProvider)
		{
			this.InstanceProvider = instanceProvider;
		}

		void IServiceBehavior.AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
		{
		}

		void IServiceBehavior.ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
			foreach (var dispatcher in serviceHostBase.ChannelDispatchers.OfType<ChannelDispatcher>())
			{
				foreach (EndpointDispatcher ed in dispatcher.Endpoints)
				{
					ed.DispatchRuntime.InstanceProvider = this.InstanceProvider;
				}
			}
		}

		void IServiceBehavior.Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{}
	}

	public class FactoryInstanceProvider : IInstanceProvider
	{
		private readonly Func<object> instanceContextFunc;

		public FactoryInstanceProvider(Func<object> instanceContextFunc)
		{
			this.instanceContextFunc = instanceContextFunc;
		}

		object IInstanceProvider.GetInstance(InstanceContext instanceContext, Message message)
		{
			return this.instanceContextFunc();
		}

		object IInstanceProvider.GetInstance(InstanceContext instanceContext)
		{
			return this.instanceContextFunc();
		}

		void IInstanceProvider.ReleaseInstance(InstanceContext instanceContext, object instance)
		{
		}
	}
}
