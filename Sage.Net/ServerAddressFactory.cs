﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace Sage.Net
{
	public class ServerAddressFactory
	{
		public const string LOCALHOST = "127.0.0.1";
		public const string DEFAULT_PATH = "game";
		public const int DEFAULT_HTTP_PORT = 8484;

		public static Uri GetNetPipe(string path)
		{
			return new UriBuilder(Uri.UriSchemeNetPipe, LOCALHOST, -1, path).Uri;
		}

		public static Uri GetNetPipe()
		{
			return GetNetPipe(DEFAULT_PATH);
		}

		public static Uri GetNetTcp(int port, string path)
		{
			return new UriBuilder(Uri.UriSchemeNetTcp, LOCALHOST, port, path).Uri;
		}

		public static Uri GetNetTcp(int port)
		{
			return GetNetTcp(port, DEFAULT_PATH);
		}

		public static Uri GetNetTcp()
		{
			return GetNetTcp(GetFreeTcpPort(), DEFAULT_PATH);
		}

		public static Uri GetHttp(int port, string path)
		{
			return new UriBuilder(Uri.UriSchemeHttp, LOCALHOST, port, path).Uri;
		}

		public static Uri GetHttp(int port)
		{
			return GetHttp(port, DEFAULT_PATH);
		}

		public static Uri GetHttp()
		{
			return GetHttp(DEFAULT_HTTP_PORT, DEFAULT_PATH);
		}

		public static IEnumerable<Uri> GetDefaultAddresses()
		{
			yield return GetNetPipe();
			yield return GetNetTcp();
			yield return GetHttp();
		}

		public static int GetFreeTcpPort()
		{
			TcpListener l = new TcpListener(IPAddress.Loopback, 0);
			try {
				l.Start();
				return ((IPEndPoint)l.LocalEndpoint).Port;
			} finally {
				l.Stop();
			}
		}
	}
}
