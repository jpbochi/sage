﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace Sage.DemoClient
{
	internal static class DispatcherExt
	{
		public static bool TryInvoke(this Dispatcher dispatcher, Action dlg)
		{
			if (dispatcher.CheckAccess()) return false;
			InternalInvoke(dispatcher, dlg);
			return true;
		}

		public static bool TryInvoke<T>(this Dispatcher dispatcher, Action<T> dlg, T arg)
		{
			if (dispatcher.CheckAccess()) return false;
			InternalInvoke(dispatcher, dlg, arg);
			return true;
		}

		public static bool TryInvoke<T1,T2>(this Dispatcher dispatcher, Action<T1,T2> dlg, T1 arg1, T2 arg2)
		{
			if (dispatcher.CheckAccess()) return false;
			InternalInvoke(dispatcher, dlg, arg1, arg2);
			return true;
		}

		public static DispatcherOperation BeginInvoke(this Dispatcher dispatcher, DispatcherPriority priority, Action dlg)
		{
			return InternalBeginInvoke(dispatcher, priority, dlg);
		}

		public static DispatcherOperation BeginInvoke<T>(this Dispatcher dispatcher, DispatcherPriority priority, Action<T> dlg, T arg)
		{
			return InternalBeginInvoke(dispatcher, priority, dlg, arg);
		}

		public static DispatcherOperation BeginInvoke<T1,T2>(this Dispatcher dispatcher, DispatcherPriority priority, Action<T1,T2> dlg, T1 arg1, T2 arg2)
		{
			return InternalBeginInvoke(dispatcher, priority, dlg, arg1, arg2);
		}

		public static void CheckAndInvoke(this Dispatcher dispatcher, Action dlg)
		{
			InternalCheckAndInvoke(dispatcher, dlg);
		}

		public static void CheckAndInvoke<T>(this Dispatcher dispatcher, Action<T> dlg, T arg)
		{
			InternalInvoke(dispatcher, dlg, arg);
		}

		public static void CheckAndInvoke<T1,T2>(this Dispatcher dispatcher, Action<T1,T2> dlg, T1 arg1, T2 arg2)
		{
			InternalCheckAndInvoke(dispatcher, dlg, arg1, arg2);
		}

		public static TOut CheckAndInvoke<TOut>(this Dispatcher dispatcher, Func<TOut> dlg)
		{
			return (TOut)InternalCheckAndInvoke(dispatcher, dlg);
		}

		public static TOut CheckAndInvoke<T,TOut>(this Dispatcher dispatcher, Func<T,TOut> dlg, T arg)
		{
			return (TOut)InternalCheckAndInvoke(dispatcher, dlg, arg);
		}

		public static TOut CheckAndInvoke<T1,T2,TOut>(this Dispatcher dispatcher, Func<T1,T2,TOut> dlg, T1 arg1, T2 arg2)
		{
			return (TOut)InternalCheckAndInvoke(dispatcher, dlg, arg1, arg2);
		}

		private static object InternalInvoke(Dispatcher dispatcher, Delegate dlg, params object[] args)
		{
				return dispatcher.Invoke(dlg, DispatcherPriority.Normal, args);
		}

		private static DispatcherOperation InternalBeginInvoke(Dispatcher dispatcher, DispatcherPriority priority, Delegate dlg, params object[] args)
		{
			return dispatcher.BeginInvoke(dlg, priority, args);
		}

		private static object InternalCheckAndInvoke(Dispatcher dispatcher, Delegate dlg, params object[] args)
		{
			if (!dispatcher.CheckAccess()) return dispatcher.Invoke(dlg, DispatcherPriority.Normal, args);
			
			return dlg.DynamicInvoke(args); //TODO:[optimize] DynamicInvoke is slow
		}
	}
}
