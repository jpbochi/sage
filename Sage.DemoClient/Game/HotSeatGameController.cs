﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using JpLabs.Geometry;
using System.Diagnostics;

namespace Sage.DemoClient.Game
{
	internal interface IGameContext
	{
		IGameMachine Game { get; }

		PointInt SelectedPosition { get; }
	}

	internal class HotSeatGameController
	{
		class GameContext : IGameContext
		{
			public IGameMachine Game { get; set; }

			public PointInt SelectedPosition { get; set; }

			public GameContext(IGameMachine game)
			{
				this.Game = game;
			}
		}

		private GamePage gamePage;
		private IGameMachine game;
		private PointInt selectedPos;

		public HotSeatGameController(IGameMachine game)
		{
			this.game = game;
		}

		public Func<IGameMachine> RestartGameFunc { get; set; }

		public void BindToView(GamePage gamePage, GameView gameView)
		{
			this.gamePage = gamePage;

			gamePage.CommandBindings.Add(
				new CommandBinding(GameCommands.Create, (s,e) => { RestartGame(); })
			);

			//gamePage.RestartClicked += GamePage_RestartClicked;
			gameView.TileClicked += GameView_TileClicked;

			gamePage.CommandBindings.Add(
				new CommandBinding(GameCommands.PlayMove, PlayMove_Executed)
			);

			UpdateView();
		}

		private void RestartGame()
		{
			var funcCreateNewGame = RestartGameFunc;
			if (funcCreateNewGame == null) return;
			game = funcCreateNewGame();

			UpdateView();
		}

		private void PlayMove_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			var move = e.Parameter as IGameMoveDescription;

			game.Play(move);

			UpdateView();
		}

		void GameView_TileClicked(object sender, TileClickedEventArgs e)
		{
			var debugWatch = Stopwatch.StartNew();

			var state = game.State;
			if (state.IsOver()) return;

			var moves = game.GetMoveOptions().ToArray();

			var dropMove  = moves.Where( m => m.IsType(MoveType.DropPiece) && m.GetDestination() == e.Position ).FirstOrDefault();
			if (dropMove != null) {
				game.Play(dropMove);
				selectedPos = TilePos.OffBoard;
			} else {
				var moveWithPieceAtClick = moves.Where(
					m => m.IsType(MoveType.MovePiece) && m.GetOrigin() == e.Position
				).FirstOrDefault();

				if (moveWithPieceAtClick != null) {
					selectedPos = e.Position;
				} else {
					
					var move = moves.Where(
						m => m.IsType(MoveType.MovePiece) && m.GetOrigin() == selectedPos && m.GetDestination() == e.Position
					).FirstOrDefault();

					game.Play(move);

					selectedPos = TilePos.OffBoard;
				}
			}

			debugWatch.Stop();
			Debug.Print("Move played in {0}ms", debugWatch.Elapsed.TotalMilliseconds);

			debugWatch = Stopwatch.StartNew();
			UpdateView();
			debugWatch.Stop();
			Debug.Print("View updated in {0}ms", debugWatch.Elapsed.TotalMilliseconds);
		}

		private void UpdateView()
		{
			gamePage.DataContext = new GameContext(game) { SelectedPosition = selectedPos };
		}
	}
}
