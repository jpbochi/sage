﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using JpLabs.Extensions;

namespace Sage.DemoClient.Game
{
	/// <summary>
	/// Interaction logic for GamePage.xaml
	/// </summary>
	public partial class GamePage : Page
	{
		public GamePage()
		{
			InitializeComponent();
		}

		public void SetGameView(UIElement view)
		{
			contentGameView.Content = view;
		}

		public void SetPlayerView(UIElement view)
		{
			contentPlayerView.Content = view;
		}
	}
}
