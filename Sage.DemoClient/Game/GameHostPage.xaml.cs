﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sage.Net.Game.Server;
using System.ServiceModel;
using System.Windows.Threading;

namespace Sage.DemoClient.Game
{
	/// <summary>
	/// Interaction logic for GameHostPage.xaml
	/// </summary>
	public partial class GameHostPage : Page
	{
		public GameHostPage()
		{
			InitializeComponent();

			txtLogArea.Document.LineHeight = 1;
		}

		public DispatcherOperation BeginAddLogText(string text)
		{
			return Dispatcher.BeginInvoke(DispatcherPriority.Normal, AddLogText, text);
		}

		public void AddLogText(string text)
		{
			if (Dispatcher.TryInvoke(AddLogText, text)) return;

			txtLogArea.AppendText(text + Environment.NewLine);
			txtLogArea.ScrollToEnd();
		}
	}
}
