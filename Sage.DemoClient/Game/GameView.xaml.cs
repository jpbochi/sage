﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using JpLabs.Geometry;

namespace Sage.DemoClient.Game
{
	/// <summary>
	/// Interaction logic for TicTacToeView.xaml
	/// </summary>
	public partial class GameView : UserControl
	{
		static public readonly RoutedEvent TileClickedEvent = EventManager
		.RegisterRoutedEvent(
			"TileClicked",
			RoutingStrategy.Bubble,
			typeof(EventHandler<TileClickedEventArgs>),
			typeof(GameView)
		);

		public GameView()
		{
			InitializeComponent();

			this.DataContextChanged += GameView_DataContextChanged;
		}

		public event EventHandler<TileClickedEventArgs> TileClicked
		{
			add { AddHandler(TileClickedEvent, value); } 
			remove { RemoveHandler(TileClickedEvent, value); }
		}

		void GameView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			UpdateGame();
		}

		public void UpdateGame()
		{
			var context = this.DataContext as IGameContext;
			if (context == null) return;

			DrawBoard(context.Game.State.Board);

			DrawAvailableMoves(context.Game, context.SelectedPosition);
		}

		private void EnsureGridUIIsConstructed(RectBoard board)
		{
			int tileCount = board.Cols * board.Rows;

			if (grid.Children.Count != tileCount) {
				grid.Children.Clear();

				grid.RowDefinitions.Clear();
				grid.ColumnDefinitions.Clear();

				for (int i=0; i<board.Rows; i++) grid.RowDefinitions.Add(new RowDefinition());
				for (int i=0; i<board.Cols; i++) grid.ColumnDefinitions.Add(new ColumnDefinition());

				var tiles = board.GetAllTiles();
					//from c in Enumerable.Range(0, board.Cols)
					//from r in Enumerable.Range(0, board.Rows)
					//select TilePos.Create(c, r);

				foreach (var tile in tiles) {
					var button = new Button() { Margin = new Thickness(2), FontSize = 16 };

					Grid.SetColumn(button, tile.X);
					Grid.SetRow(button, tile.Y);

					button.Click += TileButton_Click;

					grid.Children.Add(button);
				}
			}
		}

		private void DrawBoard(IGameBoard board)
		{
			EnsureGridUIIsConstructed((RectBoard)board);

			var tileButtons = grid.Children.Cast<Button>();

			foreach (var tileButton in tileButtons) {
				var pos = GetTilePos(tileButton);

				tileButton.Content = CreateTileContent(board, pos);
			}
		}

		private string CreateTileContent(IGameBoard board, PointInt pos)
		{
			var piece = board.GetPiecesAt(pos).FirstOrDefault();

			return (piece == null) ? string.Empty : piece.GetDisplayText();
		}

		private void DrawAvailableMoves(IGameMachine game, PointInt selectedPosition)
		{
			var tileButtons = grid.Children.Cast<Button>();
			foreach (var tileButton in tileButtons) tileButton.IsEnabled = false;

			var moves = game.GetMoveOptions();

			foreach (var move in moves) DrawMoveOption(move);

			DrawSelectedMoveAction(moves.Where( m => m.IsType(MoveType.MovePiece) && m.GetOrigin() == selectedPosition ));
		}

		private void DrawMoveOption(IGameMoveDescription moveDesc)
		{
			if (moveDesc.IsType(MoveType.DropPiece)) DrawDropAction(moveDesc);
			else
			if (moveDesc.IsType(MoveType.MovePiece)) DrawMoveActionSource(moveDesc);
		}

		private void DrawDropAction(IGameMoveDescription dropMove)
		{
			var button = GetTileButton(dropMove.GetDestination(), grid);

			button.Content = string.Concat(button.Content, ".");
			button.IsEnabled = true;
		}

		private void DrawMoveActionSource(IGameMoveDescription movePieceMove)
		{
			var piecePosition = movePieceMove.GetOrigin();

			var buttonAtPiece = GetTileButton(piecePosition, grid);
			buttonAtPiece.IsEnabled = true;
		}

		private void DrawSelectedMoveAction(IEnumerable<IGameMoveDescription> moves)
		{
			foreach (var drop in moves.Select( m => m.GetDestination() )) {
				var button = GetTileButton(drop, grid);

				button.Content = string.Concat(button.Content, ".");
				button.IsEnabled = true;
			}
		}

		private void TileButton_Click(object sender, RoutedEventArgs e)
		{
			var tileButton = (UIElement)sender;

			var tilePos = GetTilePos(tileButton);

			this.RaiseEvent(new TileClickedEventArgs(tilePos, TileClickedEvent));
		}

		private static PointInt GetTilePos(UIElement tileControl)
		{
			return TilePos.Create(Grid.GetColumn(tileControl), Grid.GetRow(tileControl));
		}

		private static Button GetTileButton(PointInt position, Grid grid)
		{
			return grid.Children.OfType<Button>().Where( btn => GetTilePos(btn) == position ).FirstOrDefault();
		}
	}

	public class TileClickedEventArgs : RoutedEventArgs
	{
		public TileClickedEventArgs(PointInt pos, RoutedEvent routedEvent) : base(routedEvent)
		{
			this.Position = pos;
		}

		public PointInt Position { get; private set; }
	}
}
