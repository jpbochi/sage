﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Sage.DemoClient.Game
{
	internal static class GameCommands
	{
		// Nested Types
		private enum CommandId
		{
			//NewGame, ConnectToGame, InitializeGame,
			Create, Start, PlayMove
		}

		// Fields
		private static RoutedCommand[] _internalCommands = new RoutedCommand[Enum.GetValues(typeof(CommandId)).Length];

		// Methods
		private static RoutedCommand EnsureCommand(CommandId idCommand)
		{
			lock (_internalCommands.SyncRoot)
			{
				if (_internalCommands[(int) idCommand] == null)
				{
					string name = idCommand.ToString();
					
					//string text = idCommand.ToDescription();					
					//var command = new RoutedUICommand(text, name, typeof(GameCommands));
					
					var command = new RoutedCommand(name, typeof(GameCommands));
					
					_internalCommands[(int) idCommand] = command;
				}
			}
			return _internalCommands[(int) idCommand];
		}

		//private static string GetPropertyName(CommandId commandId)
		//{ return commandId.ToDescription(); }

		// Properties
		//public static RoutedCommand New { get { return EnsureCommand(CommandId.NewGame); } }
		//public static RoutedCommand ConnectTo { get { return EnsureCommand(CommandId.ConnectToGame); } }
		//public static RoutedCommand Initialize { get { return EnsureCommand(CommandId.InitializeGame); } }
		public static RoutedCommand Create { get { return EnsureCommand(CommandId.Create); } }
		public static RoutedCommand Start { get { return EnsureCommand(CommandId.Start); } }
		public static RoutedCommand PlayMove { get { return EnsureCommand(CommandId.PlayMove); } }
	}
}
