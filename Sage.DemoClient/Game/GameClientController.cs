﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sage.Net.Game.Client;
using JpLabs.Geometry;
using System.ServiceModel;
using System.Diagnostics;

namespace Sage.DemoClient.Game
{
	internal class GameClientController : IDisposable
	{
		class GameContext : IGameContext
		{
			private readonly GameClient client;

			public GameContext(GameClient client)
			{
				this.client = client;
			}

			IGameMachine IGameContext.Game
			{
				get { return client; }
			}

			public PointInt SelectedPosition
			{
				get; set;
			}
		}

		private readonly GameClient gameClient;
		private GamePage gamePage;
		private PointInt selectedPos;

		public Uri RemoteHostAddress { get; set; }

		public GameClientController(GameClient gameClient)
		{
			this.gameClient = gameClient;
		}

		public CommunicationState ClientState
		{
			get {
				var client = this.gameClient;
				if (client == null) return CommunicationState.Closed;
				return client.State;
			}
		}

		public void BindToView(GamePage gamePage, GameView gameView)
		{
			this.gamePage = gamePage;

			gameView.TileClicked += GameView_TileClicked;

			if (!gameClient.IsConnected()) gameClient.Connect(RemoteHostAddress);

			UpdateView();
		}

		void GameView_TileClicked(object sender, TileClickedEventArgs e)
		{
			var game = (IGameMachine)gameClient;

			var debugWatch = Stopwatch.StartNew();

			var state = game.State;
			if (state.IsOver()) return;

			var moves = game.GetMoveOptions().ToArray();

			var dropMove  = moves.Where( m => m.IsType(MoveType.DropPiece) && m.GetDestination() == e.Position ).FirstOrDefault();
			if (dropMove != null) {
				game.Play(dropMove);
				selectedPos = TilePos.OffBoard;
			} else {
				var moveWithPieceAtClick = moves.Where(
					m => m.IsType(MoveType.MovePiece) && m.GetOrigin() == e.Position
				).FirstOrDefault();

				if (moveWithPieceAtClick != null) {
					selectedPos = e.Position;
				} else {
					
					var move = moves.Where(
						m => m.IsType(MoveType.MovePiece) && m.GetOrigin() == selectedPos && m.GetDestination() == e.Position
					).FirstOrDefault();

					game.Play(move);

					selectedPos = TilePos.OffBoard;
				}
			}

			debugWatch.Stop();
			Debug.Print("Move played in {0}ms", debugWatch.Elapsed.TotalMilliseconds);

			debugWatch = Stopwatch.StartNew();
			UpdateView();
			debugWatch.Stop();
			Debug.Print("View updated in {0}ms", debugWatch.Elapsed.TotalMilliseconds);
		}

		public void Dispose()
		{
			var client = this.gameClient;
			if (client != null) client.Dispose();

			//this.gameClient = null; //I want to reuse clients
		}

		private void UpdateView()
		{
			gamePage.DataContext = new GameContext(gameClient) { SelectedPosition = selectedPos };
		}
	}
}
