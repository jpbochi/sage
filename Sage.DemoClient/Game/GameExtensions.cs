﻿using JpLabs.Symbols;
using Sage.Augmentation;
using Sage.Games.Amazons;
using Sage.Games.Go;
using Sage.Games.TicTacToe;

namespace Sage.DemoClient.Game
{
	public static class PlayerExt
	{
		public static AugProperty NameProperty =
			AugProperty.Create(
				"Name", typeof(string), typeof(PlayerExt), "[unnamed]"
			);

		public static string GetName(this IGamePlayer player)
		{
			return player.GetValue<string>(NameProperty);
		}

		public static IGamePlayer SetName(this IGamePlayer player, string name)
		{
			player.SetValue(NameProperty, name);
			return player;
		}

		public static bool IsNameUnset(this IGamePlayer player)
		{
			return player.IsValueUnset(NameProperty);
		}

		public static void ClearName(this IGamePlayer player)
		{
			player.ClearValue(NameProperty);
		}
	}

	public static class PieceExt
	{
		public static string GetDisplayText(this IGamePiece piece)
		{
			if (piece == null) return ".";

			var pieceType = piece.GetPieceType();
			var pieceFlag = piece.GetFlag();

			if (pieceType == GoPieces.Stone) {
				return (pieceFlag == ClassicPlayer.Black.GetId()) ? "X"
					 : (pieceFlag == ClassicPlayer.White.GetId()) ? "O"
					 : "?";
			}

			if (pieceType.Is<AmazonPieces>()) {
				return (pieceType == AmazonPieces.Arrow) ? "#"
					 : (pieceFlag == ClassicPlayer.White.GetId()) ? "W"
					 : (pieceFlag == ClassicPlayer.Black.GetId()) ? "B"
					 : "?";
			}

			return (piece.GetFlag() == TicTacToePlayer.X.GetId()) ? "X"
				 : (piece.GetFlag() == TicTacToePlayer.O.GetId()) ? "O"
				 : "?";
		}
	}
}
