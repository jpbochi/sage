﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using JpLabs.Extensions;

namespace Sage.DemoClient.Game
{
	/// <summary>
	/// Interaction logic for PlayerView.xaml
	/// </summary>
	public partial class PlayerView : UserControl
	{
		static public readonly RoutedEvent MoveClickedEvent = EventManager
		.RegisterRoutedEvent(
			"MoveClicked",
			RoutingStrategy.Bubble,
			typeof(EventHandler<MoveClickedEventArgs>),
			typeof(PlayerView)
		);

		public PlayerView()
		{
			InitializeComponent();

			//TODO: Build a Lambda-To-Binding tool
			//lblPlayerName.SetBinding(TextBlock.TextProperty, "Game.CurrentPlayer.Name");

			this.DataContextChanged += PlayerView_DataContextChanged;
			//this.LayoutUpdated += PlayerView_LayoutUpdated;
			//this.SourceUpdated += new EventHandler<DataTransferEventArgs>(PlayerView_SourceUpdated);
		}

		public event EventHandler<MoveClickedEventArgs> TileClicked
		{
			add { AddHandler(MoveClickedEvent, value); } 
			remove { RemoveHandler(MoveClickedEvent, value); }
		}

		void PlayerView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			UpdateUI();
		}

		void UpdateUI()
		{
			//bool isGameOver = (DataContext is Mage.IGame);
			//linePlayerName.Visibility = (isGameOver) ? Visibility.Collapsed : Visibility.Visible;
			//lblGameOver.Visibility = (isGameOver) ? Visibility.Visible : Visibility.Collapsed;

			var context = DataContext as IGameContext;
			if (context == null) return;

			var state = context.Game.State;

			bool isGameOver = state.IsOver();

			ShowHide(linePlayerName, !isGameOver);
			ShowHide(lblGameOver, isGameOver);
			ShowHide(lblWinner, isGameOver);

			if (!isGameOver) {
				lblPlayerName.Text = state.GetCurrentPlayer().GetName();

				DisplaySpecialMoves(context.Game);
			} else {
				var winners = state.GetWinners();

				lblWinner.Text = (winners.Count() != 1) ? "Draw" : string.Format("{0} Won", winners.First().GetName());
			}
		}

		private void DisplaySpecialMoves(IGameMachine game)
		{
			var specialMoves = game.GetMoveOptions().Where(m => m.IsValueUnset(MoveDesc.DestinationProperty));

			holderSpecialMoves.Children.Clear();

			foreach (var move in specialMoves) {
				
				var moveType = move.GetMoveType();
				var descAttr = moveType.GetAttributeProvider().GetSingleAttrOrNull<DescriptionAttribute>(true);

				string moveName = (descAttr != null) ? descAttr.Description : moveType.Name;

				var button = new Button() { Margin = new Thickness(2), Content = moveName };

				//button.Click += (
				//    (s,e) => {
				//        //game.Play(move);
				//        //this.RaiseEvent(new MoveClickedEventArgs(move, MoveClickedEvent));
				//        GameCommands.PlayMove.Execute(
				//    }
				//);

				button.Command = GameCommands.PlayMove;
				button.CommandParameter = move;

				holderSpecialMoves.Children.Add(button);
			}
		}

		private static void ShowHide(UIElement element, bool visible)
		{
			element.Visibility = (visible) ? Visibility.Visible : Visibility.Collapsed;
		}
	}

	public class MoveClickedEventArgs : RoutedEventArgs
	{
		public MoveClickedEventArgs(IGameMoveDescription moveDesc, RoutedEvent routedEvent) : base(routedEvent)
		{
			this.MoveDesc = moveDesc;
		}

		public IGameMoveDescription MoveDesc { get; private set; }
	}
}
