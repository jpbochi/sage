﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sage.Net.Game.Server;
using System.ServiceModel;
using Sage;
using System.Threading;
using JpLabs.TweakedEvents;
using System.Windows;
using JpLabs.Extensions;
using Sage.Net;
using System.Threading.Tasks;

namespace Sage.DemoClient.Game
{
	internal class GameHostController : IDisposable
	{
		private readonly IGameMachine game;
		private GameServer gameServer;
		private GameHostPage view;

		public Uri HostAddress { get; set; }

		public GameHostController(IGameMachine game)
		{
			this.game = game;

			AttachEvents(game);
		}

		public CommunicationState ServerState
		{
			get {
				var server = this.gameServer;
				if (server == null) return CommunicationState.Closed;
				return server.State;
			}
		}

		public void BindToView(GameHostPage gameHostPage)
		{
			this.view = gameHostPage;

			view.btnStartStop.Click += btnStartStop_Click;

			UpdateView();
		}

		public void Dispose()
		{
			var disposable = gameServer as IDisposable;
			if (disposable != null) disposable.Dispose();

			this.gameServer = null;
		}

		public void StartServer()
		{
			StartServer(this.HostAddress);
		}

		private void btnStartStop_Click(object sender, RoutedEventArgs e)
		{
			var state = this.ServerState;
			switch (state)
			{
				case CommunicationState.Closed: StartServer(this.HostAddress); break;
				case CommunicationState.Created: break;
				default: StopServer(); break;
			}

			UpdateView();
		}

		private void StartServer(Uri address)
		{
			this.gameServer = new GameServer(game);

			//AttachEvents(this.LobbyServer);

			var baseAddresses = address.ToEnumerable().Union(ServerAddressFactory.GetNetPipe()).ToArray();

			view.AddLogText("Starting server...");
			UpdateView();

			Task.Factory.StartNew(
			    () => gameServer.StartServer(baseAddresses)
			)
			.ContinueWith(
			    t => {
			        if (t.IsFaulted) {
			            view.AddLogText("Error starting server: " + t.Exception.InnerException.Message);
			        } else {
			            view.AddLogText("Server open at " + address.ToString());
			        }
			        UpdateView();
			    }
			);
		}

		private void StopServer()
		{
			view.AddLogText("Stopping server...");

			Dispose();

			view.AddLogText("Server stopped");
		}

		private void UpdateView()
		{
			if (view.Dispatcher.TryInvoke(UpdateView)) return;

			view.btnStartStop.IsEnabled = false;

			var state = ServerState;
			switch (state)
			{
				case CommunicationState.Closed: {
					view.btnStartStop.Content = "Start Server";
					view.btnStartStop.IsEnabled = true;
				} break;

				case CommunicationState.Created:
				case CommunicationState.Opening: {
					view.btnStartStop.Content = "Starting...";
				} break;

				case CommunicationState.Opened: {
					view.btnStartStop.Content = "Stop Server";
					view.btnStartStop.IsEnabled = true;
				} break;

				case CommunicationState.Faulted: {
					view.btnStartStop.Content = "Faulted - Dispose";
					view.btnStartStop.IsEnabled = true;
				} break;

				default: {
					view.btnStartStop.Content = state.ToString();
				} break;
			}
		}

		private void AttachEvents(GameServer server)
		{
			server.CommunicationExceptionHappened += TweakedEvent.ToWeak<ThreadExceptionEventArgs>(GameServer_CommunicationExceptionHappened);
		}

		private void AttachEvents(IGameMachine game)
		{
			//var basicGame = (Sage.BasicGameMachine)game;
			//basicGame.ActionPlayed += TweakedEvent.ToWeak(Game_ActionPlayed);
		}

		void GameServer_CommunicationExceptionHappened(object sender, ThreadExceptionEventArgs e)
		{
			view.BeginAddLogText(string.Format("* Exception: {0}", e.Exception.Message));
		}

		/*void Game_ActionPlayed(object sender, EventArgs e)
		{
			view.BeginAddLogText("* Action played");

			var player = game.Get;

			if (player != null) {
				view.BeginAddLogText("* Current player is " + player.Name);
			} else {
				view.BeginAddLogText("* Game Over");

				var winners = game.GetWinners();
				view.BeginAddLogText((winners.Count() != 1) ? "Draw" : string.Format("{0} Won", winners.First().Name));
			}
		}//*/
	}
}
