﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sage.DemoClient.Game;
using Sage.Games;
using JpLabs.Extensions;

namespace Sage.DemoClient
{
	/// <summary>
	/// Interaction logic for MainPage.xaml
	/// </summary>
	public partial class MainPage : Page
	{
		enum KnownGames
		{
			TicTacToe, Amazons, Go
		}

		class PlayerData
		{
			public string Color { get;  set; }
			public string Name { get;  set; }
		}

		public MainPage()
		{
			InitializeComponent();

			cmbGameType.ItemsSource = Enum.GetValues(typeof(KnownGames));
			cmbGameType.SelectedValue = KnownGames.TicTacToe;

			gridPlayers.ItemsSource = new [] {
				new PlayerData() { Color = "White", Name = "Fisrt" },
				new PlayerData() { Color = "Black", Name = "Second" },
			};
		}

		private Uri GetGameAddress()
		{
			return new Uri(txtGameHostAddress.Text);
		}

		private void StartHotSeatGame_Click(object sender, RoutedEventArgs e)
		{
			var game = SetupNewGame();
			StartHotSeatGame(game);
		}

		private IGameMachine SetupNewGame()
		{
			var game = CreateGame();
			StartUpGame(game.State);
			return game;
		}

		private IGameMachine CreateGame()
		{
			switch ((KnownGames)cmbGameType.SelectedValue)
			{
				case KnownGames.TicTacToe:	return GameFactory.Get<Sage.Games.TicTacToe.TicTacToeGameModule>();
				case KnownGames.Amazons:	return GameFactory.Get<Sage.Games.Amazons.AmazonsGameModule>();
				case KnownGames.Go:			return GameFactory.Get<Sage.Games.Go.GoGameModule>();
				default: throw new NotImplementedException();
			}
		}

		private void StartUpGame(IGameState state)
		{
			var playerData = gridPlayers.ItemsSource.Cast<PlayerData>();

			if (chkShufflePlayers.IsChecked ?? false) playerData = playerData.Shuffle();

			var zip = state.Players.Zip(playerData, (player,data) => Tuple.Create(player,data));

			foreach (var t in zip) {
				t.Item1.SetName(t.Item2.Name);
			}
		}

		private void StartHotSeatGame(IGameMachine game)
		{
			var gamePage = new GamePage();
			var gameView = new GameView();
			var playerView = new PlayerView();
			gamePage.SetGameView(gameView);
			gamePage.SetPlayerView(playerView);

			var controller = new HotSeatGameController(game);
			controller.RestartGameFunc = SetupNewGame;
			controller.BindToView(gamePage, gameView);

			this.NavigationService.Navigate(gamePage); 
		}//*/

		private void StartGameHost_Click(object sender, RoutedEventArgs e)
		{
			var game = SetupNewGame();
			StartNetworkGame(game);
		}

		private void StartNetworkGame(IGameMachine game)
		{
			var gameHostPage = new GameHostPage();

			var controller = new GameHostController(game);
			controller.HostAddress = GetGameAddress();
			controller.BindToView(gameHostPage);

			controller.StartServer();

			this.NavigationService.Navigate(gameHostPage);
		}

		private void ConnectToGameHost_Click(object sender, RoutedEventArgs e)
		{
			var gameClient = new Sage.Net.Game.Client.GameClient();

			var gamePage = new GamePage();
			var gameView = new GameView();
			gamePage.SetGameView(gameView);
			gamePage.SetPlayerView(new PlayerView());

			var controller = new GameClientController(gameClient);
			controller.RemoteHostAddress = GetGameAddress();
			controller.BindToView(gamePage, gameView);

			this.NavigationService.Navigate(gamePage);
		}
	}
}
