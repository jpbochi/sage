﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Geometry;
using Xunit.Extensions;
using Xunit;

namespace Sage.Tests
{
	public class TilePosTests
	{
		[Theory]
		[InlineData("a1", 0, 0)]
		[InlineData("b2", 1, 1)]
		[InlineData("a2", 0, 1)]
		[InlineData("b1", 1, 0)]
		[InlineData("a11", 0, 10)]
		[InlineData("z1", 25, 0)]
		[InlineData("aa1", 26, 0)]
		[InlineData("ab1", 27, 0)]
		[InlineData("az1", 51, 0)]
		[InlineData("ba1", 52, 0)]
		[InlineData("zz1", 701, 0)]
		[InlineData("aaa1", 702, 0)]
		[InlineData("aab1", 703, 0)]
		[InlineData("aba1", 728, 0)]
		void GetTileNameTest(string expectedName, int x, int y)
		{
			var point = new PointInt(x, y);

			Assert.Equal(expectedName, TilePos.ToTileName(point));
		}

		[Theory]
		[InlineData("a1", 0, 0)]
		[InlineData("b2", 1, 1)]
		[InlineData("a2", 0, 1)]
		[InlineData("b1", 1, 0)]
		[InlineData("a11", 0, 10)]
		[InlineData("z1", 25, 0)]
		[InlineData("aa1", 26, 0)]
		[InlineData("ab1", 27, 0)]
		[InlineData("az1", 51, 0)]
		[InlineData("ba1", 52, 0)]
		[InlineData("zz1", 701, 0)]
		[InlineData("aaa1", 702, 0)]
		[InlineData("aab1", 703, 0)]
		[InlineData("aba1", 728, 0)]
		void ParseTileNameTest(string pointName, int x, int y)
		{
			var expectedPoint = new PointInt(x, y);

			Assert.Equal(expectedPoint, TilePos.FromName(pointName));
		}

		[Fact]
		void GetOffBoardTileName()
		{
			Assert.Equal(TilePos.OffBoardName, TilePos.OffBoard.ToTileName());
		}

		[Fact]
		void GetOffBoardTileFromName()
		{
			Assert.Equal(TilePos.OffBoard, TilePos.FromName(TilePos.OffBoardName));
		}

		//void GetInvalidTileName
	}
}
