﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Extensions;
using Moq;
using JpLabs.Extensions;
using JpLabs.Geometry;

namespace Sage.Tests
{
	public class RectBoardTests
	{
		private IGameBoard GetNewBoard(ushort columns, ushort rows)
		{
			return new RectBoard(columns, rows);
		}

		private IGameBoard GetNew3x2Board()
		{
			return GetNewBoard(3, 2);
		}

		private IGamePiece AddBasePieceToPosition(IGameBoard board, PointInt position)
		{
			return board.AddPiece(new BasicPiece(), position);
		}

		private static IEnumerable<PointInt> GetValidPointsIn3x2Board()
		{
			return new [] { "a1", "b1", "c1", "a2", "b2", "c2" }
				.Select( n => TilePos.FromName(n) )
				.Concat( TilePos.OffBoard );
		}

		private static IEnumerable<PointInt> GetInvalidPointsIn3x2Board()
		{
			return new [] { "d1", "d2", "a3", "c3" }
				.Select( n => TilePos.FromName(n) );
		}

		public static IEnumerable<object[]> ValidPointsIn3x2Board
		{
			get { return GetValidPointsIn3x2Board().Select( p => new object[] { p } ); }
		}

		public static IEnumerable<object[]> InvalidPointsIn3x2Board
		{
			get { return GetInvalidPointsIn3x2Board().Select( p => new object[] { p } ); }
		}
		
		[Fact]
		void GetAllTiles()
		{
			const int columns = 3;
			const int rows =  2;

			AssertExt.SetEqual(
				from c in Enumerable.Range(0, columns)
				from r in Enumerable.Range(0, rows)
				select TilePos.Create(c, r),
				GetNew3x2Board().GetAllTiles()
			);
		}

		[Theory]
		[PropertyData("ValidPointsIn3x2Board")]
		void PositionIsInside3x2Board(PointInt position)
		{
			Assert.True(GetNew3x2Board().Contains(position));
		}

		[Theory]
		[PropertyData("InvalidPointsIn3x2Board")]
		void PositionIsNotInside3x2Board(PointInt position)
		{
			Assert.False(GetNew3x2Board().Contains(position));
		}

		[Theory]
		[PropertyData("ValidPointsIn3x2Board")]
		void AddPieceToBoard(PointInt position)
		{
			var board = GetNew3x2Board();
			var mockPiece = new Mock<IGamePiece>();

			var addedPiece = board.AddPiece(mockPiece.Object, position);

			Assert.Equal(addedPiece, mockPiece.Object);
			mockPiece.Verify( p => p.SetPosition(board, position, It.IsAny<int>()), Times.Once());

			Assert.Single(board.GetAllPieces());
		}

		[Fact]
		void AddSeveralPiecesToOffBoard()
		{
			var board = GetNew3x2Board();
			var firstPiece = AddBasePieceToPosition(board, TilePos.OffBoard);
			var secondPiece = AddBasePieceToPosition(board, TilePos.OffBoard);
		}

		[Theory]
		[PropertyData("InvalidPointsIn3x2Board")]
		void AddPieceToInvalidBoardPositionFails(PointInt position)
		{
			var board = GetNew3x2Board();
			var mockPiece = new Mock<IGamePiece>();

			Assert.Throws<ArgumentException>( () => board.AddPiece(mockPiece.Object, position) );
		}

		[Fact]
		void CreateAllPiecesFromEmptyBoard()
		{
			var board = GetNew3x2Board();

			Assert.Empty(board.GetAllPieces());
		}

		[Fact]
		void GetAllPiecesFromFullBoard()
		{
			var board = GetNew3x2Board();
			var positions = GetValidPointsIn3x2Board().Concat(TilePos.OffBoard);
			var addedPieces = positions.Select( pos => AddBasePieceToPosition(board, pos) ).ToArray();

			AssertExt.SetEqual(addedPieces, board.GetAllPieces());
		}

		[Theory]
		[PropertyData("ValidPointsIn3x2Board")]
		void GetPieceFromEmptyBoard(PointInt position)
		{
			var board = GetNew3x2Board();

			Assert.Empty(board.GetPiecesAt(position));
		}

		[Fact]
		void GetPieceFromFullBoard()
		{
			var board = GetNew3x2Board();
			var positions = GetValidPointsIn3x2Board().Concat(TilePos.OffBoard);
			var addedPieces = positions.Select( pos => AddBasePieceToPosition(board, pos) ).ToArray();

			var groupedPieces = addedPieces.GroupBy( p => p.Position );

			foreach (var piecesAtPos in groupedPieces) {
				AssertExt.SetEqual(piecesAtPos, board.GetPiecesAt(piecesAtPos.Key));
			}
		}

		[Theory]
		[InlineData("a1", "b2")][InlineData("c2", TilePos.OffBoardName)][InlineData(TilePos.OffBoardName, "c1")]
		void MovePieceOnBoard(string originName, string destinationName)
		{
			var board = GetNew3x2Board();
			var origin = TilePos.FromName(originName);
			var destination = TilePos.FromName(destinationName);

			var mockPiece = new Mock<IGamePiece>();
			board.AddPiece(mockPiece.Object, origin);

			var movedPiece = board.MovePiece(mockPiece.Object, destination);

			Assert.Equal(movedPiece, mockPiece.Object);
			mockPiece.Verify( p => p.SetPosition(board, destination, It.IsAny<int>()), Times.Once());
		}

		[Theory]
		[InlineData("a1", "d1")][InlineData("a1", "a3")]
		void MovePieceOnBoardToInvalidPositionFails(string originName, string destinationName)
		{
			var board = GetNew3x2Board();
			var origin = TilePos.FromName(originName);
			var destination = TilePos.FromName(destinationName);

			var mockPiece = new Mock<IGamePiece>();
			board.AddPiece(mockPiece.Object, origin);

			Assert.Throws<ArgumentException>( () => board.MovePiece(mockPiece.Object, destination) );
		}
	}
}
