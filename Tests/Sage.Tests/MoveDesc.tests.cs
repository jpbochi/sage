﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using System.Collections;
using Moq;

namespace Sage.Tests
{
	public class MoveDescTests
	{
		void AssertAreEqual(IGameMoveDescription desc1, IGameMoveDescription desc2)
		{
			Assert.Equal(desc1, desc2);
			Assert.Equal(desc2, desc1);
			Assert.Equal(desc1.GetHashCode(), desc2.GetHashCode());
		}

		void AssertAreNotEqual(IGameMoveDescription desc1, IGameMoveDescription desc2)
		{
			Assert.NotEqual(desc1, desc2);
			Assert.NotEqual(desc2, desc1);
			Assert.NotEqual(desc1.GetHashCode(), desc2.GetHashCode());
		}

		TPiece CreateMock<TPiece>(int? id) where TPiece : class, IGamePiece
		{
			var mock = new Mock<TPiece>();
			mock.SetupGet(p => p.Id).Returns(id);
			return mock.Object;
		}

		[Fact]
		void EmptyDescriptorsEquality()
		{
			var desc1 = new MoveDescription();
			var desc2 = new MoveDescription();

			AssertAreEqual(desc1, desc2);
		}

		[Fact]
		void NullMovePieceDescriptorsEquality()
		{
			var piece = CreateMock<IGamePieceClass>(null);
			var desc1 = MoveDesc.CreateDropPieceDesc(-1, piece, TilePos.OffBoard);
			var desc2 = MoveDesc.CreateDropPieceDesc(-1, piece, TilePos.OffBoard);

			AssertAreEqual(desc1, desc2);
		}

		[Fact]
		void MovePieceDescriptorsEquality()
		{
			var piece = CreateMock<IGamePieceClass>(null);
			var desc1 = MoveDesc.CreateDropPieceDesc(0, piece, TilePos.Create(2, 1));
			var desc2 = MoveDesc.CreateDropPieceDesc(0, piece, TilePos.Create(2, 1));

			AssertAreEqual(desc1, desc2);
		}

		[Fact]
		void MovePieceDescriptorsInequality_ByPlayerId()
		{
			var piece = CreateMock<IGamePieceClass>(null);
			var desc1 = MoveDesc.CreateDropPieceDesc(0, piece, TilePos.OffBoard);
			var desc2 = MoveDesc.CreateDropPieceDesc(1, piece, TilePos.OffBoard);

			AssertAreNotEqual(desc1, desc2);
		}

		[Fact]
		void MovePieceDescriptorsInequality_ByTarget()
		{
			var piece = CreateMock<IGamePieceClass>(null);
			var desc1 = MoveDesc.CreateDropPieceDesc(-1, piece, TilePos.Create(1, 2));
			var desc2 = MoveDesc.CreateDropPieceDesc(-1, piece, TilePos.Create(2, 1));

			AssertAreNotEqual(desc1, desc2);
		}

		[Fact]
		void MovePieceDescriptorsInequality_ByPiece()
		{
			var piece1 = CreateMock<IGamePieceClass>(1);
			var piece2 = CreateMock<IGamePieceClass>(2);
			var desc1 = MoveDesc.CreateDropPieceDesc(-1, piece1, TilePos.OffBoard);
			var desc2 = MoveDesc.CreateDropPieceDesc(-1, piece2, TilePos.OffBoard);

			AssertAreNotEqual(desc1, desc2);
		}

		[Fact]
		void PieceDescriptorsEquality_ByType()
		{
			var desc1 = new MoveDescription();
			desc1.SetValue(MoveDesc.MoveTypeProperty, MoveType.PassTurn);

			var desc2 = new MoveDescription();
			desc2.SetValue(MoveDesc.MoveTypeProperty, MoveType.PassTurn.Clone());

			AssertAreEqual(desc1, desc2);
		}
	}
}
