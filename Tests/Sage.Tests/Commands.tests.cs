﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Moq;
using JpLabs.Geometry;
using Sage.Augmentation;

namespace Sage.Tests
{
	public class CommandsTests
	{
		Random GetDeterministicRandom()
		{
			return new Random(42);
		}

		IList<IGamePlayer> CreatePlayers(int playerCount)
		{
			var rnd = GetDeterministicRandom();

			return Enumerable.Range(0, playerCount).Select( i => new BasicPlayer(rnd.Next()) ).ToArray();
		}

		void AssertAreEqual(IGameCommand cmd1, IGameCommand cmd2)
		{
			Assert.Equal(cmd1, cmd2);
			Assert.Equal(cmd2, cmd1);
			Assert.Equal(cmd1.GetHashCode(), cmd2.GetHashCode());
		}

		[Fact]
		void AddPieceCommand_Equality()
		{
			var piece = new Mock<IGamePieceClass>().Object;
			var pos = TilePos.OffBoard;

			var cmd1 = new AddPieceCommand(piece, pos);
			var cmd2 = new AddPieceCommand(piece, pos);

			AssertAreEqual(cmd1, cmd2);
		}

		[Fact]
		void MovePieceCommand_Equality()
		{
			var piece = new Mock<IGamePiece>().Object;
			var pos = TilePos.OffBoard;

			var cmd1 = new MovePieceCommand(piece, pos);
			var cmd2 = new MovePieceCommand(piece, pos);

			AssertAreEqual(cmd1, cmd2);
		}

		[Fact]
		void CycleTurnCommand_Equality()
		{
			var cmd1 = new CycleTurnCommand();
			var cmd2 = new CycleTurnCommand();

			AssertAreEqual(cmd1, cmd2);
		}

		[Fact]
		void SetGamePropertyCommand_Equality()
		{
			var prop = AugProperty.Create("null", typeof(object), typeof(CommandsTests));
			var value = new object();

			var cmd1 = new SetGamePropertyCommand(prop, value);
			var cmd2 = new SetGamePropertyCommand(prop, value);

			AssertAreEqual(cmd1, cmd2);
		}

		[Fact]
		void CapturePieceCommand_Equality()
		{
			var piece = new Mock<IGamePiece>().Object;

			var cmd1 = new CapturePieceCommand(piece);
			var cmd2 = new CapturePieceCommand(piece);

			AssertAreEqual(cmd1, cmd2);
		}

		[Fact]
		void AddPieceCommand_CallsBoardAddPiece()
		{
			var mockBoard = new Mock<IGameBoard>();
			var state = new BasicGameState(null, mockBoard.Object);

			var piece = new Mock<IGamePieceClass>().Object;
			var mockPieceClass = new Mock<IGamePieceClass>();
			mockPieceClass.Setup( p => p.CreatePiece() ).Returns(piece);

			var position = new PointInt(0, 0);

			var op = new AddPieceCommand(mockPieceClass.Object, position);
			op.Execute(state);

			mockPieceClass.Verify( p => p.CreatePiece() );
			mockBoard.Verify( b => b.AddPiece(piece, position) );
		}

		[Fact]
		void MovePieceCommand_CallsBoardMovePiece()
		{
			var mockBoard = new Mock<IGameBoard>();
			var state = new BasicGameState(null, mockBoard.Object);

			var position = new PointInt(0, 0);
			var piece = new Mock<IGamePiece>().Object;

			var op = new MovePieceCommand(piece, position);
			op.Execute(state);

			mockBoard.Verify( b => b.MovePiece(piece, position) );
		}

		[Fact]
		void CycleTurnCommand_CyclesFromFisrtToSecond()
		{
			var players = CreatePlayers(2);
			var state = new BasicGameState(players, null);
			state.CurrentPlayerId = players.First().Id;

			var op = new CycleTurnCommand();
			op.Execute(state);

			Assert.Equal(players.Last().Id, state.CurrentPlayerId);
		}

		[Fact]
		void CycleTurnCommand_CyclesFromThirdToFourth()
		{
			var players = CreatePlayers(4);
			var state = new BasicGameState(players, null);
			state.CurrentPlayerId = players[2].Id;

			var op = new CycleTurnCommand();
			op.Execute(state);

			Assert.Equal(players[3].Id, state.CurrentPlayerId);
		}

		[Fact]
		void CycleTurnCommand_CyclesFromLastToFisrt()
		{
			var players = CreatePlayers(2);
			var state = new BasicGameState(players, null);
			state.CurrentPlayerId = players.Last().Id;

			var op = new CycleTurnCommand();
			op.Execute(state);

			Assert.Equal(players.First().Id, state.CurrentPlayerId);
		}

		[Fact]
		void SetGamePropertyCommand_CallsSetValue()
		{
			var property = AugProperty.Create("Test", typeof(object), typeof(CommandsTests));
			var value = new object();
			var cmd = new SetGamePropertyCommand(property, value);

			var mockState = new Mock<IGameState>();
			cmd.Execute(mockState.Object);

			mockState.Verify( t => t.SetValue(property, value) );
		}

		[Fact]
		void CapturePieceCommand_MovePieceOffBoard()
		{
			var mockBoard = new Mock<IGameBoard>();
			var state = new BasicGameState(null, mockBoard.Object);

			var piece = new Mock<IGamePiece>().Object;

			var op = new CapturePieceCommand(piece);
			op.Execute(state);

			mockBoard.Verify( b => b.MovePiece(piece, TilePos.OffBoard) );
		}
	}
}
