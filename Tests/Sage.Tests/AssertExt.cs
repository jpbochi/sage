﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using System.Collections;
using Xunit.Sdk;
using Moq;

namespace Sage.Tests
{
	public static class MockMatch
	{
		public static T Create<T>(Func<T,bool> matchFunc) where T : class
		{
			var mockObj = new Mock<T>();
			mockObj
			.As<IStructuralEquatable>()
			.Setup(m => m.Equals(It.IsAny<T>(), It.IsAny<IEqualityComparer>()))
			.Returns<T,IEqualityComparer>(
				(o,c) => matchFunc(o)
			);

			return mockObj.Object;
		}
	}

	public static class AssertExt
	{
		class EnumerableComparer<T> : IEqualityComparer<IEnumerable<T>>
		{
			IEqualityComparer<T> Comparer = EqualityComparer<T>.Default;

			public bool Equals(IEnumerable<T> x, IEnumerable<T> y)
			{
				return x.Zip(y, (xi, yi) => Comparer.Equals(xi, yi) ).All( b => b );
			}

			public int GetHashCode(IEnumerable<T> obj)
			{
				throw new NotImplementedException();
			}
		}

		public static T AssertSingle<T>(this IEnumerable<T> source)
		{
			return Assert.Single(source);
		}

		public static void AssertAll<T>(this IEnumerable<T> source, Action<T> action)
		{
			foreach (var i in source) action(i);
		}

		public static IEnumerable<T> AssertCast<T>(this IEnumerable source)
		{
			foreach (var item in source) {
				Assert.IsAssignableFrom<T>(item);
				yield return (T)item;
			}
		}

		public static void AssertSequenceEqual<T>(this IEnumerable<T> expected, IEnumerable<T> actual)
		{
			Assert.Equal(expected, actual, new EnumerableComparer<T>());
			
			//expected.Zip(actual, (e, a) => { Assert.Equal<T>(e, a); return 0; } ).ToArray();
		}

		public static void StructuralEqual(object expected, object actual)
		{
			if (!StructuralComparisons.StructuralEqualityComparer.Equals(expected, actual))
				throw new EqualException(expected, actual);
		}

		public static bool IsSetEqual<T>(this IEnumerable<T> expected, IEnumerable<T> actual)
		{
			var actualList = actual.ToList();
			foreach (var expectedItem in expected) {
				var comparer = StructuralComparisons.StructuralEqualityComparer;

				int index = actualList.FindIndex( actualItem => comparer.Equals(expectedItem, actualItem) );
				if (index != -1 )
					actualList.RemoveAt(index);
				else
					return false;
			}
			return actualList.Count == 0;
		}

		public static void SetEqual<T>(this IEnumerable<T> expected, IEnumerable<T> actual)
		{
			var actualList = actual.ToList();
			foreach (var expectedItem in expected) {
				var comparer = StructuralComparisons.StructuralEqualityComparer;

				int index = actualList.FindIndex( actualItem => comparer.Equals(expectedItem, actualItem) );
				if (index != -1 )
					actualList.RemoveAt(index);
				else
					throw new AssertException(string.Format("Expected item was not found in list: {0}", expectedItem));
			}
			Assert.Empty(actualList);
		}
	}
}
