﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Geometry;
using Ninject;

namespace Sage.Games.Tests
{
	internal static class GameExt
	{
		public static void PlayMove(this IGameMachine game, PointInt origin, PointInt destination)
		{
			//var move = game.GetMoveOptions().Where( m => m.IsType(MoveType.MovePiece) && m.GetPiece().Position == origin && m.GetDestination() == destination ).First();
			var move = game.GetMoveOptions().Where( m => m.IsType(MoveType.MovePiece) && m.GetOrigin() == origin && m.GetDestination() == destination ).First();
			game.Play(move);
		}

		public static void PlayDropAt(this IGameMachine game, PointInt position)
		{
			var move = game.GetMoveOptions().Where( m => m.IsType(MoveType.DropPiece) && m.GetDestination() == position ).First();
			game.Play(move);
		}

		public static void PlayPassTurn(this IGameMachine game)
		{
			var move = game.GetMoveOptions().Where( m => m.IsType(MoveType.PassTurn) ).First();
			game.Play(move);
		}

		public static IGameState GetNewState(this IKernel kernel, IGameBoard board)
		{
			kernel.Rebind<IGameBoard>().ToConstant(board);
		    return kernel.Get<IGameState>();
		}

		public static IGameMachine GetNewGameMachine(this IKernel kernel)
		{
		    return kernel.Get<IGameMachine>();
		}

		public static IGameMachine GetNewGameMachine(this IKernel kernel, IGameBoard board)
		{
			kernel.Rebind<IGameBoard>().ToConstant(board);
		    return kernel.Get<IGameMachine>();
		}
	}
}
