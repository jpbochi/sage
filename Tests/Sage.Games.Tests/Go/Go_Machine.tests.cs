﻿using System.Linq;
using JpLabs.Extensions;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Xunit;

namespace Sage.Games.Tests.Go
{
	public class Go_Machine_Tests : Go_Referee_Tests
	{
		[Fact]
		void TestInitialMachine()
		{
			var game = Kernel.GetNewGameMachine();

			VerifyInitialState(game.State);
			VerifyInitialMoveOptions_Descriptions(game.GetMoveOptions(), game.State.Board);
		}

		[Fact]
		void TestMachine_PlayValidFisrtMove()
		{
			string initialBoard = PrepareBoard(@"
				...
				...
				..."
			);
			var game = Kernel.GetNewGameMachine(ParseBoard(initialBoard));

			game.PlayDropAt(TilePos.FromName("b2"));

			string expectedBoard = PrepareBoard(@"
				...
				.x.
				..."
			);

			Assert.Equal(expectedBoard, DumpBoard(game.State.Board));
			Assert.Equal(ClassicPlayer.White.GetId(), game.State.CurrentPlayerId);
			Assert.Empty(game.State.Winners.EmptyIfNull());
		}


		[Fact]
		void TestDoublePassEndsGame()
		{
			var game = Kernel.GetNewGameMachine();

			game.PlayPassTurn();
			game.PlayPassTurn();

			Assert.Null(game.State.CurrentPlayerId);
		}
	}
}
