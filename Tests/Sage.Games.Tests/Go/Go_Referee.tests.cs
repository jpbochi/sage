﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Sage.Games.Go;
using Sage.Tests;
using Xunit;
using Xunit.Extensions;

namespace Sage.Games.Tests.Go
{
	public class Go_Referee_Tests : Go_State_Tests
	{
		protected IGameReferee GetNewReferee()
		{
		    return Kernel.Get<IGameReferee>();
		}

		IGameMove GetDropMove(IGameState state, IGameReferee referee, PointInt destination)
		{
			return referee
			.GetMoveOptions(state)
			.Where(m => m.Description.IsType(MoveType.DropPiece) && m.Description.GetDestination() == destination)
			.First();

			//return referee.GetMoveByDescription(
			//    state,
			//    MoveDesc.CreateDropPieceDesc(state.CurrentPlayer.Value, MockMatch.Create<IGamePieceClass>(p => true), destination)
			//);
		}

		void PlayDrop(IGameState state, IGameReferee referee, PointInt destination)
		{
			var move = GetDropMove(state, referee, destination);

			foreach (var cmd in move.Resolve(state)) cmd.Execute(state);
		}

		static IEnumerable<Tuple<string,string,string[]>> GetBoardsForCaptureTests()
		{
			yield return Tuple.Create(PrepareBoard(@"
				. . x o .
				. x o . o
				. . x o ."), "d2", new [] {"c2"}
			);
			yield return Tuple.Create(PrepareBoard(@"
				o . o x .
				x o o x .
				. x x . .
				. . . . ."), "b1", new [] {"a1", "c1", "b2", "c2"}
			);
			yield return Tuple.Create(PrepareBoard(@"
				. . . x . .
				. . x o x .
				. x o . . .
				. . x . . ."), "d3", new [] {"d2", "c3"}
			);
		}

		static IEnumerable<Tuple<string,string>> GetBoardsForSuicideMoveTests()
		{
			yield return Tuple.Create(PrepareBoard(@"
				. o . .
				o . o .
				. o . ."), "b2"
			);
			yield return Tuple.Create(PrepareBoard(@"
				x . x o .
				o x x o .
				. o o . .
				. . . . ."), "b1"
			);
		}

		static IEnumerable<Tuple<string,string,string>> GetBoardsForKoMoveTests()
		{
			yield return Tuple.Create(PrepareBoard(@"
				.xo.
				xo.o
				.xo."), "c2", "b2"
			);
			yield return Tuple.Create(PrepareBoard(@"
				....
				..xo
				.xo."), "d3", "c3"
			);
		}

		public static IEnumerable<object[]> BoardsForCaptureTests
		{
			get { return GetBoardsForCaptureTests().Select( t => new object[] { t.Item1, t.Item2, t.Item3 } ); }
		}

		public static IEnumerable<object[]> BoardsForSuicideMoveTests
		{
			get { return GetBoardsForSuicideMoveTests().Select( t => new object[] { t.Item1, t.Item2 } ); }
		}

		public static IEnumerable<object[]> BoardsForKoMoveTests
		{
			get { return GetBoardsForKoMoveTests().Select( t => new object[] { t.Item1, t.Item2, t.Item3 } ); }
		}

		[Theory]
		[InlineData(9)]
		[InlineData(13)]
		[InlineData(19)]
		void TestInitialMoveOptions_Benchmark(int boardSize)
		{
			var state = Kernel.GetNewState(GetBoard((ushort)boardSize, (ushort)boardSize));
			var referee = GetNewReferee();

			const int totalMilisecondsToWait = 444;
			double averageMs = 1;

			{
				//TODO: find out why the first call is considerably slower than the others
				var debugWatch = Stopwatch.StartNew();
				referee.GetMoveOptions(state).Last();
				debugWatch.Stop();
				Debug.Print("1st pass calculated in {0}ms", debugWatch.Elapsed.TotalMilliseconds);

				GC.Collect();
				GC.WaitForPendingFinalizers();
			}

			var testWatch = Stopwatch.StartNew();

			int testCycles = 0;
			while (testWatch.ElapsedMilliseconds < totalMilisecondsToWait) {
				testCycles++;
				var debugWatch = Stopwatch.StartNew();

				var moves = referee.GetMoveOptions(state).ToArray();

				debugWatch.Stop();
				var time = debugWatch.Elapsed.TotalMilliseconds;
				averageMs *= time;
			}

			averageMs = Math.Pow(averageMs,  1.0 / testCycles);
			Debug.Print("Go moves calculated in {0:0.00}ms for a {1}x{1} board (geometric mean of {2} cycles)", averageMs, boardSize, testCycles);
		}

		[Fact]
		void TestInitialMoveOptions()
		{
			var state = GetNewState();
			var referee = GetNewReferee();

			var moves = referee.GetMoveOptions(state).ToArray();
			Assert.NotNull(moves);

			VerifyInitialMoveOptions_Descriptions(moves.Select( m => m.Description ), state.Board);
			VerifyInitialMoveOptions_Resolution(moves, state);
		}

		protected void VerifyInitialMoveOptions_Descriptions(IEnumerable<IGameMoveDescription> moveDescs, IGameBoard board)
		{
			var expectedPlayerId = ClassicPlayer.Black.GetId();

			var expectedPieceTemplate
				= board.GetPiecesAt(TilePos.OffBoard)
				.Where( p => p.GetFlag() == expectedPlayerId )
				.AssertCast<IGamePieceClass>()
				.Single();

			var expectedDescriptions
				= board.GetAllTiles()
				.Select(
					t => MoveDesc.CreateDropPieceDesc(expectedPlayerId, expectedPieceTemplate, t)
				).Concat(
					MoveDesc.CreatePassTurnDesc(expectedPlayerId)
				);

			AssertExt.SetEqual(expectedDescriptions, moveDescs);
		}		

		protected void VerifyInitialMoveOptions_Resolution(IEnumerable<IGameMove> moves, IGameState state)
		{
			// Assert that all moves resolve to operation that match their descriptions
			foreach (var move in moves) {
				if (move.Description.IsType(MoveType.PassTurn)) {
					VerifyPassTurnMove(move, state);
				} else {
					VerifyDropStoneMove(move, state);
				}
			}
		}

		private void VerifyPassTurnMove(IGameMove move, IGameState state)
		{
			var expectedCommands = new IGameCommand[] {
				new SetGamePropertyCommand(GoProperties.PreviousMoveTypeProperty, MoveType.PassTurn),
				new SetGamePropertyCommand(GoProperties.PreviousBoardProperty, null),
				new CycleTurnCommand(),
				new UpdateScoreCommand(),
			};
			var cmds = move.Resolve(state);
			AssertExt.SetEqual(expectedCommands, cmds.ToArray());
		}

		private void VerifyDropStoneMove(IGameMove move, IGameState state, params PointInt[] expectedCaptures)
		{
			var pieceClass = move.Description.GetPieceClass(state.Board);
			var position = move.Description.GetDestination();
			var expectedPreviousBoard = WorkBoard.Create(state.Board);

			var expectedCommands = new List<IGameCommand>() {
				new AddPieceCommand(pieceClass, position),
				new SetGamePropertyCommand(GoProperties.PreviousMoveTypeProperty, MoveType.DropPiece),
				new SetGamePropertyCommand(GoProperties.PreviousBoardProperty, expectedPreviousBoard),
				new CycleTurnCommand(),
				new UpdateScoreCommand(),
			};

			expectedCommands.AddRange(
				expectedCaptures.Select( c => new CapturePieceCommand(state.Board.GetPiecesAt(c).FirstOrDefault()) )
			);

			var cmds = move.Resolve(state);
			AssertExt.SetEqual(expectedCommands, cmds.ToArray());
		}

		[Theory]
		[PropertyData("BoardsForCaptureTests")]
		void TestStoneCapture(string boardSetup, string dropTile, params string[] expectedCaptures)
		{
			var state = GetStateWithBoardSetup(boardSetup);
			var referee = GetNewReferee();

			var dropMove = GetDropMove(state, referee, TilePos.FromName(dropTile));
			
			VerifyDropStoneMove(dropMove, state, expectedCaptures.Select(t => TilePos.FromName(t)).ToArray());
		}

		[Theory]
		[PropertyData("BoardsForSuicideMoveTests")]
		void TestSuicideMoveIsNotAvailable(string boardSetup, string suicidePos)
		{
			var state = GetStateWithBoardSetup(boardSetup);
			var referee = GetNewReferee();

			var suicideMoveQuery = referee
				.GetMoveOptions(state)
				.Where(m => m.Description.IsType(MoveType.DropPiece) && m.Description.GetDestination() == TilePos.FromName(suicidePos));
			
			Assert.Empty(suicideMoveQuery);
		}

		[Theory]
		[PropertyData("BoardsForKoMoveTests")]
		void TestKoMoveIsNotAvailable(string boardSetup, string fisrtMove, string koMove)
		{
			var state = GetStateWithBoardSetup(boardSetup);
			var referee = GetNewReferee();

			PlayDrop(state, referee, TilePos.FromName(fisrtMove));

			var koMoveQuery = referee
				.GetMoveOptions(state)
				.Where(m => m.Description.IsType(MoveType.DropPiece) && m.Description.GetDestination() == TilePos.FromName(koMove));
			
			Assert.Empty(koMoveQuery);
		}
	}
}
