﻿using System;
using System.Linq;
using System.Text;
using JpLabs.Symbols;
using Ninject;
using Sage.Games.Go;

namespace Sage.Games.Tests.Go
{
	static class Ext
	{
		public static Ninject.Syntax.IBindingToSyntax<T> RebindNamed<T>(this IKernel kernel, string name)
		{
			var bindingToRemove = kernel.GetBindings(typeof(T)).Where( b => b.Metadata.Name == name ).FirstOrDefault();
			if (bindingToRemove != null) kernel.RemoveBinding(bindingToRemove);

			var builder = kernel.Bind<T>();
			String.Intern(name);
			builder.Binding.Metadata.Name = name;

			return builder;
		}
	}

	public class Go_Helper
	{
		private Lazy<IKernel> lazyKernel = new Lazy<IKernel>( () => new StandardKernel(new Sage.Games.Go.GoGameModule()) );

		protected IKernel Kernel 
		{
			get { return lazyKernel.Value; }
		}

		protected IGameState GetNewState()
		{
			return Kernel.Get<IGameState>();
		}
		
		protected IGameBoard GetBoard(ushort cols, ushort rows)
		{
			var kernel = Kernel;

			//var names = new [] { GoGameModule.ROWS_PARAM, GoGameModule.COLS_PARAM };
			//var bindingsToRemove = kernel.GetBindings(typeof(ushort)).Where( b => names.Contains(b.Metadata.Name) );
			//foreach (var b in bindingsToRemove) kernel.RemoveBinding(b);

			//kernel.Bind<ushort>().ToConstant(rows).Named(GoGameModule.ROWS_PARAM);
			//kernel.Bind<ushort>().ToConstant(cols).Named(GoGameModule.COLS_PARAM);

			kernel.RebindNamed<ushort>(GoGameModule.ROWS_PARAM).ToConstant(rows);
			kernel.RebindNamed<ushort>(GoGameModule.COLS_PARAM).ToConstant(cols);
			return kernel.Get<IGameBoard>();
		}

		private IGamePiece GetStonePiece(int flag)
		{
			return Kernel.Get<IGamePieceClass>(GoPieces.Stone.Name).CreatePiece().SetFlag(flag);
		}

		protected IGameBoard ParseBoard(string strBoard)
		{
			var lines = strBoard.Split(new []{'\n'}, StringSplitOptions.RemoveEmptyEntries);
			int rows = lines.Length;
			int cols = lines[0].Length;

			var board = GetBoard((ushort)cols, (ushort)rows);

			for (int r=0; r<rows; r++) {
				for (int c=0; c<cols; c++) {
					if (lines[r][c] == 'x') board.AddPiece(GetStonePiece(ClassicPlayer.Black.GetId()), TilePos.Create(c, r));
					else
					if (lines[r][c] == 'o') board.AddPiece(GetStonePiece(ClassicPlayer.White.GetId()), TilePos.Create(c, r));
				}
			}

			return board;
		}

		protected static string PrepareBoard(string boardSetup)
		{
			return boardSetup
				.Replace(" ", "")
				.Replace("\t", "")
				.Replace("\r", "")
				.TrimEnd('\n');
		}

		protected string DumpBoard(IGameBoard board)
		{
			var lines = board.GetAllTiles().GroupBy( t => t.Y ).ToArray();

			var builder = new StringBuilder();
			foreach (var line in lines) {
				builder.Append('\n');

				foreach (var tile in line) {
					var piece = board.GetPiecesAt(tile).FirstOrDefault();

					var ch = (piece == null) ? '.'
						   : (piece.GetFlag() == ClassicPlayer.Black.GetId()) ? 'x'
						   : (piece.GetFlag() == ClassicPlayer.White.GetId()) ? 'o'
						   : '?';
					
					builder.Append(ch);
				}
			}
			
			return builder.ToString();
		}
	}
}
