﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Symbols;
using Ninject;
using Sage.Games.Go;
using Sage.Tests;
using Xunit;
using Xunit.Extensions;

namespace Sage.Games.Tests.Go
{
	public class Go_State_Tests : Go_Helper
	{
		private IGameCommand GetUpdateScoreCommand()
		{
			return Kernel.Get<IGameCommand>(KnownGameOperations.UpdateScore);
		}
		
		protected IGameState GetStateWithBoardSetup(string boardSetup)
		{
			var board = ParseBoard(boardSetup);
			Kernel.Rebind<IGameBoard>().ToConstant(board);

			return Kernel.Get<IGameState>();
		}

		static IEnumerable<Tuple<string,int,int>> GetBoardsWithExpectedScore()
		{
			yield return Tuple.Create(
				PrepareBoard(@"
					......
					..xo..
					.xo.o.
					..xo..
					......"
				), 0, 1
			);
			yield return Tuple.Create(
				PrepareBoard(@"
					.x......
					x.x.....
					.x.x.ooo
					..x..o.o
					......o."
				), 3, 2
			);
			yield return Tuple.Create(
				PrepareBoard(@"
					.x..o...
					.x.o..o.
					.x.o.o.o
					..x.o.o."
				), 5, 10
			);
		}

		public static IEnumerable<object[]> BoardsWithExpectedScore
		{
			get { return GetBoardsWithExpectedScore().Select( t => new object[] { t.Item1, t.Item2, t.Item3 } ); }
		}

		public static IEnumerable<object[]> TestBoards
		{
			get { return GetBoardsWithExpectedScore().Select( t => new object[] { t.Item1 } ); }
		}

		[Fact]
		void CreateInitialState()
		{
			var state = GetNewState();
			
			VerifyInitialState(state);
		}

		protected void VerifyInitialState(IGameState state)
		{
			AssertExt.AssertSequenceEqual(
				new []{ ClassicPlayer.Black.GetId(), ClassicPlayer.White.GetId()},
				state.Players.Select( p => p.Id ).ToArray()
			);

			Assert.Equal(ClassicPlayer.Black.GetId(), state.CurrentPlayerId);

			Assert.NotNull(state.Board);

			AssertExt.SetEqual(
				from c in Enumerable.Range(0, Sage.Games.Go.GoGameModule.DEFAULT_BOARD_SIZE)
				from r in Enumerable.Range(0, Sage.Games.Go.GoGameModule.DEFAULT_BOARD_SIZE)
				select TilePos.Create(c, r),
				state.Board.GetAllTiles()
			);

			Assert.Equal(2, state.Board.GetAllPieces().Count());
			Assert.Equal(2, state.Board.GetPiecesAt(TilePos.OffBoard).Count());

			Assert.Single(state.Board.GetPiecesAt(TilePos.OffBoard).Where( p => p.GetFlag() == ClassicPlayer.Black.GetId() ));
			Assert.Single(state.Board.GetPiecesAt(TilePos.OffBoard).Where( p => p.GetFlag() == ClassicPlayer.White.GetId() ));
		}
		
		[Theory]
		[PropertyData("TestBoards")]
		public void TestBoardParseAndDumper(string boardDesc)
		{
			var board = ParseBoard(boardDesc);

			Assert.Equal(boardDesc, DumpBoard(board));
		}

		[Theory]
		[PropertyData("BoardsWithExpectedScore")]
		void TestBoardsWithExpectedTerritory(string boardDesc, int expectedBlackTerritory, int expectedWhiteTerritory)
		{
			var state = GetStateWithBoardSetup(boardDesc);

			var cmd = GetUpdateScoreCommand();
			cmd.Execute(state);

			Assert.Equal(expectedBlackTerritory, state.GetPlayerById(ClassicPlayer.Black.GetId()).GetValue<int>(GoProperties.TerritoryProperty));
			Assert.Equal(expectedWhiteTerritory, state.GetPlayerById(ClassicPlayer.White.GetId()).GetValue<int>(GoProperties.TerritoryProperty));

			Assert.Empty(state.Winners);
			Assert.NotNull(state.CurrentPlayerId);
		}
	}
}
