﻿using System.Linq;
using Xunit;

namespace Sage.Games.Tests.Go
{
	public class Go_Net_Tests : Go_Machine_Tests
	{
		[Fact]
		void InitialStateIsSerializable()
		{
			var state = GetNewState();

			var serialized = GameSerializer.Serialize<IGameState>(state);
		}

		[Fact]
		void InitialStateIsDeserializable()
		{
			var state = GetNewState();

			var serialized = GameSerializer.Serialize<IGameState>(state);

			var deserialized = GameSerializer.Deserialize<IGameState>(serialized);

			VerifyInitialState(deserialized);
		}

		[Fact]
		void InitialStateReserialization_GivesSameResult()
		{
			var state = GetNewState();

			var serialized = GameSerializer.Serialize<IGameState>(state);

			var deserialized = GameSerializer.Deserialize<IGameState>(serialized);

			var reserialized = GameSerializer.Serialize<IGameState>(deserialized);

			Assert.Equal(serialized, reserialized);
		}


		[Fact]
		void InitialMoveDescriptions_AreSerializable()
		{
			var machine = Kernel.GetNewGameMachine();
			var moves = machine.GetMoveOptions().ToArray();

			var serialized = GameSerializer.Serialize(moves);
			var deserialized = GameSerializer.Deserialize<IGameMoveDescription[]>(serialized);
			var reserialized = GameSerializer.Serialize(deserialized);

			Assert.Equal(serialized, reserialized);
			Assert.InRange(serialized.Length, 0, ushort.MaxValue);
		}
	}
}
