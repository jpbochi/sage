﻿using System.Collections.Generic;
using System.Linq;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Sage.Games.Amazons;
using Sage.Tests;
using Xunit;

namespace Sage.Games.Tests.Amazons
{
	public class Amazons_Referee_Tests : Amazons_State_Tests
	{
		protected IGameReferee GetNewReferee()
		{
		    return Kernel.Get<IGameReferee>();
		}

		void PlayMoveTo(IGameState state, IGameReferee referee, PointInt origin, PointInt destination)
		{
			var move = referee.GetMoveOptions(state).Where(
				m => m.Description.GetOrigin() == origin && m.Description.GetDestination() == destination
			).First();

			foreach (var cmd in move.Resolve(state)) cmd.Execute(state);
		}

		[Fact]
		void TestInitialMoveOptions()
		{
			var state = GetNewState();
			var referee = GetNewReferee();

			var moves = referee.GetMoveOptions(state).ToArray();
			Assert.NotNull(moves);

			VerifyInitialMoveOptions_Descriptions(moves.Select( m => m.Description ), state.Board);
			VerifyInitialMoveOptions_Resolution(moves, state.Board);
		}

		protected void VerifyInitialMoveOptions_Descriptions(IEnumerable<IGameMoveDescription> moveDescs, IGameBoard board)
		{
			var expectedValidMoves = new [] {
				"a3-a1", "a3-a2", "a3-a4", "a3-b2", "a3-b3", "a3-b4", "a3-c3", "a3-c5", "a3-d3", "a3-d6", "a3-e3", "a3-f3", // from a3
				"c1-a1", "c1-b1", "c1-b2", "c1-c2", "c1-c3", "c1-c4", "c1-c5", "c1-c6", "c1-d1", "c1-d2", "c1-e3", "c1-f4", // from c1
				"e1-b4", "e1-c3", "e1-d1", "e1-d2", "e1-e2", "e1-e3", "e1-e4", "e1-e5", "e1-e6", "e1-f1", "e1-f2", "e1-g1", // from e1
				"g3-b3", "g3-c3", "g3-d3", "g3-d6", "g3-e3", "g3-e5", "g3-f2", "g3-f3", "g3-f4", "g3-g1", "g3-g2", "g3-g4", // from g3
			};

			var actualValidMoves = moveDescs
				.Select( m => string.Format("{0}-{1}", m.GetOrigin().ToTileName(), m.GetDestination().ToTileName()) )
				.OrderBy( s => s ).ToArray();

			AssertExt.AssertSequenceEqual(expectedValidMoves, actualValidMoves);

			var expectedPlayerId = ClassicPlayer.White.GetId();
			moveDescs.AssertAll( m => Assert.Equal(expectedPlayerId, m.GetPlayerId()) );
		}

		protected void VerifyInitialMoveOptions_Resolution(IEnumerable<IGameMove> moves, IGameBoard board)
		{
			// Assert that all moves resolve to operation that match their descriptions
			foreach (var move in moves) {
				var piece = move.Description.GetPiece(board);
				var destination = move.Description.GetDestination();

				var expectedCommands = new IGameCommand[] {
					new MovePieceCommand(piece, destination),
					new SetGamePropertyCommand(AmazonProperties.LastPieceToMoveProperty, piece),
					new IsGameOverEvaluation(),
				};
				var cmds = move.Resolve(null);
				AssertExt.SetEqual(expectedCommands, cmds.ToArray());
			}
		}

		[Fact]
		void TestMoveOptionsAfterFirstMove()
		{
			var state = GetNewState();
			var referee = GetNewReferee();

			var origin = TilePos.FromName("c1");
			var destination = TilePos.FromName("c5");
			PlayMoveTo(state, referee, origin, destination);

			var expectedArrows = new [] {
			    "a7", "b4", "b5", "b6", "c1", "c2", "c3", "c4", "c6", "d4", "d5", "d6", "e3", "e5", "f2", "f5", "g1"
			};

			var moves = referee.GetMoveOptions(state).ToArray();
			VerifyMoveOptionsAfterFirstMove_Descriptions(moves.Select( m => m.Description ), state.Board, destination, expectedArrows);
			VerifyMoveOptionsAfterFirstMove_Resolution(moves, state.Board);
		}

		private void VerifyMoveOptionsAfterFirstMove_Descriptions(IEnumerable<IGameMoveDescription> moveDescs, IGameBoard board, PointInt moveDestination, IEnumerable<string> expectedArrows)
		{
			var expectedPieceClass = board.GetPiecesAt(moveDestination).Single().GetArrowClass();
			var expectedPlayerId = ClassicPlayer.White.GetId();

			var expectedDescriptions = expectedArrows.Select(
				tile => MoveDesc.CreateDropPieceDesc(expectedPlayerId, expectedPieceClass, TilePos.FromName(tile))
			);

			AssertExt.SetEqual(expectedDescriptions, moveDescs);
		}

		protected void VerifyMoveOptionsAfterFirstMove_Resolution(IEnumerable<IGameMove> moves, IGameBoard board)
		{
			foreach (var move in moves) {
				var pieceClass = move.Description.GetPieceClass(board);
				var position = move.Description.GetDestination();

				var expectedCommands = new IGameCommand[] {
					new AddPieceCommand(pieceClass, position),
					new SetGamePropertyCommand(AmazonProperties.LastPieceToMoveProperty, null),
					new CycleTurnCommand(),
					new IsGameOverEvaluation(),
				};
				var cmds = move.Resolve(null);
				AssertExt.SetEqual(expectedCommands, cmds.ToArray());
			}
		}
	}
}
