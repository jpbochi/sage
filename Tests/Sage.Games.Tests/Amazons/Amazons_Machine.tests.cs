﻿using System.Linq;
using JpLabs.Extensions;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Sage.Tests;
using Xunit;

namespace Sage.Games.Tests.Amazons
{
	public class Amazons_Machine_Tests : Amazons_Referee_Tests
	{
		[Fact]
		void TestInitialMachine()
		{
			var game = Kernel.GetNewGameMachine();

			VerifyInitialState(game.State);
			VerifyInitialMoveOptions_Descriptions(game.GetMoveOptions(), game.State.Board);
		}

		[Fact]
		void TestMachine_PlayValidFisrtMove()
		{
			var game = Kernel.GetNewGameMachine();

			game.PlayMove(TilePos.FromName("c1"), TilePos.FromName("c5"));

			string expectedBoard = PrepareBoard(@"
				....A..
				.......
				A.....A
				.......
				a.A...a
				.......
				..a.a.."
			);

			Assert.Equal(expectedBoard, DumpBoard(game.State.Board));
			Assert.Equal(ClassicPlayer.White.GetId(), game.State.CurrentPlayerId);
			Assert.Empty(game.State.Winners.EmptyIfNull());
		}

		[Fact]
		void TestMachine_PlayKillingWhiteMove()
		{
			string initialBoard = PrepareBoard(@"
				A.A
				##.
				a.a"
			);
			string finalBoard = PrepareBoard(@"
				A..
				##A
				a#a"
			);

			var game = Kernel.GetNewGameMachine(ParseBoard(initialBoard));

			game.PlayMove(TilePos.FromName("c1"), TilePos.FromName("c2"));
			game.PlayDropAt(TilePos.FromName("b3"));

			Assert.Equal(finalBoard, DumpBoard(game.State.Board));
			Assert.Equal(ClassicPlayer.White.GetId(), game.State.Winners.AssertSingle());
		}

		[Fact]
		void TestMachine_PlayKillingBlackMove()
		{
			string initialBoard = PrepareBoard(@"
				A.A
				#.#
				a.a"
			);
			string finalBoard = PrepareBoard(@"
				A#A
				#a#
				a.."
			);

			var game = Kernel.GetNewGameMachine(ParseBoard(initialBoard));
			game.State.CurrentPlayerId = ClassicPlayer.Black.GetId();

			game.PlayMove(TilePos.FromName("c3"), TilePos.FromName("b2"));
			game.PlayDropAt(TilePos.FromName("b1"));

			Assert.Equal(finalBoard, DumpBoard(game.State.Board));
			Assert.Equal(ClassicPlayer.Black.GetId(), game.State.Winners.AssertSingle());
		}
	}
}
