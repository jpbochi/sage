﻿using System;
using System.Linq;
using System.Text;
using JpLabs.Symbols;
using Ninject;
using Sage.Games.Amazons;

namespace Sage.Games.Tests.Amazons
{
	public class Amazons_Helper
	{
		private Lazy<IKernel> lazyKernel = new Lazy<IKernel>( () => new StandardKernel(new Sage.Games.Amazons.AmazonsGameModule()) );

		protected IKernel Kernel 
		{
			get { return lazyKernel.Value; }
		}

		protected IGameState GetNewState()
		{
			return Kernel.Get<IGameState>();
		}

		private IGamePiece GetAmazonPiece(int flag)
		{
			return Kernel.Get<IGamePieceClass>(AmazonPieces.Amazon.Name).CreatePiece().SetFlag(flag);
		}

		private IGamePiece GetArrowPiece()
		{
			return Kernel.Get<IGamePieceClass>(AmazonPieces.Arrow.Name).CreatePiece();
		}

		/// <param name="strBoard">Example: "\nA#.\n...\n.#a"</param>
		protected IGameBoard ParseBoard(string strBoard)
		{
			var lines = strBoard.Split(new []{'\n'}, StringSplitOptions.RemoveEmptyEntries);

			int rows = lines.Length;
			int cols = lines[0].Length;
			var board = new RectBoard((ushort)cols, (ushort)rows);

			for (int r=0; r<rows; r++) {
				for (int c=0; c<cols; c++) {
					if (lines[r][c] == 'A') board.AddPiece(GetAmazonPiece(ClassicPlayer.White.GetId()), TilePos.Create(c, r));
					else
					if (lines[r][c] == 'a') board.AddPiece(GetAmazonPiece(ClassicPlayer.Black.GetId()), TilePos.Create(c, r));
					else
					if (lines[r][c] == '#') board.AddPiece(GetArrowPiece(), TilePos.Create(c, r));
				}
			}

			return board;
		}

		protected static string PrepareBoard(string boardSetup)
		{
			return boardSetup
				.Replace(" ", "")
				.Replace("\t", "")
				.Replace("\r", "")
				.TrimEnd('\n');
		}

		protected static string DumpBoard(IGameBoard board)
		{
			var lines = board.GetAllTiles().GroupBy( t => t.Y ).ToArray();

			var builder = new StringBuilder();
			foreach (var line in lines) {
				builder.Append('\n');

				foreach (var tile in line) {
					var piece = board.GetPiecesAt(tile).FirstOrDefault();

					var ch = (piece == null) ? '.'
						   : (piece.GetFlag() == ClassicPlayer.White.GetId()) ? 'A'
						   : (piece.GetFlag() == ClassicPlayer.Black.GetId()) ? 'a'
						   : '#';
					
					builder.Append(ch);
				}
			}
			
			return builder.ToString();
		}
	}
}
