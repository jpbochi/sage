﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Sage.Games.Tests.Amazons
{
	public class Amazons_Net_Tests : Amazons_Machine_Tests
	{
		[Fact]
		void InitialStateIsSerializable()
		{
			var state = GetNewState();

			var serialized = GameSerializer.Serialize<IGameState>(state);
		}

		[Fact]
		void InitialStateIsDeserializable()
		{
			var state = GetNewState();

			var serialized = GameSerializer.Serialize<IGameState>(state);
			var deserialized = GameSerializer.Deserialize<IGameState>(serialized);

			VerifyInitialState(deserialized);
		}

		[Fact]
		void InitialStateReserialization_GivesSameResult()
		{
			var state = GetNewState();

			var serialized = GameSerializer.Serialize<IGameState>(state);
			var deserialized = GameSerializer.Deserialize<IGameState>(serialized);
			var reserialized = GameSerializer.Serialize<IGameState>(deserialized);

			//deserialized.Board.GetAllPieces().First().GetPieceType() == Sage.Games.Amazons.AmazonPieces.Amazon

			Assert.Equal(serialized, reserialized);
		}

		[Fact]
		void InitialMoveDescriptions_AreSerializable()
		{
			var machine = Kernel.GetNewGameMachine();
			var moves = machine.GetMoveOptions().ToArray();

			var serialized = GameSerializer.Serialize(moves);
			var deserialized = GameSerializer.Deserialize<IGameMoveDescription[]>(serialized);
			var reserialized = GameSerializer.Serialize(deserialized);

			Assert.Equal(serialized, reserialized);
			Assert.InRange(serialized.Length, 0, ushort.MaxValue);
		}
	}
}
