﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Symbols;
using Ninject;
using Sage.Games.Amazons;
using Sage.Tests;
using Xunit;
using Xunit.Extensions;

namespace Sage.Games.Tests.Amazons
{
	public class Amazons_State_Tests : Amazons_Helper
	{
		private IGameCommand GetUpdateScoreCommand()
		{
			return Kernel.Get<IGameCommand>(KnownGameOperations.UpdateScore);
		}

		private IGameState GetStateWithBoardSetup(string boardSetup)
		{
			var board = ParseBoard(boardSetup);
			Kernel.Rebind<IGameBoard>().ToConstant(board);

			return Kernel.Get<IGameState>();
		}

		static IEnumerable<Tuple<string,int,int>> GetWinningBoards()
		{
			yield return Tuple.Create(PrepareBoard(@"
				A#.
				##a")
				, ClassicPlayer.White.GetId()
				, ClassicPlayer.Black.GetId()
			);
			yield return Tuple.Create(PrepareBoard(@"
				A##
				.#a")
				, ClassicPlayer.Black.GetId()
				, ClassicPlayer.White.GetId()
			);
		}

		static IEnumerable<string> GetGameNotEndedBoards()
		{
			yield return PrepareBoard(@"
				..A.A..
				A.....A
				a.....a
				..a.a..");
			yield return PrepareBoard(@"
				AAAA#..
				####...
				.......
				..#####
				...aaaa");
			yield return PrepareBoard(@"
				A##
				.#a");
			yield return PrepareBoard("\nA.a");
			yield return PrepareBoard("\nA.#a");
		}

		public static IEnumerable<object[]> WinningConditions
		{
			get { return GetWinningBoards().Select( t => new object[] { t.Item1, t.Item2, t.Item3 } ); }
		}

		public static IEnumerable<object[]> GameNotEndedConditions
		{
			get { return GetGameNotEndedBoards().Select( t => new object[] { t } ); }
		}

		public static IEnumerable<object[]> TestBoards
		{
			get {
				return
					GameNotEndedConditions
					.Concat(
						GetWinningBoards().Select( t => new object[] { t.Item1 } )
					);
			}
		}

		[Fact]
		void CreateInitialState()
		{
			var state = GetNewState();
			
			VerifyInitialState(state);
		}

		protected void VerifyInitialState(IGameState state)
		{
			AssertExt.AssertSequenceEqual(
				new []{ ClassicPlayer.White.GetId(), ClassicPlayer.Black.GetId()},
				state.Players.Select( p => p.Id )
			);

			Assert.Equal(ClassicPlayer.White.GetId(), state.CurrentPlayerId);

			Assert.NotNull(state.Board);

			AssertExt.SetEqual(
				from c in Enumerable.Range(0, 7)
				from r in Enumerable.Range(0, 7)
				select TilePos.Create(c, r),
				state.Board.GetAllTiles()
			);

			Assert.Equal(AmazonPieces.Arrow, state.Board.GetPiecesAt(TilePos.OffBoard).AssertSingle().GetPieceType());

			string expectedBoard = PrepareBoard(@"
				..A.A..
				.......
				A.....A
				.......
				a.....a
				.......
				..a.a.."
			);
			Assert.Equal(expectedBoard, DumpBoard(state.Board));

			state.Board.GetAllPieces().Where(p => p.Position != TilePos.OffBoard).AssertAll( p => Assert.NotNull(p.GetArrowClass()) );
		}

		[Theory]
		[PropertyData("TestBoards")]
		void TestBoardParseAndDumper(string boardDesc)
		{
			var board = ParseBoard(boardDesc);

			Assert.Equal(boardDesc, DumpBoard(board));
		}

		[Theory]
		[PropertyData("WinningConditions")]
		void VerifyWinningConditions(string boardDesc, int currentPlayerId, int expectedWinner)
		{
			var state = GetStateWithBoardSetup(boardDesc);
			state.CurrentPlayerId = currentPlayerId;
			var cmd = GetUpdateScoreCommand();

			cmd.Execute(state);

			Assert.Equal(expectedWinner, state.Winners.AssertSingle());
			Assert.Null(state.CurrentPlayerId);
		}

		[Theory]
		[PropertyData("GameNotEndedConditions")]
		void VerifyGameNotEndedConditions(string boardDesc)
		{
			var state = GetStateWithBoardSetup(boardDesc);
			var cmd = GetUpdateScoreCommand();

			cmd.Execute(state);

			Assert.Empty(state.Winners);
			Assert.NotNull(state.CurrentPlayerId);
		}
	}
}
