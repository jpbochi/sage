﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using JpLabs.Geometry;
using Sage.Tests;
using Moq;
using Sage.Games.TicTacToe;
using Ninject;
using Ninject.Parameters;
using JpLabs.Symbols;

namespace Sage.Games.Tests.TicTacToe
{
	public class TicTacToe_Referee_Tests : TicTacToe_State_Tests
	{
		protected IGameReferee GetNewReferee()
		{
		    return Kernel.Get<IGameReferee>();
		}

		[Fact]
		void TestInitialMoveOptions()
		{
			var state = GetNewState();
			var referee = GetNewReferee();

			var moves = referee.GetMoveOptions(state);
			Assert.NotNull(moves);

			VerifyInitialMoveOptions_Descriptions(moves.Select( m => m.Description ), state.Board);
			VerifyInitialMoveOptions_Resolution(moves, state.Board);
		}

		protected void VerifyInitialMoveOptions_Descriptions(IEnumerable<IGameMoveDescription> moveDescs, IGameBoard board)
		{
			var expectedPlayerId = TicTacToePlayer.X.GetId();

			// Assert all exptected moves are yielded with correct descriptions
			var expectedPieceTemplate
				= board.GetPiecesAt(TilePos.OffBoard)
				.Where( p => p.GetFlag() == expectedPlayerId )
				.AssertCast<IGamePieceClass>()
				.Single();

			var expectedDescriptions = board.GetAllTiles().Select(
				t => MoveDesc.CreateDropPieceDesc(expectedPlayerId, expectedPieceTemplate, t)
			);

			AssertExt.SetEqual(expectedDescriptions, moveDescs);
		}

		void VerifyInitialMoveOptions_Resolution(IEnumerable<IGameMove> moves, IGameBoard board)
		{
			// Assert that all moves resolve to operation that match their descriptions
			foreach (var move in moves) {
				var pieceClass = move.Description.GetPieceClass(board);
				var position = move.Description.GetDestination();

				var expectedCommands = new IGameCommand[] {
					new AddPieceCommand(pieceClass, position),
					new CycleTurnCommand(),
					new IsGameOverEvaluation(),
				};
				var cmds = move.Resolve(null);
				AssertExt.SetEqual(expectedCommands, cmds.ToArray());
			}
		}
	}
}
