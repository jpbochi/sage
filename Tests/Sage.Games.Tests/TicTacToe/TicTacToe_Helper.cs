﻿using System;
using System.Linq;
using System.Text;
using JpLabs.Symbols;
using Ninject;
using Sage.Games.TicTacToe;

namespace Sage.Games.Tests.TicTacToe
{
	public class TicTacToe_Helper
	{
		private Lazy<IKernel> lazyKernel = new Lazy<IKernel>( () => new StandardKernel(new Sage.Games.TicTacToe.TicTacToeGameModule()) );

		protected IKernel Kernel 
		{
			get { return lazyKernel.Value; }
		}

		protected IGameState GetNewState()
		{
			return Kernel.Get<IGameState>();
		}

		private IGamePiece GetMarkPiece(int flag)
		{
			return Kernel.Get<IGamePieceClass>(TicTacToePieces.Mark.Name).CreatePiece().SetFlag(flag);
		}

		/// <param name="strBoard">Example: "\n...\n.X.\n..O"</param>
		protected IGameBoard ParseBoard(string strBoard)
		{
			var board = Kernel.Get<IGameBoard>();

			var lines = strBoard.Split(new []{'\n'}, StringSplitOptions.RemoveEmptyEntries);

			for (int r=0; r<lines.Length; r++) {
				for (int c=0; c<lines[0].Length; c++) {
					if (lines[r][c] == 'X') board.AddPiece(GetMarkPiece(TicTacToePlayer.X.GetId()), TilePos.Create(c, r));
					else if (lines[r][c] == 'O') board.AddPiece(GetMarkPiece(TicTacToePlayer.O.GetId()), TilePos.Create(c, r));
				}
			}

			return board;
		}

		protected string PrepareBoard(string boardSetup)
		{
			return boardSetup
				.Replace(" ", "")
				.Replace("\t", "")
				.Replace("\r", "")
				.TrimEnd('\n');
		}

		protected string DumpBoard(IGameBoard board)
		{
			var builder = new StringBuilder();

			for (int r=0; r<3; r++) {
				builder.Append('\n');

				for (int c=0; c<3; c++) {
					var piece = board.GetPiecesAt(TilePos.Create(c, r)).FirstOrDefault();

					var ch = (piece == null) ? '.'
						   : (piece.GetFlag() == TicTacToePlayer.X.GetId()) ? 'X'
						   : (piece.GetFlag() == TicTacToePlayer.O.GetId()) ? 'O'
						   : '?';
					
					builder.Append(ch);
				}
			}

			return builder.ToString();
		}
	}
}
