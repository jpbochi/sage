﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Symbols;
using Ninject;
using Sage.Games.TicTacToe;
using Sage.Tests;
using Xunit;
using Xunit.Extensions;

namespace Sage.Games.Tests.TicTacToe
{
	public class TicTacToe_State_Tests : TicTacToe_Helper
	{
		private IGameCommand GetUpdateScoreCommand()
		{
			return Kernel.Get<IGameCommand>(KnownGameOperations.UpdateScore);
		}

		private IGameState GetStateWithBoardSetup(string boardSetup)
		{
			var board = ParseBoard(boardSetup);
			Kernel.Rebind<IGameBoard>().ToConstant(board);

			return Kernel.Get<IGameState>();
		}

		static IEnumerable<Tuple<string,int>> GetWinningBoards()
		{
			yield return Tuple.Create("\nXXX\n...\n...", TicTacToePlayer.X.GetId());
			yield return Tuple.Create("\n...\nXXX\n...", TicTacToePlayer.X.GetId());
			yield return Tuple.Create("\n...\n...\nXXX", TicTacToePlayer.X.GetId());
			yield return Tuple.Create("\nX..\nX..\nX..", TicTacToePlayer.X.GetId());
			yield return Tuple.Create("\n.X.\n.X.\n.X.", TicTacToePlayer.X.GetId());
			yield return Tuple.Create("\n..X\n..X\n..X", TicTacToePlayer.X.GetId());
			yield return Tuple.Create("\nX..\n.X.\n..X", TicTacToePlayer.X.GetId());
			yield return Tuple.Create("\n..X\n.X.\nX..", TicTacToePlayer.X.GetId());

			yield return Tuple.Create("\nOOO\n...\n...", TicTacToePlayer.O.GetId());
			yield return Tuple.Create("\n...\nOOO\n...", TicTacToePlayer.O.GetId());
			yield return Tuple.Create("\n...\n...\nOOO", TicTacToePlayer.O.GetId());
			yield return Tuple.Create("\nO..\nO..\nO..", TicTacToePlayer.O.GetId());
			yield return Tuple.Create("\n.O.\n.O.\n.O.", TicTacToePlayer.O.GetId());
			yield return Tuple.Create("\n..O\n..O\n..O", TicTacToePlayer.O.GetId());
			yield return Tuple.Create("\nO..\n.O.\n..O", TicTacToePlayer.O.GetId());
			yield return Tuple.Create("\n..O\n.O.\nO..", TicTacToePlayer.O.GetId());
		}

		static IEnumerable<string> GetDrawBoards()
		{
			yield return "\nXOX\nOXO\nOXO";
			yield return "\nOXO\nXOX\nXOX";
		}

		static IEnumerable<string> GetGameNotEndedBoards()
		{
			yield return "\nXOX\nOXO\nOX.";
			yield return "\nOXO\nXOX\nXO.";
		}

		public static IEnumerable<object[]> WinningConditions
		{
			get { return GetWinningBoards().Select( t => new object[] { t.Item1, t.Item2 } ); }
		}

		public static IEnumerable<object[]> DrawConditions
		{
			get { return GetDrawBoards().Select( t => new object[] { t } ); }
		}

		public static IEnumerable<object[]> GameNotEndedConditions
		{
			get { return GetGameNotEndedBoards().Select( t => new object[] { t } ); }
		}

		public static IEnumerable<object[]> TestBoards
		{
			get {
				return
					GameNotEndedConditions
					.Concat(DrawConditions)
					.Concat(
						GetWinningBoards().Select( t => new object[] { t.Item1 } )
					);
			}
		}

		[Fact]
		void CreateInitialState()
		{
			var state = GetNewState();
			
			VerifyInitialState(state);
		}

		protected void VerifyInitialState(IGameState state)
		{
			AssertExt.AssertSequenceEqual(
				new []{ TicTacToePlayer.X.GetId(), TicTacToePlayer.O.GetId()},
				state.Players.Select( p => p.Id )
			);

			Assert.Equal(TicTacToePlayer.X.GetId(), state.CurrentPlayerId);

			Assert.NotNull(state.Board);

			Assert.Equal(2, state.Board.GetAllPieces().Count());
			Assert.Equal(2, state.Board.GetPiecesAt(TilePos.OffBoard).Count());

			Assert.Single(state.Board.GetPiecesAt(TilePos.OffBoard).Where( p => p.GetFlag() == TicTacToePlayer.X.GetId() ));
			Assert.Single(state.Board.GetPiecesAt(TilePos.OffBoard).Where( p => p.GetFlag() == TicTacToePlayer.O.GetId() ));

			AssertExt.SetEqual(
				from c in Enumerable.Range(0, 3)
				from r in Enumerable.Range(0, 3)
				select TilePos.Create(c, r),
				state.Board.GetAllTiles()
			);
		}

		[Theory]
		[PropertyData("TestBoards")]
		void TestBoardParseAndDumper(string boardDesc)
		{
			var board = ParseBoard(boardDesc);

			Assert.Equal(boardDesc, DumpBoard(board));
		}

		[Theory]
		[PropertyData("WinningConditions")]
		void VerifyWinningConditions(string boardDesc, int expectedWinner)
		{
			var state = GetStateWithBoardSetup(boardDesc);
			var cmd = GetUpdateScoreCommand();

			cmd.Execute(state);

			Assert.Equal(expectedWinner, state.Winners.AssertSingle());
			Assert.Null(state.CurrentPlayerId);
		}

		[Theory]
		[PropertyData("DrawConditions")]
		void VerifyDrawConditions(string boardDesc)
		{
			var state = GetStateWithBoardSetup(boardDesc);
			var cmd = GetUpdateScoreCommand();

			cmd.Execute(state);

			Assert.Equal(state.Players.Select( p => p.Id ).ToArray(), state.Winners);
			Assert.Null(state.CurrentPlayerId);
		}

		[Theory]
		[PropertyData("GameNotEndedConditions")]
		void VerifyGameNotEndedConditions(string boardDesc)
		{
			var state = GetStateWithBoardSetup(boardDesc);
			var cmd = GetUpdateScoreCommand();

			cmd.Execute(state);

			Assert.Empty(state.Winners);
			Assert.NotNull(state.CurrentPlayerId);
		}
	}
}
