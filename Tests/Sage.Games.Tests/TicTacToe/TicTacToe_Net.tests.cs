﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;

namespace Sage.Games.Tests.TicTacToe
{
	public class TicTacToe_Net_Tests : TicTacToe_Machine_Tests
	{
		[Fact]
		void InitialStateIsSerializable()
		{
			var state = GetNewState();

			var serialized = GameSerializer.Serialize<IGameState>(state);
		}

		[Fact]
		void InitialStateIsDeserializable()
		{
			var state = GetNewState();

			var serialized = GameSerializer.Serialize<IGameState>(state);

			var deserialized = GameSerializer.Deserialize<IGameState>(serialized);

			VerifyInitialState(deserialized);
		}

		[Fact]
		void InitialStateReserialization_GivesSameResult()
		{
			var state = GetNewState();

			var serialized = GameSerializer.Serialize<IGameState>(state);

			var deserialized = GameSerializer.Deserialize<IGameState>(serialized);

			var reserialized = GameSerializer.Serialize<IGameState>(deserialized);

			Assert.Equal(serialized, reserialized);
		}


		[Fact]
		void InitialMoveDescriptions_AreSerializable()
		{
			var machine = Kernel.GetNewGameMachine();
			var moves = machine.GetMoveOptions().ToArray();

			var serialized = GameSerializer.Serialize(moves);
			var deserialized = GameSerializer.Deserialize<IGameMoveDescription[]>(serialized);
			var reserialized = GameSerializer.Serialize(deserialized);

			Assert.Equal(serialized, reserialized);
			Assert.InRange(serialized.Length, 0, ushort.MaxValue);
		}
	}
}
