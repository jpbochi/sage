﻿using System.Linq;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Sage.Games.TicTacToe;
using Sage.Tests;
using Xunit;

namespace Sage.Games.Tests.TicTacToe
{
	public class TicTacToe_Machine_Tests : TicTacToe_Referee_Tests
	{
		[Fact]
		void TestInitialMachine()
		{
			var game = Kernel.GetNewGameMachine();

			VerifyInitialState(game.State);
			VerifyInitialMoveOptions_Descriptions(game.GetMoveOptions(), game.State.Board);
		}

		[Fact]
		void TestMachine_PlayValidFisrtMove()
		{
			var game = Kernel.GetNewGameMachine();

			game.PlayDropAt(TilePos.Create(1, 2));

			string expectedBoard = PrepareBoard(@"
				...
				...
				.X."
			);

			Assert.Equal(expectedBoard, DumpBoard(game.State.Board));
			Assert.Equal(TicTacToePlayer.O.GetId(), game.State.CurrentPlayerId);
			Assert.Empty(game.State.Winners);
		}

		[Fact]
		void TestMachine_PlayKillingXMove()
		{
			string initialBoard = PrepareBoard(@"
				X.X
				.O.
				X.O"
			);
			string finalBoard = PrepareBoard(@"
				XXX
				.O.
				X.O"
			);

			var game = Kernel.GetNewGameMachine(ParseBoard(initialBoard));

			game.PlayDropAt(TilePos.Create(1, 0));

			Assert.Equal(finalBoard, DumpBoard(game.State.Board));
			Assert.Equal(TicTacToePlayer.X.GetId(), game.State.Winners.AssertSingle());
		}

		[Fact]
		void TestMachine_PlayKillingOMove()
		{
			string initialBoard = PrepareBoard(@"
				X.X
				O.O
				X.X"
			);
			string finalBoard = PrepareBoard(@"
				X.X
				OOO
				X.X"
			);

			var game = Kernel.GetNewGameMachine(ParseBoard(initialBoard));
			game.State.CurrentPlayerId = TicTacToePlayer.O.GetId();

			game.PlayDropAt(TilePos.Create(1, 1));

			Assert.Equal(finalBoard, DumpBoard(game.State.Board));
			Assert.Equal(TicTacToePlayer.O.GetId(), game.State.Winners.AssertSingle());
		}

		[Fact]
		void TestMachine_PlayDrawMove()
		{
			string initialBoard = PrepareBoard(@"
				OXO
				XOX
				XO."
			);
			string finalBoard = PrepareBoard(@"
				OXO
				XOX
				XOX"
			);

			var game = Kernel.GetNewGameMachine(ParseBoard(initialBoard));

			game.PlayDropAt(TilePos.Create(2, 2));

			Assert.Equal(finalBoard, DumpBoard(game.State.Board));
			Assert.Equal(game.State.Players.Select( p => p.Id ).ToArray(), game.State.Winners);
		}
	}
}
