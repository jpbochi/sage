﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using JpLabs.Symbols;
using System.Collections.ObjectModel;
using JpLabs.Geometry;

namespace Sage.Games.Tests
{
	internal class GameSerializer
	{
		private static DataContractSerializer CreateSerializer<T>()
		{
			var knownTypes = GetKnownTypes();
			int maxItemsInObjectGraph = int.MaxValue;
			bool ignoreExtensionDataObject = false;
			bool preserveObjectReferences = true;
			IDataContractSurrogate dataContractSurrogate = null;//new SerializeBaseTypeSurrogate();
			DataContractResolver dataContractResolver = new SymbolContractResolver();

			return new DataContractSerializer(
				typeof(T),
				knownTypes,
				maxItemsInObjectGraph,
				ignoreExtensionDataObject,
				preserveObjectReferences,
				dataContractSurrogate,
				dataContractResolver
			);
		}

		private static IEnumerable<Type> GetKnownTypes()
		{
			return new [] {
				typeof(BasicGameState),
				typeof(RectBoard),
				typeof(BasicPlayer),
				typeof(BasicPiece),
				typeof(BasicPieceClass),
				typeof(Symbol),

				typeof(MoveDescription),
				typeof(PointInt),
			};
		}

		public static string Serialize<T>(T obj)
		{
			var output = new StringBuilder();
			using (var textWriter = new StringWriter(output))
			using (var xmlWriter = new XmlTextWriter(textWriter) { Formatting = Formatting.Indented } )
			{
				XmlObjectSerializer serializer = CreateSerializer<T>();

				serializer.WriteObject(xmlWriter, obj);
			}
			
			return output.ToString();
		}

		public static T Deserialize<T>(string serializedObj)
		{
			using (var textReader = new StringReader(serializedObj))
			using (var xmlReader = new XmlTextReader(textReader))
			{
				XmlObjectSerializer serializer = CreateSerializer<T>();
				return (T)serializer.ReadObject(xmlReader);
			}
		}
	}
}
